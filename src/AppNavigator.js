import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  StatusBar,
  Dimensions,
  Platform,
  Image,
  TextInput
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import {Root} from 'native-base';

import styles from './styles/style';

import Splash from './views/Splash';
import Login from './views/Login';
import Register from './views/Register';
import PasswordVerfication from './views/passwordVerification';
import Dashboard from './views/Dashboard';
import Guest from './views/Guest'
import UploadVideo from './views/UploadVideo'
import OnAir from './views/OnAir'
import AddEvents from './views/AddEvents'
import Purchase from './views/Purchase'
import GroupChat from './views/GroupChat'
import Notifications from './views/Notifications'
import Profile from './views/Profile'
// {
//       initialRouteName: 'Splash',
//       headerMode:'none',
//       header:null,
// });

const Components = [
  {
    name:"Splash",
    component:Splash
  },
  {
    name:"Login",
    component:Login
  },
  {
    name:"Register",
    component:Register
  },
  {
    name:"PasswordVerfication",
    component:PasswordVerfication
  },
  {
    name:"Dashboard",
    component:Dashboard
  },
  {
    name:"Guest",
    component:Guest
  },
  {
    name:"UploadVideo",
    component:UploadVideo
  },
  {
    name:"OnAir",
    component:OnAir
  },
  {
    name:"AddEvents",
    component:AddEvents
  },
  {
    name:"GroupChat",
    component:GroupChat
  },
  {
    name:"Notifications",
    component:Notifications
  },
  {
    name:"Profile",
    component:Profile
  },
  {
    name:"Purchase",
    component:Purchase
  },
]

export default class App extends Component {

_renderStatusBar(){
    if(Platform.OS === "ios"){
      return (
        <StatusBar translucent
          animated
          StatusBarAnimation="fade"
          backgroundColor="rgba(0,0,0,0.3)"
          barStyle="light-content"/>
      )
    }
    else {
      return (
        <StatusBar
          animated
          StatusBarAnimation="fade"
          backgroundColor="black"
          barStyle="light-content"/>
      )
    }
}

constructor() {
  super();
  Text.defaultProps = Text.defaultProps || {}
  Text.defaultProps.allowFontScaling=false;
  TextInput.defaultProps = TextInput.defaultProps || {}
  TextInput.defaultProps.allowFontScaling=false;
}

render() {
    const Stack = createStackNavigator();
    return (
      <Root>
      <View style={{flex:1, backgroundColor:"#bd376a"}}>
        {this._renderStatusBar()}
        <NavigationContainer>
          <Stack.Navigator headerMode={null}>
            {Components.map((res,i)=>{
              return(
                <Stack.Screen name={res.name} component={res.component} />
              )
            })}
          </Stack.Navigator>
        </NavigationContainer>
        
        {/* <AppStack
          onNavigationStateChange={(prevState, currentState, action) => {
          }}
        /> */}
      </View>
      </Root>
    );
  }

}
