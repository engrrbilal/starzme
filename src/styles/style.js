import {
  Platform,
  StyleSheet,
  Dimensions,
} from 'react-native';
export const { width, height } = Dimensions.get("window");
export const pinkDark = "#dc5de0";
const IS_IPHONE_X = height === 812 || height === 896;
const marginMinusSmall = -(height / 15)
const marginMinusBig = -(height / 14)
const fontSmall = 14
const fontMedium = 16
const fontBig = 18
const fontLarge = 20
const fontXLarge = 24
const fontBold = "Montserrat-Bold"
const fontRegular = "Montserrat-Regular"
const fontLight = "Montserrat-Light"
const styles = StyleSheet.create({
  fontBook: {
    fontFamily: fontRegular
  },
  splashBg: {
    width: width,
    height: height,
    resizeMode: "stretch",
    flex: 1,
    backgroundColor: "#000"
  },
  splashInner: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  splashIndicator: {
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: 40
  },
  splashLogo: {
    width: width / 2,
    height: height / 4,
    resizeMode: "contain"
  },
  splashLogoText: {
    width: width / 2,
    height: 70,
    resizeMode: "contain"
  },
  mainBg: {
    width: width,
    height: height,
    resizeMode: "stretch",
    flex: 1,
    backgroundColor: "#4b2a74"
  },
  mainLogo: {
    width: width / 1.4,
    height: height / 6,
    resizeMode: "contain",
    marginLeft: -30
  },
  labelContainer: {
    flexDirection: "row",
    flex: 1,
    paddingVertical: 10,
    borderBottomWidth: 1,
    borderColor: "#cccccc",
    paddingHorizontal: 20
  },
  formIcons: {
    marginHorizontal: 5,
    width: 25,
    height: 25,
    resizeMode: "contain"
  },
  labelForm: {
    fontSize: 14,
    color: "#969696",
    paddingTop: 5,
    fontFamily: fontLight,
    letterSpacing: 2,
  },
  formIconsCont: {
    alignItems: "center",
    justifyContent: "center",
    marginRight: 10
  },
  formInput: {
    width: "100%",
    height: 45,
    fontSize: 15,
    color: "#999999",
    fontFamily: fontRegular,
  },
  phoneText: {
    height: 45,
    fontSize: 15,
    color: "#999999",
    fontFamily: fontRegular,
  },
  formInputText: {
    width: "100%",
    fontSize: 15,
    color: "#999999",
    fontFamily: fontRegular,
    paddingVertical: 15
  },
  loginBottomText: {
    paddingVertical: 20,
    color: "#FFF",
    fontSize: 12,
    textAlign: "center",
    fontFamily: fontRegular,
    letterSpacing: 1
  },
  loginTitleText: {
    color: "#FFF",
    fontSize: 30,
    textAlign: "center",
    textTransform: "uppercase",
    fontWeight: "bold"
  },
  loginInnerText: {
    color: "#FFF",
    fontSize: 16
  },
  pinInput: {
    width: '22%',
    height: 45,
    fontSize: 14,
    marginLeft: 5,
    marginRight: 5,
    textAlign: 'center',
    color: "#000",
    fontWeight: "bold",
    backgroundColor: "#FFF"
    //fontFamily:fontRegular,
  },
  socialIconsCont: {
    width: width / 5,
    height: 60,
    alignItems: "center",
    justifyContent: "center"
  },
  socialIcons: {
    width: "60%",
    height: "100%",
    resizeMode: "contain"
  },
  dashboardMainView: {
    flex: 1,
    backgroundColor: '#fff'
  },
  dashboardContainerView: {
    flex: 1
  },
  drawerContainer: {
    flex: 1,
    backgroundColor: "#f27353",
    borderTopRightRadius: 15,
    overflow: "hidden"
  },
  bannerCont: {
    width: "100%",
    height: height / 4,
    backgroundColor: "grey",
    borderRadius: 20,
    overflow: "hidden"
  },
  bannerImage: {
    width: null,
    height: height / 4,
    resizeMode: "cover"
  },
  detailBannerMainCont: {
    width: "100%",
    height: height / 4,
    backgroundColor: "#FFF",
    borderRadius: 20,
    shadowColor: "#787878",
    shadowOffset: { width: 0, height: 6 },
    shadowRadius: 5,
    shadowOpacity: 0.4
  },
  detailBannerCont: {
    width: "100%",
    height: height / 4,
    backgroundColor: "grey",
    borderRadius: 20,
    overflow: "hidden"
  },
  detailBannerImage: {
    width: null,
    height: height / 4,
    resizeMode: "cover"
  },
  detailBannerInner: {
    backgroundColor: "#FFF",
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    backgroundColor: "#FFF",
    shadowColor: "#787878",
    shadowOffset: { width: 0, height: 5 },
    shadowRadius: 3,
    shadowOpacity: 0.2,
    marginTop: -50,
    flex: 1,
    flexDirection: "row",
    paddingVertical: 10,
    paddingHorizontal: 10
  },
  bannerDetailIconContInner: {
    width: 50,
    height: 50,
    overflow: "hidden",
    borderRadius: 25
  },
  bannerDetailIconContInner2: {
    width: 50,
    height: 50
  },
  bannerDetailIconImage: {
    width: 50,
    height: 50,
    resizeMode: "stretch"
  },
  bannerDetailIconCont: {
    width: 50
  },
  detailInnerIcons: {
    width: 40,
    height: 40,
    marginLeft: 2
  },
  detailInnerIconsImage: {
    width: 40,
    height: 40
  },
  gymHeading: {
    color: "#2d2d2f",
    fontSize: fontSmall,
    fontWeight: "bold"
  },
  marginTopMinus: {
    top: IS_IPHONE_X === true ? marginMinusBig : marginMinusSmall
  },
  homeHeading: {
    fontWeight: "bold",
    fontSize: fontBig,
    color: "#000",
    fontFamily: fontRegular
  },
  homeHeadingUnSelected: {
    fontSize: fontBig,
    color: "#8a93a5",
    fontFamily: fontRegular
  },
  homeInnerText: {
    fontSize: fontSmall,
    color: "#73879b",
    paddingVertical: 5,
    fontFamily: fontLight
  },
  cardView: {
    flex: 1,
    borderRadius: 20,
    backgroundColor: "#FFF",
    paddingHorizontal: 20,
    paddingVertical: 20,
    shadowColor: "#787878",
    shadowOffset: { width: 0, height: 3 },
    shadowRadius: 5,
    shadowOpacity: 0.2,
    borderWidth: Platform.OS == 'ios' ? 0 : 1,
    borderColor: "#e6eaeb"
  },
  aboutText: {
    color: "#8a93a5",
    fontSize: fontSmall,
    lineHeight: 23
  },
  editImageCont: {
    width: 124,
    height: 124,
    borderRadius: 62,
    borderWidth: 4,
    borderColor: "#fcb828",
    position: "relative",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#fcb828"
  },
  editImage: {
    width: 116,
    height: 116,
    resizeMode: "cover",
    borderRadius: 58
  },
  cameraIconCont: {
    backgroundColor: "#fcb828",
    width: 40,
    height: 40,
    borderRadius: 20,
    position: "absolute",
    right: -5,
    bottom: -5,
    alignItems: "center",
    justifyContent: "center"
  },
  cameraIconImage: {
    width: 25,
    height: 25,
    resizeMode: "contain"
  },
  labelContainerInner: {
    flex: 1,
    paddingVertical: 10
  },
  labelFormInner: {
    fontSize: 14,
    color: "#73879b",
    paddingVertical: 5,
    paddingHorizontal: 10,
    fontFamily: fontLight
  },
  formInputInner: {
    width: "100%",
    height: 45,
    fontSize: 16,
    color: "#2d2d2f",
    backgroundColor: "#f9f9f7",
    borderRadius: 10,
    paddingHorizontal: 10,
    fontFamily: fontRegular
  },
  formInputInnerText: {
    width: "100%",
    lineHeight: 45,
    fontSize: 16,
    color: "#2d2d2f",
    backgroundColor: "#f9f9f7",
    borderRadius: 10,
    paddingHorizontal: 10,
    fontFamily: fontRegular
  },
  checkinText: {
    fontSize: fontMedium,
    color: "#73879b",
    paddingVertical: 5,
    textAlign: "center"
  },
  payText: {
    textAlign: "center",
    flex: 1,
    color: "#030505",
    fontWeight: "bold",
    fontSize: fontMedium,
    paddingVertical: 2
  },
  payTextDesc: {
    textAlign: "center",
    flex: 1,
    color: "#73879b",
    fontSize: fontSmall,
    paddingTop: 5,
    paddingBottom: 10,
    lineHeight: 23
  },
  payTextAmount: {
    textAlign: "center",
    flex: 1,
    color: "#f27f55",
    fontSize: fontXLarge,
    paddingVertical: 2,
    fontWeight: "bold"
  },
  barHeaderTextUnselected: {
    borderBottomWidth: 3,
    borderColor: "#e7e8e8",
    paddingVertical: 10,
    paddingRight: 30,
    marginBottom: 20
  },
  barHeaderTextSelected: {
    borderColor: "#030505",
  },
  gymItemContainer: {
    flex: 1,
    borderRadius: 10,
    paddingHorizontal: 10,
    paddingVertical: 7,
    shadowColor: "#787878",
    shadowOffset: { width: 0, height: 1 },
    borderWidth: Platform.OS == 'ios' ? 0 : 1,
    borderColor: "#e6eaeb",
    shadowRadius: 5,
    shadowOpacity: 0.2,
    backgroundColor: "#FFF",
    marginVertical: 15
  },
  gymIconContInner: {
    width: 40,
    height: 40,
    overflow: "hidden",
    borderRadius: 20
  },
  gymIconImage: {
    width: 40,
    height: 40,
    resizeMode: "stretch"
  },
  gymIconCont: {
    width: 40
  },
  gymListHeading: {
    color: "#2d2d2f",
    fontSize: fontSmall,
    fontWeight: "bold"
  },
  gymListDesc: {
    color: "#8697a8",
    fontSize: 10,
    paddingVertical: 2
  },
  gymImageContInner: {
    width: 100,
    height: 100,
    overflow: "hidden",
    borderRadius: 10,
    top: -20,
    backgroundColor: "grey",
    borderWidth: 0.3,
    borderColor: "#787878"
  },
  gymImage: {
    width: 100,
    height: 100,
    resizeMode: "cover"
  },
  gymImageCont: {
    width: 100
  },
  notItemContainer: {
    flex: 1,
    borderRadius: 10,
    paddingHorizontal: 5,
    paddingVertical: 7,
    backgroundColor: "#FFF",
    marginVertical: 5
  },
  notIconCont: {
    width: 40,
    alignItems: "center",
    justifyContent: "center"
  },
  subHeaderButtons: {
    flex: 1,
    height: 45,
    backgroundColor: "#FFF",
    justifyContent: "center",
    alignItems: "center",
    borderBottomWidth: 2,
    borderColor: "#e7e8e8",
    paddingBottom: 10,
    marginBottom: 5
  },
  subHeaderButtonsUnselected: {
    flex: 1,
    height: 45,
    backgroundColor: "#FFF",
    justifyContent: "center",
    alignItems: "center",
    borderBottomWidth: 2,
    borderColor: "#000",
    paddingBottom: 10,
    marginBottom: 5
  },
  subHeaderText: {
    color: "#000",
    fontSize: 14,
    textAlign: "center",
    fontWeight: "bold"
  },
  subHeaderTextUnselected: {
    color: "grey",
    fontSize: 14,
    textAlign: "center",
    fontWeight: "bold",
  },
  starIconsBig: {
    fontSize: 30,
    marginHorizontal: 2,
    color: "#f3b118"
  },
  switchButton: {
    transform: [{ scaleX: Platform.OS == 'ios' ? 0.8 : 1 }, { scaleY: Platform.OS == 'ios' ? 0.8 : 1 }],
    resizeMode: "contain",
    marginTop: Platform.OS == 'ios' ? 13 : -5,
    marginLeft: 10
  },
  videoContainer: {
    width: width / 2
  },
  headerHeading: {
    fontSize: 24,
    color: "#FFF",
    textAlign: 'center',
    paddingHorizontal: 20,
    marginTop: 30,
    marginBottom: 10
  },
  profileImageParentView: {
    width: '100%',
    height: height * 0.17,
    backgroundColor: 'green'
  },
  profileImageContainer: {
    width: 70,
    height: 70,
    borderRadius: 50,
    overflow: 'hidden',
    flexWrap: "wrap-reverse",
    marginRight: 10
  },
  profileImage: {
    flex: 1
  },
  nameContainer: {
    width: '100%',
    paddingHorizontal: 20
  },
  userName: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '500',
    marginTop: 10,
    width: width * 0.5,
    textAlign: "left",
  },
  accountText: {
    fontSize: 20,
    color: '#000',
    fontWeight: '500',
    width: 100,
  },
  cardParent: {
    flex: 1,
    width: '100%',
    paddingHorizontal: 10,
    paddingVertical: 10
  },
  cardContainer: {
    width: '100%',
    backgroundColor: '#fff',
    borderRadius: 20,
    overflow: 'hidden',
    borderWidth: 0.5,
    borderColor: 'gray',
    marginBottom: 20
  },
  cardInputContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    paddingHorizontal: 10,
    paddingBottom: 10,
    alignItems: 'center',
  },
  cardHeadingContainer: {
    width: '100%',
    height: 50,
    justifyContent: 'center',
    borderBottomWidth: 0.5,
    borderBottomColor: 'gray',
    marginBottom: 5,
    paddingHorizontal: 10
  },
  cardInputHeadings: {
    fontSize: 20,
    color: 'red'
  },
  cardInput: {
    width: width * 0.7,
    height: 50,
    marginTop: 5,
  },
  iconContainer: {
    width: width * 0.1,
    height: 30,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  profileImageParentContainer: {
    paddingHorizontal: 10,
    flexDirection: "row",
    marginBottom: 10,
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  confirmButtom: {
    width: 100,
    height: 50,
    alignSelf: 'center',
    marginBottom: 30,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderColor: 'gray',
  },
  nodePlayerView: {
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0
  },
  nodeCameraView: {
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0
  },
  playBtn: {
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    backgroundColor: "#333",
    borderColor: "#333",
    borderWidth: 3,
    borderRadius: 2,
    height: 50,
    width: width / 2,
    paddingVertical: 10,
    paddingHorizontal: 30,
    elevation: 4,
    marginVertical: 10
  },
  playBtnContainer: {
    position: "absolute",
    bottom: 100,
    left: 0,
    right: 0,
    marginVertical: 20
  },
  goLive: {
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    backgroundColor: "#d1a667",
    borderColor: "#d1a667",
    borderWidth: 3,
    borderRadius: 2,
    height: 50,
    width: width / 2,
    paddingVertical: 10,
    paddingHorizontal: 30,
    elevation: 4,
    marginVertical: 10
  },
  adminBtnContainer: {
    position: "absolute",
    top: 0,
    right: 0,
    margin: 30,
    marginTop: 60
  },
  adminBtn: {
    backgroundColor: "#006D9E",
    padding: 10,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 5,
    elevation: 4
  },
  btnText: { color: "#FFF", fontSize: 18 }
});

export default styles;
