/* @flow weak */
import React, {Component} from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  StyleSheet,
  Text,
  ImageBackground,
  Dimensions,
  Platform
} from 'react-native';

import { Icon } from 'native-base';
const {width, height} = Dimensions.get("window");
const fontBold = "Montserrat-Regular"

class StateDropDown extends Component {
  render(){
    const {text, onPress} = this.props;
    return (
      <View style={{width:"100%", alignItems:"center", justifyContent:"center", paddingVertical:20}}>
        <View style={{width:"80%", height:40, backgroundColor:"#FFF", borderRadius:30, paddingHorizontal:15}}>
          <TouchableOpacity style={{flexDirection:"row"}} onPress={onPress}>
            <Text style={{flex:1, paddingVertical:10, color:"#908f8f", fontSize:14}}>{text}</Text>
            <View style={{width:30, height:40, alignItems:"center", justifyContent:"center"}}>
              <Icon type="MaterialIcons" style={{fontSize:20, textAlign:"right", color:"#908f8f"}} name="arrow-drop-down" />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

export { StateDropDown };
