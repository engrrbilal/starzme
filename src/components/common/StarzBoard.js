/* @flow weak */
import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  ActivityIndicator,
  Image,
  Platform,
  ImageBackground
} from 'react-native';

import * as Animatable from 'react-native-animatable';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
const fontRegular = "Montserrat-Regular"

class StarzBoard extends Component {
  render(){
    const {leftPress,rightPress,bottomLeftPress,bottomRightPress} = this.props;
    return (
      <Animatable.View
      animation="zoomIn"
      iterationCount={1}
      style={{flex:1, paddingVertical:10, paddingHorizontal:2}}
      direction="alternate"
      duration={400}
      delay={200}>

          <ImageBackground resizeMode="stretch" source={require("../../assets/starzBoardImage.png")} style={{width:screenWidth-4, height:"100%", flex:1}}>
              <View style={{flex:1}}>
                <View style={{flex:1, flexDirection:"row"}}>
                  <TouchableOpacity onPress={leftPress} style={{flex:1}}></TouchableOpacity>
                  <TouchableOpacity onPress={rightPress} style={{flex:1}}></TouchableOpacity>
                </View>
                <View style={{flex:1, flexDirection:"row"}}>
                  <TouchableOpacity onPress={bottomLeftPress} style={{flex:1}}></TouchableOpacity>
                  <TouchableOpacity onPress={bottomRightPress} style={{flex:1}}></TouchableOpacity>
                </View>
              </View>
          </ImageBackground>

      </Animatable.View>
    )
  }
}

export { StarzBoard };
