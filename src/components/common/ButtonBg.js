/* @flow weak */
import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  ImageBackground
} from 'react-native';
const {width, height} = Dimensions.get("window");
const fontRegular = "Montserrat-Regular"

const ButtonBg = ({label,onPress,color}) => (
  <View style={styles.buttonCont}>
    <TouchableOpacity
      activeOpacity={0.5}
        onPress={onPress}>
        <View style={[styles.loginBtnBg, {backgroundColor:color}]}>
          <Text style={styles.buttonLoginText}>{label}</Text>
        </View>
    </TouchableOpacity>
  </View>
);

const ButtonBg2 = ({label,onPress}) => (
  <View style={styles.buttonCont}>
    <TouchableOpacity
      activeOpacity={0.5}
        onPress={onPress}>
        <View style={styles.loginBtnBg2}>
          <Text style={styles.buttonLoginText2}>{label}</Text>
        </View>
    </TouchableOpacity>
  </View>
);

const ButtonSmall = ({label,onPress}) => (
  <View style={styles.buttonCont}>
    <TouchableOpacity
      activeOpacity={0.5}
        onPress={onPress}>
        <ImageBackground style={styles.loginBtnBg3} source={require('../../assets/smallButton.png')} resizeMode="stretch">
          <Text style={styles.buttonLoginText3}>{label}</Text>
        </ImageBackground>
    </TouchableOpacity>
  </View>
);

const styles = StyleSheet.create({
  buttonCont:{
    width:"80%"
  },
  loginBtnBg:{
    borderRadius:22,
    paddingVertical:12
  },
  buttonLoginText:{
    color:"#FFF",
    fontSize:15,
    textAlign:"center",
    fontWeight:"600"
  },
  loginBtnBg2:{
    height:55,
    justifyContent:"center",
    alignItems:"center",
    borderRadius:10,
    backgroundColor:"#FFF",
    marginVertical:10,
    width:width/1.3,
    borderWidth:0.5,
    borderColor:"#73879b"
  },
  buttonLoginText2:{
    color:"#73879b",
    fontSize:18,
    fontFamily:fontRegular
  },
  loginBtnBg3:{
    height:40,
    justifyContent:"center",
    alignItems:"center",
    marginVertical:5,
    width:100
  },
  buttonLoginText3:{
    color:"#FFF",
    fontSize:12,
    fontWeight:"bold",
    fontFamily:fontRegular
  }
})

export {ButtonBg, ButtonBg2, ButtonSmall};
