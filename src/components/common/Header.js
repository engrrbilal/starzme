/* @flow weak */
import React, {Component} from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  StyleSheet,
  Text,
  ImageBackground,
  Dimensions,
  Platform
} from 'react-native';

import { MenuIcon, EditIcon } from '../common';
import { Icon } from 'native-base';
const {width, height} = Dimensions.get("window");
const fontBold = "Montserrat-Regular"

class Header extends Component {
  render(){
    const {onLeftPress, onRightPress, headerText, rightIcon, bgColor} = this.props;
    if(onLeftPress == null){
      return (
        <View style={[styles.headerContainer, {backgroundColor:bgColor}]}>
            <View style={styles.statusBarBg}></View>
            <View style={{width: "100%", flexDirection: "row"}}>
              <View style={{ width: 50, height: 40, opacity: 0 }}><Icon style={styles.headerBackIcon} type="MaterialIcons" name="arrow-back" /></View>
              <Text style={styles.headerTitleText}>{headerText}</Text>
              <View style={{ width: 50, height: 40, opacity: 0 }}><Icon style={styles.headerBackIcon} type="MaterialIcons" name="arrow-back" /></View>
            </View>
        </View>
      )
    }
    else if(onLeftPress == null && onRightPress != null){
      return (
        <View style={[styles.headerContainer, {backgroundColor:bgColor}]}>
            <View style={styles.statusBarBg}></View>
            <View style={{width: "100%", flexDirection: "row"}}>
              <View style={{ width: 50, height: 40, opacity: 0 }}><Icon style={styles.headerBackIcon} type="MaterialIcons" name="arrow-back" /></View>
              <Text style={styles.headerTitleText}>{headerText}</Text>
              <View style={{ width: 50, height: 40, opacity: 0 }}><Icon style={styles.headerBackIcon} type="MaterialIcons" name="arrow-back" /></View>
            </View>
        </View>
      )
    }
    else if(onLeftPress != null && onRightPress != null){
      return (
        <View style={[styles.headerContainer, {backgroundColor:bgColor}]}>
            <View style={styles.statusBarBg}></View>
            <View style={{width: "100%", flexDirection: "row"}}>
              <TouchableOpacity onPress={onLeftPress}>
                <View style={{ width: 50, height: 40, alignItems: "center" }}><Icon style={styles.headerBackIcon} type="MaterialIcons" name="arrow-back" /></View>
              </TouchableOpacity>
              <Text style={styles.headerTitleText}>{headerText}</Text>
              <TouchableOpacity onPress={onRightPress}>
                <View style={{ width: 50, height: 40, alignItems: "center", marginRight:5 }}><Icon type="MaterialIcons" style={styles.headerBackIcon} name={rightIcon} /></View>
              </TouchableOpacity>
            </View>
        </View>
      )
    }
    else {
      return (
        <View style={[styles.headerContainer, {backgroundColor:bgColor}]}>
            <View style={styles.statusBarBg}></View>
            <View style={{width: "100%", flexDirection: "row"}}>
              <TouchableOpacity onPress={onLeftPress}>
                <View style={{ width: 50, height: 40, alignItems: "center" }}>
                  <Icon style={styles.headerBackIcon} type="MaterialIcons"  name="arrow-back" />
                </View>
              </TouchableOpacity>
              <Text style={styles.headerTitleText}>{headerText}</Text>
              <View style={{ width: 50, height: 40, opacity: 0 }}>
                <Icon style={styles.headerBackIcon} type="MaterialIcons" name="arrow-back" />
                </View>
            </View>
        </View>
      )
    }

  }
}

const styles = StyleSheet.create({
  headerContainer:{
    width:width,
    shadowColor:"#787878",
    shadowOffset:{width:0,height:2},
    shadowRadius:3,
    shadowOpacity:0.1
  },
  statusBarBg:{
    height:Platform.OS == 'ios' ? 45 : 25,
  },
  headerTitleText:{
    color:"#FFF",
    fontSize:20,
    textAlign:"center",
    textTransform:"uppercase",
    fontWeight:"bold",
    flex:1,
    lineHeight:25
  },
  headerBackIcon:{
    fontSize:25,
    color:"#FFF",
    width:50,
    textAlign:"center",
    marginLeft:5
  }
})

export { Header };
