/* @flow weak */
import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  ActivityIndicator,
  Image
} from 'react-native';

import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

class Mask extends Component {
  render(){
    return (
      <View style={{width:screenWidth, height:screenHeight, position:"absolute", zIndex:1000, justifyContent:"center", alignItems:"center",  backgroundColor: "rgba(0, 0, 0, 0.7)"}}>
        <View style={{flex:1, justifyContent:"center", alignItems:"center"}}>
            <ActivityIndicator size="large" color="#ff0000" />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({

})

export { Mask };
