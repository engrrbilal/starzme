/* @flow weak */
import React, {Component} from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  StyleSheet,
  Text,
  ImageBackground,
  Dimensions,
  Platform
} from 'react-native';

import { Icon } from 'native-base';
const {width, height} = Dimensions.get("window");
const IS_IPHONE_X = height === 812 || height === 896;
const fontBold = "Montserrat-Regular"

class BottomBar extends Component {

  render(){
    const {onHomePress,onAirPress,onUploadPress,onEventPress} = this.props;
    return (
      <View style={styles.bottomBar}>
        <View style={styles.bottomBarInner}>
            <TouchableOpacity activeOpacity={0.8} style={[styles.footerButtons, {backgroundColor:"#eb7f3e"}]} onPress={onHomePress}>
              <View style={{flex:1, alignItems: "center"}}>
                <Image source={require("../../assets/tabs/homeIcon.png")} style={styles.bottomBarIcons} />
                <Text style={styles.footerText}>Home</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity activeOpacity={0.8} style={[styles.footerButtons, {backgroundColor:"#e34769"}]} onPress={onAirPress}>
              <View style={{flex:1, alignItems: "center"}}>
                <Image source={require("../../assets/tabs/onAirIcon.png")} style={styles.bottomBarIcons} />
                <Text style={styles.footerText}>On Air</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity activeOpacity={0.8} style={[styles.footerButtons, {backgroundColor:"#fead39"}]} onPress={onUploadPress}>
              <View style={{flex:1, alignItems: "center"}}>
                <Image source={require("../../assets/tabs/uploadVideoIcon.png")} style={styles.bottomBarIcons} />
                <Text style={styles.footerText}>Upload Video</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity activeOpacity={0.8} style={[styles.footerButtons, {backgroundColor:"#9e4193"}]} onPress={onEventPress}>
              <View style={{flex:1, alignItems: "center"}}>
                <Image source={require("../../assets/tabs/eventsIcon.png")} style={styles.bottomBarIcons} />
                <Text style={styles.footerText}>Starz Events</Text>
              </View>
            </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  bottomBar:{
    width:"100%",
    height:80
  },
  bottomBarInner:{
    width:"100%",
    height:80,
    flexDirection:"row"
  },
  footerButtons:{
    flex:1,
    paddingTop:15
  },
  bottomBarIcons:{
    height: 20,
    width: 25,
    resizeMode:"contain"
  },
  footerText:{
    fontSize: 10,
    color:"#FFF",
    textAlign:"center",
    paddingTop:5
  }
})

export { BottomBar };
