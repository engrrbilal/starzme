/* @flow weak */
import React, {Component} from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  StyleSheet,
  Text,
  ImageBackground,
  Dimensions,
  Platform,
  TextInput
} from 'react-native';

import { Icon } from 'native-base';
const {width, height} = Dimensions.get("window");
const fontBold = "Montserrat-Regular"

class SearchDropDown extends Component {
  render(){
    const {text, onRightPress} = this.props;
    return (
      <View style={{width:"100%", alignItems:"center", justifyContent:"center", paddingVertical:20}}>
        <View style={{width:"80%", height:40, backgroundColor:"#FFF", borderRadius:30, paddingHorizontal:10}}>
          <View style={{flexDirection:"row", width:"100%"}}>
            <View style={{width:25, height:40, alignItems:"center", justifyContent:"center"}}>
              <Icon type="FontAwesome" style={{fontSize:15, textAlign:"right", color:"#908f8f", fontWeight:"bold"}} name="search" />
            </View>
            <TextInput
            style={{flex:1, paddingHorizontal:5, color:"#000", fontSize:14, height:40}}
            placeholderTextColor={"#908f8f"}
            returnKeyType="search"
            underlineColorAndroid="transparent"
            onChangeText={(text) => this.searchText = text}
            placeholder="Search" />
            <View style={{width:0.7, height:20, backgroundColor:"#908f8f", marginTop:10}}></View>
            <TouchableOpacity onPress={onRightPress}>
              <View style={{width:35, height:40, alignItems:"center", justifyContent:"center"}}>
                <Icon type="Ionicons" style={{fontSize:20, textAlign:"right", color:"#f9413f"}} name="ios-options" />
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }
}

export { SearchDropDown };
