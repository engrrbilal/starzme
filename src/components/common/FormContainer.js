/* @flow weak */
import React from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  StyleSheet,
  Text,
  Platform
} from 'react-native';

const FormContainer = ({children}) => (
  <View style={styles.mainContainer}>
    <View style={styles.container}>
      {children}
    </View>
  </View>
);

const styles = StyleSheet.create({
  mainContainer:{
    marginVertical:20,
    flexDirection:"column"
  },
  container:{
    backgroundColor:"#FFF",
    shadowColor:"#787878",
    shadowOffset:{width:0,height:5},
    shadowRadius:10,
    shadowOpacity:0.5,
    elevation:Platform.OS == 'ios' ? 3 : null,
    paddingVertical:5,
    borderRadius:20
  },
  formButtonCont:{
    width:"100%",
    alignItems:"center",
    justifyContent:"center",
    marginTop:-40,
    zIndex:999
  },
  formButton:{
    width:80,
    height:80
  }
})

export { FormContainer };
