/* @flow weak */
import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  ActivityIndicator,
  Image,
  Platform,
  ImageBackground
} from 'react-native';

import * as Animatable from 'react-native-animatable';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
const fontRegular = "Montserrat-Regular"

class CustomPopup extends Component {
  render(){
    const {title,leftPress,leftText,rightPress,rightText} = this.props;
    return (
      <View style={{width:screenWidth, height:screenHeight, position:"absolute", zIndex:9999, backgroundColor:"rgba(0,0,0,0.8)"}}>
      <Animatable.View
      style={{flex:1, justifyContent:"center", alignItems:"center", top:screenHeight/3.5}}
      animation="zoomIn" iterationCount={1} delay={200} duration={300} direction="alternate">

      <View style={{flex:1}}>
      <ImageBackground resizeMode="stretch" source={require("../../assets/popupBg.png")} style={{width:screenWidth/1.2}}>
        <ImageBackground resizeMode="contain" source={require("../../assets/imageContainer.png")} style={{width:screenWidth/1.2, height:screenWidth/3, top:-55}}>
            <View style={{width:"100%", alignItems:"center", justifyContent:"center", height:screenWidth/3.3}}>
                <Image source={require("../../assets/profileIcon.png")} style={{width:screenWidth/4.5, height:screenWidth/4.5, resizeMode:"contain"}} />
            </View>
        </ImageBackground>

                <View style={[{width:"100%", top:-40}]}>
                  <View style={{paddingHorizontal:5}}>
                      <Text style={{color:"#666666", fontSize:16, paddingBottom:5, textAlign:"center", fontFamily:fontRegular}}>{title}</Text>
                  </View>
                </View>

                <View style={{width:"100%", flexDirection:"row", justifyContent:"center", marginBottom:30}}>

                  <TouchableOpacity style={{width:"40%"}} onPress={leftPress}>
                      <View style={{backgroundColor:"#fd3a40",
                      paddingVertical:10,
                      borderRadius:15,
                      width:"90%",
                      marginHorizontal:"2%",
                      paddingHorizontal:15}}>

                          <View style={{width:"100%"}}>
                            <Text style={{color:"#FFF",
                            width:"100%",
                            textAlign:"center",
                            fontFamily:fontRegular,
                            fontSize:14}}>{leftText}</Text>
                          </View>

                      </View>
                  </TouchableOpacity>

                  <TouchableOpacity style={{width:"40%"}} onPress={rightPress}>
                      <View style={{backgroundColor:"#7755cd",
                      paddingVertical:10,
                      borderRadius:15,
                      width:"90%",
                      marginHorizontal:"2%",
                      paddingHorizontal:15}}>

                          <View style={{width:"100%"}}>
                            <Text style={{color:"#FFF",
                            width:"100%",
                            textAlign:"center",
                            fontFamily:fontRegular,
                            fontSize:14}}>{rightText}</Text>
                          </View>

                      </View>
                  </TouchableOpacity>

                </View>

      </ImageBackground>
      </View>
      </Animatable.View>
      </View>

    )
  }
}

export { CustomPopup };
