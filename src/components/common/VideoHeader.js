/* @flow weak */
import React, { Component } from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  StyleSheet,
  Text,
  ImageBackground,
  Dimensions,
  Platform,
  SafeAreaView
} from 'react-native';

import { MenuIcon, EditIcon } from '../common';
//import Icon from 'react-native-vector-icons/Ionicons';
import { Icon } from 'native-base';
const { width, height } = Dimensions.get("window");
const fontBold = "Montserrat-Regular"

class VideoHeader extends Component {
  render() {
    const { onLeftPress, onRightPress, headerText, rightIcon, bgColor, rightIcon2, rightIcon3, type = null } = this.props
    if (type == "onAir") {
      return (
        <SafeAreaView style={[styles.headerContainer, { backgroundColor: bgColor }]}>
          <View style={{ width: "100%", flexDirection: "row", paddingHorizontal: 20 }}>
            <TouchableOpacity onPress={onLeftPress}>
              <Icon style={styles.headerBackIcon} type="MaterialIcons" name="arrow-back" />
            </TouchableOpacity>
          </View>
        </SafeAreaView>
      )
    }
    else {
      return (
        <View style={[styles.headerContainer, { backgroundColor: bgColor }]}>
          <View style={styles.statusBarBg}></View>
          <View style={{ width: "100%", flexDirection: "row",marginTop:40 }}>
            {/* <View style={{ width: 10, height: 40 }}></View> */}
            <TouchableOpacity onPress={onLeftPress}>
              <View style={{ width: 50, height: 40, alignItems: "center" }}>
                <Icon style={styles.headerBackIcon} type="MaterialIcons" name="arrow-back" />
              </View>
            </TouchableOpacity>
            <View style={{ width: 30, height: 25, alignItems: "center", justifyContent: "center" }}><Image style={{ width: 22, height: 22, resizeMode: "contain", borderRadius: 11, borderWidth: 1, borderColor: "#FFF" }} source={require("../../assets/profileIcon.png")} /></View>
            <Text style={styles.headerTitleText}>{headerText}</Text>
            <TouchableOpacity>
              <View style={{ width: 40, height: 40, alignItems: "center", marginRight: 5 }}><Icon type="FontAwesome" style={[styles.headerBackIcon, { width: 40 }]} name={rightIcon2} /></View>
            </TouchableOpacity>
            <TouchableOpacity>
              <View style={{ width: 40, height: 40, alignItems: "center", marginRight: 5 }}><Icon type="FontAwesome" style={[styles.headerBackIcon, { width: 40 }]} name={rightIcon3} /></View>
            </TouchableOpacity>
            <TouchableOpacity onPress={onRightPress}>
              <View style={{ width: 40, height: 40, alignItems: "center", marginRight: 5 }}><Icon style={[styles.headerBackIcon, { width: 40 }]} name={rightIcon} /></View>
            </TouchableOpacity>
            <View style={{ width: 20, height: 40 }}></View>
          </View>
        </View>
      )
    }

  }
}

const styles = StyleSheet.create({
  headerContainer: {
    width: width,
  },
  statusBarBg: {
    height: Platform.OS == 'ios' ? 45 : 25,
  },
  headerTitleText: {
    color: "#FFF",
    fontSize: 20,
    textAlign: "center",
    textTransform: "uppercase",
    fontWeight: "bold",
    flex: 1,
    lineHeight: 25
  },
  headerBackIcon: {
    fontSize: 25,
    color: "#f8f8f8",
    textAlign: "center",
    fontWeight:"bold"
  }
})

export { VideoHeader };
