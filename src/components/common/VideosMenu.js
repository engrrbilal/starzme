/* @flow weak */
import React, { Component } from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  StyleSheet,
  Text,
  ImageBackground,
  Dimensions,
  Platform,
  FlatList
} from 'react-native';

import { Icon } from 'native-base';
const { width, height } = Dimensions.get("window");
const fontBold = "Montserrat-Regular"

class VideosMenu extends Component {
  render() {
    const { onPress, selected, catData } = this.props;
    return (
      <View style={{ width: "100%" }}>
        <FlatList
          showsHorizontalScrollIndicator={false}
          keyExtractor={(item, index) => index}
          data={catData}
          style={{ width: "100%", paddingVertical: 10 }}
          horizontal={true}
          renderItem={this._renderCategories}
        />
      </View>
    )
  }

  _renderCategories = ({ item, index }) => {
    const { selected, onPress } = this.props;
    var catTitle = item.category_name
    if (index == selected) {
      return (
        <TouchableOpacity onPress={() => onPress(index, item.type)}>
          <View style={{ width: width / 3 }}>
            <Text numberOfLines={2} style={{ color: "#FFF", fontSize: 15, paddingTop: 10, textAlign: "center", fontWeight: "600" }}>{catTitle}</Text>
            <Image style={{ width: width / 3, height: 15, resizeMode: "contain" }} source={require("../../assets/selectedBorder.png")} />
          </View>
        </TouchableOpacity>
      );
    }
    else {
      return (
        <TouchableOpacity onPress={() => onPress(index, item.type)}>
          <View style={{ width: width / 3 }}>
            <Text numberOfLines={2} style={{ color: "#FFF", fontSize: 13, paddingVertical: 12, textAlign: "center" }}>{catTitle}</Text>
          </View>
        </TouchableOpacity>
      );
    }
  }
}

const styles = StyleSheet.create({
  isSelected: {
    width: width,
    shadowColor: "#528abd",
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 20,
    shadowOpacity: 0.9,
    borderWidth: 1,
    borderColor: "#00fffe",
    borderRadius: 5
  },
  notSelected: {
    borderRadius: 2
  }
})

export { VideosMenu };
