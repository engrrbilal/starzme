/* @flow weak */
import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  ActivityIndicator
} from 'react-native';

const {width, height} = Dimensions.get("window");
const IS_IPHONE_X = height === 812 || height === 896;

const marginMinusSmall = -(height/15)
const marginMinusBig = -(height/14)

import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

var placeHolderImage = require("../../assets/placeholder.png");

class PlaceHolderSmall extends Component {

  render(){
    const {text} = this.props;
    return (
      <View style={{flex:1}}>
          <View style={{flex:1, justifyContent:"center", alignItems:"center"}}>
                <Image style={{width:100, height:100, resizeMode:"contain"}} source={placeHolderImage} />
                <Text style={{textAlign:"center", paddingVertical:10, color:"#765252", fontSize:15, fontFamily:"Montserrat-Light"}}>{text}</Text>
          </View>
      </View>
    )
  }

}

class PlaceHolder extends Component {

  render(){
    const {text} = this.props;
    return (
      <View style={[styles.marginTopMinus, {flex:1, paddingHorizontal:20}]}>
        <View style={styles.cardView}>
            <View style={{flex:1, justifyContent:"center", alignItems:"center"}}>
                  <Image style={{width:100, height:100, resizeMode:"contain"}} source={placeHolderImage} />
                  <Text style={{textAlign:"center", paddingVertical:10, color:"#765252", fontSize:15, fontFamily:"Montserrat-Light"}}>{text}</Text>
            </View>
        </View>
      </View>
    )
  }

}

const styles = StyleSheet.create({
  cardView:{
    flex:1,
    borderRadius:20,
    backgroundColor:"#FFF",
    paddingHorizontal:20,
    paddingVertical:20,
    shadowColor:"#787878",
    shadowOffset:{width:0,height:3},
    shadowRadius:5,
    shadowOpacity:0.2
  },
  marginTopMinus:{
    marginTop:IS_IPHONE_X === true ? marginMinusBig : marginMinusSmall
  }
})

export { PlaceHolder, PlaceHolderSmall };
