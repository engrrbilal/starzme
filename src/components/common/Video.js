/*Example of React Native Video*/
import React, { Component } from 'react';
//Import React
import { Platform, StyleSheet, Text, View } from 'react-native';
//Import Basic React Native Component
// import Video from 'react-native-video';
import VideoPlayer from 'react-native-video-player';
//Import React Native Video to play video

export default class VideoPlayerHandler extends Component {

    constructor(props) {
        super(props);
        this.state = {
            currentTime: 0,
            duration: 0,
            isLoading: true,
        };
    }

    // onSeek = seek => {
    //     //Handler for change in seekbar
    //     this.videoPlayer.seek(seek);
    // };

    onPaused = () => {
        //Handler for Video Pause
        // console.log("#this.state.paused : ", this.state.paused)
        this.setState({
            paused: !this.state.paused
        });
    };

    onProgress = data => {
        const { isLoading } = this.state;
        // Video Player will continue progress even if the video already ended
        console.log("#data onprogress : ", data)
        if (!isLoading) {
            this.setState({ currentTime: parseInt(data.currentTime) });
        }
    };

    onLoad = data => this.setState({ duration: parseInt(data.duration), isLoading: false }, () => {
        console.log("#data onload: ", data)
    });

    onLoadStart = data => this.setState({ isLoading: true }, () => {
        console.log("#data : ", data)
    });
    onAnimationComplete = () => {
        console.log("onAnimationComplete")
        if (this.state.currentTime != this.state.duration) {
            this.setState({ currentTime: this.state.duration })
        }
    }

    onEnd = () => {
        console.log("Video ended : ", this.state.currentTime)
        this.props.onEnd()
    };

    onError = () => alert('something went wrong! ', error);
    // onSeeking = currentTime => this.setState({ currentTime });

    render() {
        console.log("#this.props.videoUrl : ", this.props.videoUrl)
        return (
            <View style={styles.videoContainer}>

                {/* <Video
                    source={{ uri: this.props.videoUrl }}
                    ref={(ref) => this.player = ref}
                    repeat={false}
                    // onLoad={() => {
                    //     this.setState({
                    //         videoLoaded: true
                    //     })
                    // }}
                    controls={true}
                    onEnd={() => this.player.seek(0)}
                    resizeMode={'contain'}
                    // onEnd={this.onEnd}
                    onStart={this.onLoad}
                    onPlayPress={this.onLoadStart}
                    onProgress={this.onProgress}
                // // paused={this.props.paused}
                // resizeMode="contain"
                // style={styles.video}
                // volume={10}
                /> */}
                {console.log("#this.props.videoUrl r: ", this.props.videoUrl)}
                <VideoPlayer
                    video={{ uri: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4" }}
                    // video={{ uri: this.props.videoUrl }}
                    videoWidth={1600}
                    videoHeight={900}
                    // onStart={this.onLoad}
                    // onPlayPress={this.onLoadStart}
                    // resizeMode="contain"
                    autoplay={true}
                    thumbnail={{ uri: 'https://i.picsum.photos/id/866/1600/900.jpg' }}
                />
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    videoContainer: {
        // display: "flex",
        justifyContent: "center",
        alignItems: "center"
    },
    video: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: "transparent"
    }
});