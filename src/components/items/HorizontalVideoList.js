/* @flow weak */
import React, {Component} from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  StyleSheet,
  Text,
  ImageBackground,
  Dimensions,
  Platform,
  FlatList
} from 'react-native';

import { Icon } from 'native-base';
const {width, height} = Dimensions.get("window");
const fontBold = "Montserrat-Regular"
import * as Animatable from 'react-native-animatable';

const Videos = [
  {"image":"https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Two_dancers.jpg/220px-Two_dancers.jpg"},
  {"image":"https://i.guim.co.uk/img/media/dfd56d28e5b8546c9f2cac7362d03256378cd81a/253_47_1880_2348/master/1880.jpg?width=700&quality=85&auto=format&fit=max&s=ad00c490c7ed5c63435657d811e2bc18"},
  {"image":"https://www.fulcrumgallery.com/product-images/P937983-10/ballerina-in-red-detail.jpg"},
  {"image":"https://upload.wikimedia.org/wikipedia/commons/thumb/6/64/Polonezkoy_08859_nevit.jpg/364px-Polonezkoy_08859_nevit.jpg"},
  {"image":"https://cdn.britannica.com/s:500x350/53/9953-004-02360894/Catherine-Wheel-Twyla-Tharp-1981.jpg"},
  {"image":"https://upload.wikimedia.org/wikipedia/commons/thumb/2/2d/Festival_des_Vieilles_Charrues_2018_-_Ang%C3%A8le_-_004-2.jpg/250px-Festival_des_Vieilles_Charrues_2018_-_Ang%C3%A8le_-_004-2.jpg"},
  {"image":"https://www.smashinglists.com/wp-content/uploads/2012/09/Shoulders-Back.jpg"},
  {"image":"https://assets.rebelmouse.io/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpbWFnZSI6Imh0dHBzOi8vYXNzZXRzLnJibC5tcy8yMTcxMTY4OC8yMDAweC5qcGciLCJleHBpcmVzX2F0IjoxNjExNjU4MTI3fQ.uamRGI_B_EpQTgVsnWiLdNSqeNThp1HNGYIJzavyTlU/img.jpg?width=523&height=305"}
]

class HorizontalVideoList extends Component {

  render(){
    const {stateName} = this.props;
    return (
      <View style={{width:"100%"}}>
        <Text numberOfLines={1} style={{color: "#FFF", fontSize: 16, paddingTop: 10, textAlign:"center"}}>{stateName} 2020</Text>
        <FlatList
          data={Videos}
          style={{width:"100%", paddingVertical:10}}
          horizontal={true}
          renderItem={this._renderList} />
      </View>
    )
  }

  _renderList = ({ item, index }) => {
    var _index = Math.floor(Math.random() * Videos.length)
    var catTitle = Videos[_index].image
    var animationDelay = index*50

    var m = index+1
    var monthText = "0"+m

    return (
      <Animatable.View
      animation="zoomIn"
      iterationCount={1}
      style={{width:width/3, height:width/3.5, marginRight:6}}
      direction="alternate"
      duration={500}
      delay={animationDelay}>
        <TouchableOpacity activeOpacity={1}>
          <View style={{ width:width/3, height:width/3.5, position:"relative" }}>
            <ImageBackground source={require("../../assets/calendarIcon.png")} style={{position:"absolute", width:30, height:30, top:0, right:0, alignItems:"center", justifyContent:"center", zIndex:999}}>
                <Text style={{textAlign:"center", color:"#000", fontSize:13}}>{monthText}</Text>
            </ImageBackground>
            <Image style={{ width:width/3, height:width/3.5, borderWidth:1, borderColor:"#FFF" }} source={{uri:catTitle}} />
          </View>
        </TouchableOpacity>
      </Animatable.View>
    );
  }

}

const styles = StyleSheet.create({

})

export { HorizontalVideoList };
