export default [
    {"lable": "Afghanistan", "value": "AF"},
    {"lable": "Åland Islands", "value": "AX"},
    {"lable": "Albania", "value": "AL"},
    {"lable": "Algeria", "value": "DZ"},
    {"lable": "American Samoa", "value": "AS"},
    {"lable": "AndorrA", "value": "AD"},
    {"lable": "Angola", "value": "AO"},
    {"lable": "Anguilla", "value": "AI"},
    {"lable": "Antarctica", "value": "AQ"},
    {"lable": "Antigua and Barbuda", "value": "AG"},
    {"lable": "Argentina", "value": "AR"},
    {"lable": "Armenia", "value": "AM"},
    {"lable": "Aruba", "value": "AW"},
    {"lable": "Australia", "value": "AU"},
    {"lable": "Austria", "value": "AT"},
    {"lable": "Azerbaijan", "value": "AZ"},
    {"lable": "Bahamas", "value": "BS"},
    {"lable": "Bahrain", "value": "BH"},
    {"lable": "Bangladesh", "value": "BD"},
    {"lable": "Barbados", "value": "BB"},
    {"lable": "Belarus", "value": "BY"},
    {"lable": "Belgium", "value": "BE"},
    {"lable": "Belize", "value": "BZ"},
    {"lable": "Benin", "value": "BJ"},
    {"lable": "Bermuda", "value": "BM"},
    {"lable": "Bhutan", "value": "BT"},
    {"lable": "Bolivia", "value": "BO"},
    {"lable": "Bosnia and Herzegovina", "value": "BA"},
    {"lable": "Botswana", "value": "BW"},
    {"lable": "Bouvet Island", "value": "BV"},
    {"lable": "Brazil", "value": "BR"},
    {"lable": "British Indian Ocean Territory", "value": "IO"},
    {"lable": "Brunei Darussalam", "value": "BN"},
    {"lable": "Bulgaria", "value": "BG"},
    {"lable": "Burkina Faso", "value": "BF"},
    {"lable": "Burundi", "value": "BI"},
    {"lable": "Cambodia", "value": "KH"},
    {"lable": "Cameroon", "value": "CM"},
    {"lable": "Canada", "value": "CA"},
    {"lable": "Cape Verde", "value": "CV"},
    {"lable": "Cayman Islands", "value": "KY"},
    {"lable": "Central African Republic", "value": "CF"},
    {"lable": "Chad", "value": "TD"},
    {"lable": "Chile", "value": "CL"},
    {"lable": "China", "value": "CN"},
    {"lable": "Christmas Island", "value": "CX"},
    {"lable": "Cocos (Keeling) Islands", "value": "CC"},
    {"lable": "Colombia", "value": "CO"},
    {"lable": "Comoros", "value": "KM"},
    {"lable": "Congo", "value": "CG"},
    {"lable": "Congo, The Democratic Republic of the", "value": "CD"},
    {"lable": "Cook Islands", "value": "CK"},
    {"lable": "Costa Rica", "value": "CR"},
    {"lable": "Cote D'Ivoire", "value": "CI"},
    {"lable": "Croatia", "value": "HR"},
    {"lable": "Cuba", "value": "CU"},
    {"lable": "Cyprus", "value": "CY"},
    {"lable": "Czech Republic", "value": "CZ"},
    {"lable": "Denmark", "value": "DK"},
    {"lable": "Djibouti", "value": "DJ"},
    {"lable": "Dominica", "value": "DM"},
    {"lable": "Dominican Republic", "value": "DO"},
    {"lable": "Ecuador", "value": "EC"},
    {"lable": "Egypt", "value": "EG"},
    {"lable": "El Salvador", "value": "SV"},
    {"lable": "Equatorial Guinea", "value": "GQ"},
    {"lable": "Eritrea", "value": "ER"},
    {"lable": "Estonia", "value": "EE"},
    {"lable": "Ethiopia", "value": "ET"},
    {"lable": "Falkland Islands (Malvinas)", "value": "FK"},
    {"lable": "Faroe Islands", "value": "FO"},
    {"lable": "Fiji", "value": "FJ"},
    {"lable": "Finland", "value": "FI"},
    {"lable": "France", "value": "FR"},
    {"lable": "French Guiana", "value": "GF"},
    {"lable": "French Polynesia", "value": "PF"},
    {"lable": "French Southern Territories", "value": "TF"},
    {"lable": "Gabon", "value": "GA"},
    {"lable": "Gambia", "value": "GM"},
    {"lable": "Georgia", "value": "GE"},
    {"lable": "Germany", "value": "DE"},
    {"lable": "Ghana", "value": "GH"},
    {"lable": "Gibraltar", "value": "GI"},
    {"lable": "Greece", "value": "GR"},
    {"lable": "Greenland", "value": "GL"},
    {"lable": "Grenada", "value": "GD"},
    {"lable": "Guadeloupe", "value": "GP"},
    {"lable": "Guam", "value": "GU"},
    {"lable": "Guatemala", "value": "GT"},
    {"lable": "Guernsey", "value": "GG"},
    {"lable": "Guinea", "value": "GN"},
    {"lable": "Guinea-Bissau", "value": "GW"},
    {"lable": "Guyana", "value": "GY"},
    {"lable": "Haiti", "value": "HT"},
    {"lable": "Heard Island and Mcdonald Islands", "value": "HM"},
    {"lable": "Holy See (Vatican City State)", "value": "VA"},
    {"lable": "Honduras", "value": "HN"},
    {"lable": "Hong Kong", "value": "HK"},
    {"lable": "Hungary", "value": "HU"},
    {"lable": "Iceland", "value": "IS"},
    {"lable": "India", "value": "IN"},
    {"lable": "Indonesia", "value": "ID"},
    {"lable": "Iran, Islamic Republic Of", "value": "IR"},
    {"lable": "Iraq", "value": "IQ"},
    {"lable": "Ireland", "value": "IE"},
    {"lable": "Isle of Man", "value": "IM"},
    {"lable": "Israel", "value": "IL"},
    {"lable": "Italy", "value": "IT"},
    {"lable": "Jamaica", "value": "JM"},
    {"lable": "Japan", "value": "JP"},
    {"lable": "Jersey", "value": "JE"},
    {"lable": "Jordan", "value": "JO"},
    {"lable": "Kazakhstan", "value": "KZ"},
    {"lable": "Kenya", "value": "KE"},
    {"lable": "Kiribati", "value": "KI"},
    {"lable": "Korea, Democratic People'S Republic of", "value": "KP"},
    {"lable": "Korea, Republic of", "value": "KR"},
    {"lable": "Kuwait", "value": "KW"},
    {"lable": "Kyrgyzstan", "value": "KG"},
    {"lable": "Lao People'S Democratic Republic", "value": "LA"},
    {"lable": "Latvia", "value": "LV"},
    {"lable": "Lebanon", "value": "LB"},
    {"lable": "Lesotho", "value": "LS"},
    {"lable": "Liberia", "value": "LR"},
    {"lable": "Libyan Arab Jamahiriya", "value": "LY"},
    {"lable": "Liechtenstein", "value": "LI"},
    {"lable": "Lithuania", "value": "LT"},
    {"lable": "Luxembourg", "value": "LU"},
    {"lable": "Macao", "value": "MO"},
    {"lable": "Macedonia, The Former Yugoslav Republic of", "value": "MK"},
    {"lable": "Madagascar", "value": "MG"},
    {"lable": "Malawi", "value": "MW"},
    {"lable": "Malaysia", "value": "MY"},
    {"lable": "Maldives", "value": "MV"},
    {"lable": "Mali", "value": "ML"},
    {"lable": "Malta", "value": "MT"},
    {"lable": "Marshall Islands", "value": "MH"},
    {"lable": "Martinique", "value": "MQ"},
    {"lable": "Mauritania", "value": "MR"},
    {"lable": "Mauritius", "value": "MU"},
    {"lable": "Mayotte", "value": "YT"},
    {"lable": "Mexico", "value": "MX"},
    {"lable": "Micronesia, Federated States of", "value": "FM"},
    {"lable": "Moldova, Republic of", "value": "MD"},
    {"lable": "Monaco", "value": "MC"},
    {"lable": "Mongolia", "value": "MN"},
    {"lable": "Montserrat", "value": "MS"},
    {"lable": "Morocco", "value": "MA"},
    {"lable": "Mozambique", "value": "MZ"},
    {"lable": "Myanmar", "value": "MM"},
    {"lable": "Namibia", "value": "NA"},
    {"lable": "Nauru", "value": "NR"},
    {"lable": "Nepal", "value": "NP"},
    {"lable": "Netherlands", "value": "NL"},
    {"lable": "Netherlands Antilles", "value": "AN"},
    {"lable": "New Caledonia", "value": "NC"},
    {"lable": "New Zealand", "value": "NZ"},
    {"lable": "Nicaragua", "value": "NI"},
    {"lable": "Niger", "value": "NE"},
    {"lable": "Nigeria", "value": "NG"},
    {"lable": "Niue", "value": "NU"},
    {"lable": "Norfolk Island", "value": "NF"},
    {"lable": "Northern Mariana Islands", "value": "MP"},
    {"lable": "Norway", "value": "NO"},
    {"lable": "Oman", "value": "OM"},
    {"lable": "Pakistan", "value": "PK"},
    {"lable": "Palau", "value": "PW"},
    {"lable": "Palestinian Territory, Occupied", "value": "PS"},
    {"lable": "Panama", "value": "PA"},
    {"lable": "Papua New Guinea", "value": "PG"},
    {"lable": "Paraguay", "value": "PY"},
    {"lable": "Peru", "value": "PE"},
    {"lable": "Philippines", "value": "PH"},
    {"lable": "Pitcairn", "value": "PN"},
    {"lable": "Poland", "value": "PL"},
    {"lable": "Portugal", "value": "PT"},
    {"lable": "Puerto Rico", "value": "PR"},
    {"lable": "Qatar", "value": "QA"},
    {"lable": "Reunion", "value": "RE"},
    {"lable": "Romania", "value": "RO"},
    {"lable": "Russian Federation", "value": "RU"},
    {"lable": "RWANDA", "value": "RW"},
    {"lable": "Saint Helena", "value": "SH"},
    {"lable": "Saint Kitts and Nevis", "value": "KN"},
    {"lable": "Saint Lucia", "value": "LC"},
    {"lable": "Saint Pierre and Miquelon", "value": "PM"},
    {"lable": "Saint Vincent and the Grenadines", "value": "VC"},
    {"lable": "Samoa", "value": "WS"},
    {"lable": "San Marino", "value": "SM"},
    {"lable": "Sao Tome and Principe", "value": "ST"},
    {"lable": "Saudi Arabia", "value": "SA"},
    {"lable": "Senegal", "value": "SN"},
    {"lable": "Serbia and Montenegro", "value": "CS"},
    {"lable": "Seychelles", "value": "SC"},
    {"lable": "Sierra Leone", "value": "SL"},
    {"lable": "Singapore", "value": "SG"},
    {"lable": "Slovakia", "value": "SK"},
    {"lable": "Slovenia", "value": "SI"},
    {"lable": "Solomon Islands", "value": "SB"},
    {"lable": "Somalia", "value": "SO"},
    {"lable": "South Africa", "value": "ZA"},
    {"lable": "South Georgia and the South Sandwich Islands", "value": "GS"},
    {"lable": "Spain", "value": "ES"},
    {"lable": "Sri Lanka", "value": "LK"},
    {"lable": "Sudan", "value": "SD"},
    {"lable": "Surilable", "value": "SR"},
    {"lable": "Svalbard and Jan Mayen", "value": "SJ"},
    {"lable": "Swaziland", "value": "SZ"},
    {"lable": "Sweden", "value": "SE"},
    {"lable": "Switzerland", "value": "CH"},
    {"lable": "Syrian Arab Republic", "value": "SY"},
    {"lable": "Taiwan, Province of China", "value": "TW"},
    {"lable": "Tajikistan", "value": "TJ"},
    {"lable": "Tanzania, United Republic of", "value": "TZ"},
    {"lable": "Thailand", "value": "TH"},
    {"lable": "Timor-Leste", "value": "TL"},
    {"lable": "Togo", "value": "TG"},
    {"lable": "Tokelau", "value": "TK"},
    {"lable": "Tonga", "value": "TO"},
    {"lable": "Trinidad and Tobago", "value": "TT"},
    {"lable": "Tunisia", "value": "TN"},
    {"lable": "Turkey", "value": "TR"},
    {"lable": "Turkmenistan", "value": "TM"},
    {"lable": "Turks and Caicos Islands", "value": "TC"},
    {"lable": "Tuvalu", "value": "TV"},
    {"lable": "Uganda", "value": "UG"},
    {"lable": "Ukraine", "value": "UA"},
    {"lable": "United Arab Emirates", "value": "AE"},
    {"lable": "United Kingdom", "value": "GB"},
    {"lable": "United States", "value": "US"},
    {"lable": "United States Minor Outlying Islands", "value": "UM"},
    {"lable": "Uruguay", "value": "UY"},
    {"lable": "Uzbekistan", "value": "UZ"},
    {"lable": "Vanuatu", "value": "VU"},
    {"lable": "Venezuela", "value": "VE"},
    {"lable": "Viet Nam", "value": "VN"},
    {"lable": "Virgin Islands, British", "value": "VG"},
    {"lable": "Virgin Islands, U.S.", "value": "VI"},
    {"lable": "Wallis and Futuna", "value": "WF"},
    {"lable": "Western Sahara", "value": "EH"},
    {"lable": "Yemen", "value": "YE"},
    {"lable": "Zambia", "value": "ZM"},
    {"lable": "Zimbabwe", "value": "ZW"}
    ]