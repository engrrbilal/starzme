import React from 'react';
const initState = {
	data: 'initial data',
	loading:true
};
import getUserDataService from '../../services/getUserData';
export default (state = initState, action) => {
	switch (action.type) {
		case "GETUSERS":
			return{
				loading:true,
				data:getUserDataService(),
            }
        case "GETUSERSUCCESS":
            return{
                loading:true,
                data:action.payload,
            }
		default:
			return state;
	}
};