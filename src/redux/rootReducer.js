import { combineReducers } from "redux";
import userData from './reducers/getUser';
const appReducer = combineReducers({
    UserData:userData,
})
export default appReducer;