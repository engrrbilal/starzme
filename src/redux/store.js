import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { createEpicMiddleware } from 'redux-observable';
import MiddleWare from './middleware';
import rootReducer from './rootReducer';
const epicMiddleware = createEpicMiddleware();
const store = createStore(rootReducer,applyMiddleware(MiddleWare, thunk, epicMiddleware));
export default store;