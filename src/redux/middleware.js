import Headers from '../utils/globalApiHeader';
import baseURL from '../utils/baseUrl';
import { getUserDetails } from './globalActions';
const MiddleWare = store => next => action => {
     console.log("Middleware triggered:", action);
     next(action);
}
export default MiddleWare;