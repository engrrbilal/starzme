/* @flow */
import React, { Component } from 'react';
import {
  View,
  KeyboardAvoidingView,
  ScrollView,
  Image,
  TextInput,
  Text,
  ImageBackground,
  Dimensions,
  ActivityIndicator,
  AsyncStorage,
  TouchableOpacity,
  FlatList,
  StatusBar,
  WebView
} from 'react-native';
import styles from '../styles/style';
import { NavigationActions } from 'react-navigation';
import { Baseurl, Mask, Header } from '../components/common';
import {Toast, Icon, ActionSheet} from 'native-base';
const {width, height} = Dimensions.get("window");

export default class GoogleMap extends Component {
constructor(props) {
      super(props);
      const {state} = props.navigation;
      this.state = {
        loadMask:true,
        headerText:"Location",
        lat:state.params.gymLat,
        long:state.params.gymLong
      }
}

componentDidMount(){
  setTimeout(()=>{
      this.setState({loadMask:false})
  },2000)
}

_renderMask(){
    if(this.state.loadMask == true){
        return (
          <Mask />
        )
    }
}

render() {
    return (
      <View style={[styles.dashboardMainView]}>
       {this._renderMask()}
        <Header leftIcon="back" headerText={this.state.headerText} onLeftPress={()=> this.props.navigation.goBack()} onRightPress={null} />
        <View style={styles.dashboardContainerView}>
          <ScrollView showsVerticalScrollIndicator={false} style={styles.marginTopMinus}>
            <View style={{flex:1, paddingHorizontal:10}}>
                <View style={[styles.cardView, {paddingHorizontal:10, paddingVertical:5}]}>
                  <WebView
                   source={{ uri: "https://www.google.com/maps/search/?api=1&query="+this.state.lat+","+this.state.long }}
                   style={{ backgroundColor: '#000', flex: 1, width:width-40, height:(height-height/5) }}
                 />
                </View>
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}
