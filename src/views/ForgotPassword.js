/* @flow */
import React, { Component } from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  ImageBackground,
  ActivityIndicator,
  AsyncStorage,
  Dimensions,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  Keyboard,
  Platform
} from 'react-native';
import { MainBg, AppLogo, FormContainer, LoginUserIcon, LoginPassIcon, RegisterEmailIcon, RegisterGenderIcon, Mask, Baseurl } from '../components/common';
import styles,{width} from '../styles/style';
import utils from '../utils';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

export default class ForgotPassword extends Component {
constructor(props) {
        super(props);
        const {state} = props.navigation;

        this.state = {
          isKey:0,
          loadMask:false
        }
        this._keyboardDidShow = this._keyboardDidShow.bind(this)
        this._keyboardDidHide = this._keyboardDidHide.bind(this)
}

static navigationOptions = {
    title: "ForgotPassword",
    header:null
};

componentDidMount(){
    this.keyboardDidShowListener = Keyboard.addListener(
        'keyboardDidShow',
        this._keyboardDidShow,
      );
      this.keyboardDidHideListener = Keyboard.addListener(
        'keyboardDidHide',
        this._keyboardDidHide,
    );
}

componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
}

_keyboardDidShow() {
    this.setState({isKey:1})
  }

_keyboardDidHide() {
    this.setState({isKey:0})
}

onFocus() {
   this.setState({isKey:1})
}

showFooter(){
  if(this.state.isKey == 0){
    return (
      <View style={{paddingBottom:20}}>
            <View>
                <TouchableOpacity onPress={()=> this.props.navigation.navigate("Login")}>
                    <Text style={styles.loginBottomText}>BACK TO LOGIN</Text>
                </TouchableOpacity>
            </View>
      </View>
    )
  }
}

submitForgot(){
  payload = {
    email : this.user_email
  }
  if(payload.email == undefined || payload.email == "") {
      utils.showToast('Please enter your email address!');
   }
   else if(!utils.validateEmail(payload.email)){
      utils.showToast('Please enter a valid email address!');
   }
   else {
         console.log(payload)
         this.setState({loadMask:true});
           fetch(Baseurl+'forgotpassword', {
               method: 'POST',
               headers: {
                 'Accept': 'application/json',
                 'Content-Type': 'application/json',
               },
               body: JSON.stringify(payload),
             })
             .then((response) => response.json())
             .then((responseJson) => {
               console.log(responseJson)
               this.setState({loadMask:false});
               if ("response" in responseJson) {
                 utils.showToast('You will receive an email to reset your password!');
                 this.props.navigation.goBack()
               }
               else {
                 utils.showToast(responseJson.error.messages[0]);
               }

             })
             .catch((error) => {
               this.setState({loadMask:false});
               utils.showToast("A network error occurred!");
             });
    }

}

_renderMask(){
    if(this.state.loadMask == true){
        return (
          <Mask />
        )
    }
}

render() {
    return (
      <KeyboardAvoidingView behavior={Platform.OS == 'ios' ? "padding" : null} style={{flex:1, backgroundColor:"#ef795d"}}>
        <ImageBackground source={MainBg} style={styles.mainBg}>
         {this._renderMask()}
          <View style={{flex:1, marginTop:screenHeight/8}}>
            <ScrollView style={{paddingHorizontal:screenWidth/12}}>
              <View style={{flex:1, alignItems:"center", justifyContent:"center"}}>
                  <Image source={AppLogo} style={styles.mainLogo} />
              </View>
              <View style={{flex:1, marginVertical:10}}>
                  <FormContainer onPress={()=> this.submitForgot()}>

                    <View style={styles.labelContainer}>
                      <View style={{flex:1}}>
                          <Text style={styles.labelForm}>E-MAIL</Text>
                          <TextInput
                          style={styles.formInput}
                          onFocus={ () => this.onFocus() }
                          placeholderTextColor={"#2e2e2e"}
                          keyboardType="email-address"
                          ref={'email'}
                          returnKeyType="done"
                          underlineColorAndroid="transparent"
                          onChangeText={(text) => this.user_email = text}
                          placeholder="Enter E-mail" />
                      </View>
                      <View style={styles.formIconsCont}>
                          <Image source={RegisterEmailIcon} style={styles.formIcons} />
                      </View>
                    </View>

                  </FormContainer>
                  <Text style={[styles.loginBottomText, {lineHeight:20}]}>Enter you email address to {"\n"} reset your password!</Text>
              </View>
            </ScrollView>
            {this.showFooter()}
          </View>
        </ImageBackground>
      </KeyboardAvoidingView>
    );
  }

}
