/* @flow */
import React, { Component } from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  ImageBackground,
  ActivityIndicator,
  AsyncStorage,
  Dimensions,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  Keyboard,
  Platform,
  Modal,
  FlatList
} from 'react-native';
import { SplashBg, FacebookIcon, TwitterIcon, InstaIcon, LinkIcon, GoogleIcon, FormContainer, LoginUserIcon, LoginPassIcon, RegisterEmailIcon, SaveIconInActive, Mask, Baseurl, ButtonBg, Header } from '../components/common';
import styles, { width } from '../styles/style';
import * as Animatable from 'react-native-animatable';
import { CommonActions, StackActions } from '@react-navigation/native';
import PhoneInput from 'react-native-phone-input'
import utils from '../utils';
import { Icon, ActionSheet } from 'native-base';
import services from '../api/services';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

var buttonsCamera = ["Camera", "Gallery", "Cancel"];
var CANCEL_INDEX = 2;

var BUTTONS_ = ["United States", "Cancel"];
var CANCEL_INDEX_ = 1;

export default class AddEvents extends Component {
  constructor(props) {
    super(props);
    const { state } = props.navigation;

    this.state = {
      loadMask: false,
      userPhone: "+923122681827",
      countryText: "Select Country",
      stateText: "Select State",
      talentText: "Select Your Talent",
      modalVisible: false,
      state_id: 0,
      isLoading: false,
      isTalent: 0,
      talent_id: 0,
      locations: null,
      talents: null,
      modalHeaderText: "Select",
      headerText:"add event"
    }
  }

  static navigationOptions = {
    title: "Login",
    header: null
  };

  componentDidMount() {

  }

  _onSubmitEditing = (cur, next) => {
    if (next != null) {
      this.refs[next].focus()

    } else {
      this.refs[cur].blur()
    }
  }

  onChangePhoneNumber = number => {
    console.log(number)
    this.setState({ userPhone: number });
  }

  onPressFlag = () => {

  }

  openActionsheet() {
    ActionSheet.show(
      {
        options: BUTTONS_,
        cancelButtonIndex: CANCEL_INDEX_,
        title: "Country"
      },
      buttonIndex => {

        if (buttonIndex != 1) {
          this.setState({ countryText: BUTTONS_[buttonIndex] });
        }

      }
    )
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  getStates() {
    this.setState({ modalVisible: true, isLoading: true, isTalent: 0, modalHeaderText: "Select State" })
    fetch(Baseurl + "GetStates", {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson)
        if (responseJson.status == "true" || responseJson.status == true) {
          this.setState({ locations: responseJson.data, isLoading: false })
        }
        else {
          this.setState({ locations: [], isLoading: false })
          utils.showToast("A network error occurred!");
        }
      })
      .catch((error) => {
        console.log(error)
        this.setState({ locations: [], isLoading: false })
        utils.showToast("A network error occurred!");
      });
  }

  _onLocationSelect = (id, name) => {
    this.setState({
      state_id: id,
      stateText: name
    })
    this.setState({ modalVisible: false })
  }

  _renderLocations = ({ item, index }) => {
    var locationTitle = item.StateName
    return (
      <TouchableOpacity onPress={() => this._onLocationSelect(item.StateId, item.StateName)}>
        <View style={{ flex: 1, borderWidth: 0.2, borderColor: "#8a93a5", borderRadius: 5, marginVertical: 5, marginHorizontal: 10, paddingHorizontal: 10, backgroundColor: "#FFF" }}>
          <View style={{ flexDirection: "row" }}>
            <View style={{ flex: 1, paddingVertical: 5, flexDirection: "column" }}>
              <Text numberOfLines={1} style={[styles.fontBook, { color: "#000", fontSize: 16, paddingLeft: 5, paddingVertical: 10 }]}>{locationTitle}</Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  _onTalentSelect = (id, name) => {
    this.setState({
      talent_id: id,
      talentText: name
    })
    this.setState({ modalVisible: false })
  }

  _renderTalents = ({ item, index }) => {
    var title = item.TalentName
    return (
      <TouchableOpacity onPress={() => this._onTalentSelect(item.TalentId, item.TalentName)}>
        <View style={{ flex: 1, borderWidth: 0.2, borderColor: "#8a93a5", borderRadius: 5, marginVertical: 5, marginHorizontal: 10, paddingHorizontal: 10, backgroundColor: "#FFF" }}>
          <View style={{ flexDirection: "row" }}>
            <View style={{ flex: 1, paddingVertical: 5, flexDirection: "column" }}>
              <Text numberOfLines={1} style={[styles.fontBook, { color: "#000", fontSize: 16, paddingLeft: 5, paddingVertical: 10 }]}>{title}</Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  getTalent() {
    this.setState({ modalVisible: true, isLoading: true, isTalent: 1, modalHeaderText: "Select Talent" })
    fetch(Baseurl + "GetTalent", {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson)
        if (responseJson.status == "true" || responseJson.status == true) {
          this.setState({ talents: responseJson.data, isLoading: false })
        }
        else {
          this.setState({ talents: [], isLoading: false })
          utils.showToast("A network error occurred!");
        }
      })
      .catch((error) => {
        console.log(error)
        this.setState({ talents: [], isLoading: false })
        utils.showToast("A network error occurred!");
      });
  }

  renderModal() {
    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.state.modalVisible}
        onRequestClose={() => {

        }}>
        <View style={{ flex: 1 }}>
          <Header bgColor="#bd376a" headerText={this.state.modalHeaderText} onLeftPress={() => this.setModalVisible(false)} onRightPress={null} rightIcon={null} />
          {this.renderContent()}
        </View>
      </Modal>
    )
  }

renderContent() {
    if (this.state.isTalent == 1) {
      if (this.state.isLoading == true) {
        return (
          <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
            <ActivityIndicator size="large" color="#ff0000" />
          </View>
        )
      }
      else if (this.state.isLoading != true && this.state.locations != "") {
        return (
          <View style={{ flex: 1 }}>
            <FlatList
              data={this.state.talents}
              style={{ marginBottom: 10, flex: 1 }}
              renderItem={this._renderTalents} />
          </View>
        )
      }
    }
    else {
      if (this.state.isLoading == true) {
        return (
          <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
            <ActivityIndicator size="large" color="#ff0000" />
          </View>
        )
      }
      else if (this.state.isLoading != true && this.state.locations != "") {
        return (
          <View style={{ flex: 1 }}>
            <FlatList
              data={this.state.locations}
              style={{ marginBottom: 10, flex: 1 }}
              renderItem={this._renderLocations} />
          </View>
        )
      }
    }
  }

getRegistered() {
    if (this.user_fname == undefined || this.user_fname == "") {
      utils.showToast('Please enter your first name!');
    }
    else if (this.user_lname == undefined || this.user_lname == "") {
      utils.showToast('Please enter your last name!');
    }
    else if (this.user_email == undefined || this.user_email == "") {
      utils.showToast('Please enter your Email Address!');
    }
    else if (!utils.validateEmail(this.user_email)) {
      utils.showToast('Please enter valid Email Address!');
    }
    else if (this.user_pass == undefined || this.user_pass == "") {
      utils.showToast('Please enter your password!');
    }
    else if (this.c_pass == undefined || this.c_pass == "") {
      utils.showToast('Please confirm your password!');
    }
    else if (this.c_pass != this.user_pass) {
      utils.showToast('Passwords should be same!');
    }
    else if (this.state.userPhone == undefined || this.state.userPhone == "") {
      utils.showToast('Please enter your phone number!');
    }
    else if (this.state.countryText == "Select Country") {
      utils.showToast('Please select your country!');
    }
    else if (this.state.state_id == undefined || this.state.state_id == 0) {
      utils.showToast('Please select your state!');
    }
    else if (this.state.talent_id == undefined || this.state.talent_id == 0) {
      utils.showToast('Please select your talent!');
    }
    else if (this.user_zip == undefined || this.user_zip == "") {
      utils.showToast('Please enter your zip code!');
    }
    else {
        this.setState({loadMask: true})
        var payload = new FormData();

        payload.append('FirstName', this.user_fname);
        payload.append('LastName', this.user_lname);
        payload.append('EmailAddress', this.user_email);
        payload.append('Password', this.user_pass);
        payload.append('PhoneNumber', this.state.userPhone);
        payload.append('StateId', this.state.state_id);
        payload.append('CountryName', this.state.countryText);
        payload.append('ZipCode', this.user_zip);
        payload.append('Talent', this.state.talent_id);
        payload.append('SocialAccountId', "false");
        payload.append('UserImage', '');
        console.log(payload)

        fetch(Baseurl+'Registration', {
            method: 'POST',
            headers: {
              'Content-Type': 'multipart/form-data',
            },
            body: payload,
          })
          .then((response) => response.json())
          .then((responseJson) => {
            console.log(responseJson)

            if(responseJson.status == "true" || responseJson.status == true){
              this.setState({loadMask:false})
              utils.showToast("Registration Successful!")
              var _data = JSON.stringify(responseJson.data)
              this.props.navigation.navigate('PasswordVerfication', {number:this.state.userPhone, userid:responseJson.data.UserId, otp:responseJson.data.OTP})
            }
            else {
              this.setState({loadMask:false})
              utils.showToast(responseJson.message);
            }

          })
          .catch((error) => {
            console.log(error)
            this.setState({loadMask:false});
            utils.showToast("A network error occurred!");
          });
    }
}

_renderMask() {
  if (this.state.loadMask == true) {
    return (
      <Mask />
    )
  }
}

render() {
    return (
      <KeyboardAvoidingView behavior={Platform.OS == 'ios' ? "padding" : null} style={{ flex: 1, backgroundColor: "#bd376a" }}>
        <ImageBackground source={SplashBg} style={styles.mainBg}>
          <Header bgColor="transparent" headerText={this.state.headerText} onLeftPress={()=> this.props.navigation.goBack()} onRightPress={null} rightIcon={null} />
          <View style={{flex:1, backgroundColor:"#FFF"}}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={{ width: "100%", paddingVertical:10 }}>
                <View style={[styles.labelContainer, { paddingVertical: 0 }]}>
                  <View style={{ flex: 1 }}>
                    <TextInput
                      style={styles.formInput}
                      placeholderTextColor={"#999999"}
                      ref={'eventName'}
                      returnKeyType="done"
                      underlineColorAndroid="transparent"
                      onChangeText={(text) => this.event_name = text}
                      placeholder="Event Name" />
                  </View>
                </View>
                <View style={[styles.labelContainer, { paddingVertical: 0 }]}>
                  <View style={{ flex: 1 }}>
                    <TextInput
                      style={styles.formInput}
                      placeholderTextColor={"#999999"}
                      ref={'eventName'}
                      returnKeyType="done"
                      underlineColorAndroid="transparent"
                      onChangeText={(text) => this.event_name = text}
                      placeholder="Event Type" />
                  </View>
                </View>
                <View style={[styles.labelContainer, { paddingVertical: 0 }]}>
                  <View style={{ flex: 1 }}>
                    <TextInput
                      style={styles.formInput}
                      placeholderTextColor={"#999999"}
                      ref={'eventName'}
                      returnKeyType="done"
                      underlineColorAndroid="transparent"
                      onChangeText={(text) => this.event_name = text}
                      placeholder="Event Date" />
                  </View>
                </View>
                <View style={[styles.labelContainer, { paddingVertical: 0 }]}>
                  <View style={{ flex: 1 }}>
                    <TextInput
                      style={styles.formInput}
                      placeholderTextColor={"#999999"}
                      ref={'eventName'}
                      returnKeyType="done"
                      underlineColorAndroid="transparent"
                      onChangeText={(text) => this.event_name = text}
                      placeholder="Event Time" />
                  </View>
                </View>
                <View style={[styles.labelContainer, { paddingVertical: 0 }]}>
                  <View style={{ flex: 1 }}>
                    <TextInput
                      style={styles.formInput}
                      placeholderTextColor={"#999999"}
                      ref={'eventName'}
                      returnKeyType="done"
                      underlineColorAndroid="transparent"
                      onChangeText={(text) => this.event_name = text}
                      placeholder="Event Charges ($)" />
                  </View>
                </View>
                <View style={[styles.labelContainer, { paddingVertical: 0 }]}>
                  <View style={{ flex: 1 }}>
                    <TextInput
                      style={styles.formInput}
                      placeholderTextColor={"#999999"}
                      ref={'eventName'}
                      returnKeyType="done"
                      underlineColorAndroid="transparent"
                      onChangeText={(text) => this.event_name = text}
                      placeholder="Seats (Capacity)" />
                  </View>
                </View>
                <View style={[styles.labelContainer, { borderBottomWidth: 0, paddingVertical:0 }]}>
                  <View style={{ flex: 1 }}>
                    <TextInput
                      style={styles.formInput}
                      placeholderTextColor={"#999999"}
                      ref={'eventName'}
                      returnKeyType="done"
                      underlineColorAndroid="transparent"
                      onChangeText={(text) => this.event_name = text}
                      placeholder="Event Location" />
                  </View>
                </View>
                <View style={{width:screenWidth, height:300, marginVertical:10}}>
                  <Image source={require("../assets/eventAddMap.png")} style={{width:screenWidth, height:300, resizeMode:"cover"}} />
                </View>
              <View style={{ width: "100%", paddingHorizontal: 50, marginBottom: 30, alignItems: "center", justifyContent: "center" }}>
                <ButtonBg label="Submit" color="#fd3a40" />
              </View>
            </View>
          </ScrollView>
          </View>
          {this.renderModal()}
          {this._renderMask()}
        </ImageBackground>
      </KeyboardAvoidingView>
    );
  }

}
