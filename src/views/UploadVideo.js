/* @flow */
import React, { Component } from 'react';
import {
  View,
  KeyboardAvoidingView,
  ScrollView,
  Image,
  TextInput,
  Text,
  ImageBackground,
  Dimensions,
  ActivityIndicator,
  AsyncStorage,
  TouchableOpacity,
  FlatList,
  StatusBar,
  Platform,
  Modal
} from 'react-native';
import styles from '../styles/style';
import { CommonActions, StackActions } from '@react-navigation/native';
import ImagePicker from 'react-native-image-picker';
import {
  Baseurl,
  Mask,
  VideoHeader,
  UserSubHeader,
  Banner,
  PlaceHolder,
  PlaceHolderSmall,
  ButtonSmall,
  LocationIcon,
  PaymentBg,
  NotificationIcon,
  Rating,
  SplashBg,
  StateDropDown,
  StarzMenu,
  VideosMenu,
  BottomBar,
  TalentFilter,
  UploadVideoButton
} from '../components/common';
import {
  MasonryListItem
} from '../components/items';
import {Toast, Icon, ActionSheet} from 'native-base';
const {width, height} = Dimensions.get("window");
import utils from '../utils';
import * as Animatable from 'react-native-animatable';

var options = {
  title: 'Upload Video',
  videoQuality:'low',
  mediaType:"video",
  durationLimit:30,
  allowsEditing:true,
  storageOptions: {
    waitUntilSaved:true,
    cameraRoll:true
  }
};

export default class UploadVideo extends Component {
constructor(props) {
      super(props);
      const {state} = props.navigation;
      this.state = {
        headerText:"",
        loadMask:false,
        imageTaken:false,
        resultVideo:null,
        isTalentFilter:false,
        filterAnimation:""
      }
}

componentDidMount(){
  //this.openVideoGalleryOptions()
}

componentWillUnmount(){

}

openVideoGalleryOptions(){
  ImagePicker.launchImageLibrary(options, (response) => {
  console.log('Response = ', response);
  // alert('Response = ', JSON.stringify(response))

  if (response.didCancel) {
    //console.error('User cancelled image picker');
  }
  else if (response.error) {
    //console.error('ImagePicker Error: ', response.error);
  }
  else if (response.customButton) {
    //console.error('User tapped custom button: ', response.customButton);
  }
  else {
     //alert(response.uri)
     this.setState({
       imageTaken:true,
       resultVideo: response.uri
     });
    }
  });
}

_renderTalentFilter(){
    if(this.state.isTalentFilter == true){
        return (
          <TalentFilter onClose={()=> this.setTalentFilterVisible(false)} filterAnimation={this.state.filterAnimation} />
        )
    }
}

setTalentFilterVisible(visible){
  if(visible == true){
    this.setState({ isTalentFilter: visible, filterAnimation:"fadeInDownBig" });
  }
  else {
    this.setState({ filterAnimation:"fadeOutUpBig" });
    setTimeout(()=>{
      this.setState({ isTalentFilter: visible });
    },400)
  }
}

render() {
    return (
      <View style={styles.dashboardMainView}>
          <VideoHeader bgColor="#000" headerText={this.state.headerText} onLeftPress={()=> this.props.navigation.goBack()} onRightPress={()=> this.setTalentFilterVisible(true)} rightIcon2="crop" rightIcon3="smile-o" rightIcon="ios-star-outline" />
          <View style={{flex:1, backgroundColor:"#000"}}>

          <ImageBackground style={{width:"100%", height:height-100, marginTop:10}} resizeMode="cover" source={{uri:"https://i.guim.co.uk/img/media/dfd56d28e5b8546c9f2cac7362d03256378cd81a/253_47_1880_2348/master/1880.jpg?width=700&quality=85&auto=format&fit=max&s=ad00c490c7ed5c63435657d811e2bc18"}}>
              <View style={{flex:1, backgroundColor: "rgba(0, 0, 0, 0.7)"}}>

                <View style={{flex:1, alignItems:"center", justifyContent:"center"}}>
                  <TouchableOpacity style={{width:50, height:50}}>
                      <Icon type="FontAwesome5" style={{width:50, height:50, textAlign:"center", fontSize:25, color:"#FFF"}} name="play" />
                  </TouchableOpacity>
                </View>

                <View style={{width:"100%", height:100, alignItems:"center", justifyContent:"center"}}>
                  <TouchableOpacity style={{width:70, height:70}}>
                      <Image style={{width:70, height:70, resizeMode:"contain"}} source={UploadVideoButton} />
                  </TouchableOpacity>
                </View>

              </View>
          </ImageBackground>

          </View>
        {this._renderTalentFilter()}
      </View>
    );
  }
}
