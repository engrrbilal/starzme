/* @flow */
import React, { Component } from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  ImageBackground,
  ActivityIndicator,
  AsyncStorage,
  Dimensions,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  Keyboard,
  FlatList
} from 'react-native';
import { Header, PlaceHolder, Mask, Baseurl, Rating } from '../components/common';
import styles from '../styles/style';
const {width, height} = Dimensions.get("window");
import { NavigationActions, StackActions } from 'react-navigation';
import * as Animatable from 'react-native-animatable';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

export default class FilterResults extends Component {
constructor(props) {
        super(props);
        const {state} = props.navigation;
        this.state = {
          headerText:"Results",
          city_id:state.params.cityid,
          loadMask:true,
          user_Data:null,
          results:null,
          gender:state.params.gender,
          rating:state.params.rating,
          serviceid:state.params.serviceid
        }
}

static navigationOptions = {
    title: "FilterResults",
    header:null
};

componentDidMount(){
    this.getData()
}

async getData(){
    try {
      const value = await AsyncStorage.getItem('userData')
      //console.log(udidToken)
      if (value !== null){
        var data = JSON.parse(value);
        this.setState({
          user_Data:data
        })
        console.log(this.state.user_Data)
        this.getGymsByFilter()
      }
      else {

      }
    }
    catch (error) {
        alert(error)
    }
}

getGymsByFilter(){
        var userToken = this.state.user_Data.access_token

        if(this.state.gender == "Select Gender"){
          var gen = ""
        }
        else {
          var gen = this.state.gender
        }

        if(this.state.rating == "Select Rating"){
          var rating = ""
        }
        else {
          var rating = this.state.rating
        }

        if(this.state.serviceid == "" || this.state.serviceid == null || this.state.serviceid == undefined){
          var sid = ""
        }
        else {
          var sid = this.state.serviceid
        }

        var base_url = Baseurl+"app/gym/list?city_id="+this.state.city_id+"&gender="+gen+"&rating="+rating+"&service_id="+sid
        console.log(base_url)
        fetch(base_url, {
            method: 'GET',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'Bearer '+userToken,
            },
          })
          .then((response) => response.json())
          .then((responseJson) => {
            console.log(responseJson)
            if("response" in responseJson){
                this.setState({results:responseJson.response.data, loadMask:false})
            }
            else {
                this.setState({results:"", loadMask:false})
                utils.showToast(responseJson.error.messages[0]);
            }
          })
          .catch((error) => {
                this.setState({results:"", loadMask:false})
                utils.showToast("A network error occurred!");
          });
}

_renderGymResults = ({ item, index }) => {
  var gymImage = item.cover_image_urls["1x"]
  var gymLogo = item.image_urls["1x"]
  var gymName = item.title
  var gymDesc = item.description

  var animationDelay = index*50

  return (
      <TouchableOpacity onPress={()=> this.props.navigation.navigate("GymDetails", {gym_id:item.id})}>
        <Animatable.View
        animation="slideInUp"
        iterationCount={1}
        style={styles.gymItemContainer}
        direction="alternate"
        duration={500}
        delay={animationDelay}>
          <View style={{flex:1, flexDirection:"row"}}>
            <View style={styles.gymIconCont}>
                <View style={styles.gymIconContInner}>
                    <Image source={{uri:gymLogo}} style={styles.gymIconImage} />
                </View>
            </View>
            <View style={{flex:1, paddingVertical:5, paddingHorizontal:10}}>
              <Text numberOfLines={1} style={styles.gymListHeading}>{gymName}</Text>
              <Text numberOfLines={3} style={styles.gymListDesc}>{gymDesc}</Text>
              <Rating rateReview={item.rating}/>
            </View>
            <View style={styles.gymImageCont}>
                <View style={styles.gymImageContInner}>
                    <Image source={{uri:gymImage}} style={styles.gymImage} />
                </View>
            </View>
          </View>
        </Animatable.View>
      </TouchableOpacity>
  );
}

gymResults(){
  if(this.state.results == null){

  }
  else if(this.state.results == ""){
    return (
      <PlaceHolder text={"No results found!"} />
    )
  }
  else {
    return (
      <ScrollView showsVerticalScrollIndicator={false} style={styles.marginTopMinus}>
        <View style={{flex:1}}>
            <FlatList
            style={{width:"100%", paddingHorizontal:20, flex:1, minHeight:height/1.5}}
            data={this.state.results}
            renderItem={this._renderGymResults} />
        </View>
      </ScrollView>
    )
  }
}

_renderMask(){
    if(this.state.loadMask == true){
        return (
          <Mask />
        )
    }
}

render() {
    return (
      <View style={[styles.dashboardMainView]}>
        {this._renderMask()}
        <Header leftIcon="back" headerText={this.state.headerText} onLeftPress={()=> this.props.navigation.goBack()} onRightPress={null} />
        <View style={styles.dashboardContainerView}>
          {this.gymResults()}
        </View>
      </View>
    );
  }

}
