/* @flow */
import React, { Component } from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  ImageBackground,
  ActivityIndicator,
  AsyncStorage,
  Dimensions,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  Keyboard,
  Platform,
  Modal,
  FlatList
} from 'react-native';
import { SplashBg, FacebookIcon, TwitterIcon, InstaIcon, LinkIcon, GoogleIcon, FormContainer, LoginUserIcon, LoginPassIcon, RegisterEmailIcon, SaveIconInActive, Mask, Baseurl, ButtonBg, Header, CustomPopup } from '../components/common';
import styles, { width } from '../styles/style';
import * as Animatable from 'react-native-animatable';
import { CommonActions, StackActions } from '@react-navigation/native';
import PhoneInput from 'react-native-phone-input'
import utils from '../utils';
import { Icon, ActionSheet } from 'native-base';
import services from '../api/services';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

var buttonsCamera = ["Camera", "Gallery", "Cancel"];
var CANCEL_INDEX = 2;

var BUTTONS_ = ["United States", "Cancel"];
var CANCEL_INDEX_ = 1;

export default class Purchase extends Component {
  constructor(props) {
    super(props);
    const { state } = props.navigation;

    this.state = {
      loadMask: false,
      headerText:"Purchase coin",
      isCustomPopup:false,
      customPopupTitle:"",
      customPopupRightPress:"",
      customPopupLeftPress:"",
      purchaseItems:[
        {
          "image":"../assets/purchaseCoin.png"
        },
        {
          "image":"../assets/purchaseCoin.png"
        },
        {
          "image":"../assets/purchaseCoin.png"
        },
        {
          "image":"../assets/purchaseCoin.png"
        },
        {
          "image":"../assets/purchaseCoin.png"
        },
        {
          "image":"../assets/purchaseCoin.png"
        },
        {
          "image":"../assets/purchaseCoin.png"
        },
        {
          "image":"../assets/purchaseCoin.png"
        },
        {
          "image":"../assets/purchaseCoin.png"
        },
        {
          "image":"../assets/purchaseCoin.png"
        },
      ]
    }
  }

  static navigationOptions = {
    title: "Purchase",
    header: null
  };

  componentDidMount() {

  }

  openCustomPopup(rPress,lPress,title,rText,lText){
    this.setState({
      isCustomPopup:true,
      customPopupTitle:title,
      customPopupRightPress:rPress,
      customPopupLeftPress:lPress,
      customPopupRightText:rText,
      customPopupLeftText:lText
    })
  }

  purchaseCoins(){
    var rPress = ()=> this.closeCustomPopup()
    var lPress = ()=> this.closeCustomPopup()
    this.openCustomPopup(rPress,lPress,"Do you want to purchase this package of $25?","Confirm","Cancel")
  }

  closeCustomPopup(){
    this.setState({isCustomPopup:false})
  }

  _renderPurchases = ({ item, index }) => {
    return (
      <TouchableOpacity onPress={()=> this.purchaseCoins()} style={{width:screenWidth, height:120}}>
        <Image source={require("../assets/purchaseCoin.png")} style={{width:screenWidth, height:120, resizeMode:"contain"}} />
      </TouchableOpacity>
    );
  }

_renderMask() {
  if (this.state.loadMask == true) {
    return (
      <Mask />
    )
  }
}

_renderCustomPopup(){
    if(this.state.isCustomPopup == true){
        return (
          <CustomPopup title={this.state.customPopupTitle} rightPress={this.state.customPopupRightPress} rightText={this.state.customPopupRightText} leftPress={this.state.customPopupLeftPress} leftText={this.state.customPopupLeftText} />
        )
    }
}

render() {
    return (
      <View style={{ flex: 1, backgroundColor: "#bd376a" }}>
        <ImageBackground source={SplashBg} style={styles.mainBg}>
          <Header bgColor="transparent" headerText={this.state.headerText} onLeftPress={()=> this.props.navigation.goBack()} onRightPress={null} rightIcon={null} />
          <View style={{flex:1}}>
          <ScrollView showsVerticalScrollIndicator={false}>
              <View style={{ flex: 1 }}>
                <FlatList
                  data={this.state.purchaseItems}
                  style={{ marginBottom: 10, flex: 1 }}
                  renderItem={this._renderPurchases} />
              </View>
          </ScrollView>
          </View>
        </ImageBackground>
        {this._renderMask()}
        {this._renderCustomPopup()}
      </View>
    );
  }

}
