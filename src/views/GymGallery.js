/* @flow */
import React, { Component } from 'react';
import {
  View,
  KeyboardAvoidingView,
  ScrollView,
  Image,
  TextInput,
  Text,
  ImageBackground,
  Dimensions,
  ActivityIndicator,
  AsyncStorage,
  TouchableOpacity,
  FlatList,
  StatusBar
} from 'react-native';
import styles from '../styles/style';
import { NavigationActions } from 'react-navigation';
import { Baseurl, Mask, Header, Banner, PlaceHolder } from '../components/common';
import MasonryList from "react-native-masonry-list";
import {Toast, Icon, ActionSheet} from 'native-base';
const {width, height} = Dimensions.get("window");

export default class GymGallery extends Component {
constructor(props) {
      super(props);
      const {state} = props.navigation;
      this.state = {
        headerText:"Gym Gallery",
        loadMask:true,
        user_Data:null,
        gymGallery:null,
        gymId:state.params.gym_id,
        col:2,
        popupImage:"",
        showPopup:false,
      }
}

componentDidMount(){
   this.getData()
}

async getData(){
    try {
      const value = await AsyncStorage.getItem('userData')
      if (value !== null){
        var data = JSON.parse(value)
        this.setState({user_Data:data})
        this.getGymGallery()
      }
      else {

      }
    }
    catch (error) {
       console.log(error)
    }
}

getGymGallery(){
      var userToken = this.state.user_Data.access_token
      fetch(Baseurl+"gym/images/list?gym_id="+this.state.gymId, {
          method: 'GET',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+userToken,
          },
        })
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson)
          if("response" in responseJson){
              //this.setState({gymGallery:responseJson.response.data, loadMask:false})
              const Gym_gallery = responseJson.response.data
              var gymImages = []

              for(let i = 0; i < Gym_gallery.length; i++){
                b={uri:Gym_gallery[i].image_urls["2x"]};
                gymImages.push(b);
              }

              if(Gym_gallery.length == 1){
                  this.setState({gymGallery:gymImages, loadMask:false, col:1})
              }
              else {
                  this.setState({gymGallery:gymImages, loadMask:false, col:2})
              }

          }
          else {
              this.setState({gymGallery:"", loadMask:false})
              utils.showToast(responseJson.error.messages[0]);
          }
        })
        .catch((error) => {
              this.setState({gymGallery:"error", loadMask:false})
              utils.showToast("A network error occurred!");
        });
}

renderGymGallery(){
  if(this.state.gymGallery == null){

  }
  else if(this.state.gymGallery == "error"){
    return (
        <PlaceHolder text={"Network error occurred!"} />
    )
  }
  else if(this.state.gymGallery == ""){
    return (
        <PlaceHolder text={"No photo found!"} />
    )
  }
  else {
    return (
      <ScrollView showsVerticalScrollIndicator={false} style={styles.marginTopMinus}>
        <View style={{flex:1, paddingHorizontal:10, marginBottom:50}}>
            <MasonryList
                images={this.state.gymGallery}
                columns={this.state.col}
            />
        </View>
      </ScrollView>
    )
  }
}

showPopup(){
    if (this.state.showPopup == true) {
      return (
        <View style={[styles.popupViewBg, {backgroundColor:"#000"}]}>
        <TouchableOpacity style={{width:60, height:60, justifyContent:"center", alignItems:"center", position:"absolute", right:0, top:20, zIndex:100}} onPress={() => this.closePopUp()} >
          <Icon name="md-close" style={[styles.popupClose, {color:"#FFF"}]} />
        </TouchableOpacity>
          <View style={[styles.popupView, {width:"100%", borderRadius:0}]}>
            <Image source={this.state.popupImage} style={{height: 200, width: null, flex: 1}}/>
          </View>
        </View>
      )
    }
}

showPopupHere(img){
    this.setState({showPopup:true, popupImage:img})
}

closePopUp(){
    this.setState({showPopup:false, popupImage:null})
}

_renderMask(){
    if(this.state.loadMask == true){
        return (
          <Mask />
        )
    }
}

render() {
    return (
      <View style={[styles.dashboardMainView]}>
        {this._renderMask()}
        {this.showPopup()}
        <Header leftIcon="back" headerText={this.state.headerText} onLeftPress={()=> this.props.navigation.goBack()} onRightPress={null} />
        <View style={styles.dashboardContainerView}>
          {this.renderGymGallery()}
        </View>
      </View>
    );
  }
}
