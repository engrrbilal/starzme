// // /* @flow */
import React, { Component } from 'react';
import {
  View,
  KeyboardAvoidingView,
  ScrollView,
  Image,
  TextInput,
  Text,
  ImageBackground,
  Dimensions,
  ActivityIndicator,
  AsyncStorage,
  TouchableOpacity,
  FlatList,
  StatusBar,
  Platform,
  Modal,
  PermissionsAndroid,
} from 'react-native';
import styles from '../styles/style';
import { CommonActions, StackActions } from '@react-navigation/native';
import ImagePicker from 'react-native-image-picker';
import {
  Baseurl,
  Mask,
  VideoHeader,
  UserSubHeader,
  Banner,
  PlaceHolder,
  PlaceHolderSmall,
  ButtonSmall,
  LocationIcon,
  PaymentBg,
  NotificationIcon,
  Rating,
  SplashBg,
  StateDropDown,
  StarzMenu,
  VideosMenu,
  BottomBar,
  TalentFilter,
  UploadVideoButton
} from '../components/common';
import {
  MasonryListItem
} from '../components/items';
import { Toast, Icon, ActionSheet } from 'native-base';
const { width, height } = Dimensions.get("window");
import utils from '../utils';
import * as Animatable from 'react-native-animatable';
import { NodePlayerView, NodeCameraView } from 'react-native-nodemediaclient';
import { TouchableHighlight } from 'react-native-gesture-handler';
import axios from 'axios';

let options = {
  title: 'Upload Video',
  videoQuality: 'low',
  mediaType: "video",
  durationLimit: 30,
  allowsEditing: true,
  storageOptions: {
    waitUntilSaved: true,
    cameraRoll: true
  }
};
const settings = {
  camera: { cameraId: 1, cameraFrontMirror: true },
  audio: { bitrate: 32000, profile: 1, samplerate: 44100 },
  video: {
    preset: 24,
    bitrate: 400000,
    profile: 2,
    fps: 30,
    videoFrontMirror: true
  }
};
const MUX_TOKEN_ID = "ee721e7f-a771-4a41-8f6a-ba029f10b688"
const MUX_TOKEN_SECRET = "g9+QvTl1xXR03atexNxqGqMerK+ISe5ZZ+fvv/A+Uy4ipnOZ6sn/nO6UkxpHB5PKYg4wU9dqMml"
export default class OnAir extends Component {
  constructor(props) {
    super(props);
    const { state } = props.navigation;
    this.state = {
      headerText: "",
      loadMask: false,
      imageTaken: false,
      resultVideo: null,

      isPublishing: false,
      userComment: "",
      hasPermission: false,
      paused: true,
      goLive: false,
      stream_key: "",
      loading: false,

    }
  }

  componentDidMount() {
    // this.getStreamKey()
  }

  getStreamKey = () => {
    // alert("get stream key called!")
    const config = {
      headers: {
        'Accept': "application/json",
        'Authorization': "Basic ZWU3MjFlN2YtYTc3MS00YTQxLThmNmEtYmEwMjlmMTBiNjg4Omc5K1F2VGwxeFhSMDNhdGV4TnhxR3FNZXJLK0lTZTVaWitmdnYvQStVeTRpcG5PWjZzbi9uTzZVa3hwSEI1UEtZZzR3VTlkcU1tbA=="
      },
    };

    axios.post('https://api.mux.com/video/v1/live-streams', {
      username: 'ee721e7f-a771-4a41-8f6a-ba029f10b688',
      password: 'g9+QvTl1xXR03atexNxqGqMerK+ISe5ZZ+fvv/A+Uy4ipnOZ6sn/nO6UkxpHB5PKYg4wU9dqMml'
    }, config)
      .then((response) => {
        console.log("#live response.data.stream_key : ", response.data);
        alert("get data : " + JSON.stringify(response.data))
        if (response && response.data && response.data.stream_key) {
          alert("get key : " + JSON.stringify(response.data.stream_key))
          this.setState({ stream_key: response.data.stream_key })
        }

      })
      .catch(function (error) {
        console.log("#live error : ", error);
      });
  }
  onPressAdminBtn = async () => {
    const { hasPermission } = this.state;
    if (Platform.OS === "android") {
      if (!hasPermission) {
        await this.checkPermissions();
      }
    }
  };
  renderCameraView = () => {
    const { hasPermission } = this.state;
    if (Platform.OS === "android" && !hasPermission) {
      return <View />;
    }
    else {
      alert("start key : " + this.state.stream_key)
      console.log("#stream_key : ", this.state.stream_key)
      // if (this.state.stream_key) {
      return (
        <>
          <NodePlayerView
            style={{ height: 200 }}
            ref={(vp) => { this.vp = vp }}
            inputUrl={"rtmp://live.mux.com/app/1e54b576-ad67-4f48-5723-cde4182153e8"}
            scaleMode={"ScaleAspectFit"}
            bufferTime={300}
            maxBufferTime={1000}
            autoplay={true}
          />
          <NodeCameraView
            style={styles.nodeCameraView}
            ref={vb => {
              this.vb = vb;
            }}
            outputUrl={
              // `rtmp://global-live.mux.com:5222/app/ea9341f8-89df-aa6a-a66e-f43746962568`
              // `rtmps://global-live.mux.com/app/b2ebbd76-2beb-383f-fae2-c63fde6a0b91`
              `rtmp://live.mux.com/app/1e54b576-ad67-4f48-5723-cde4182153e8`
            }
            camera={{ cameraId: 1, cameraFrontMirror: true }}
            audio={{ bitrate: 32000, profile: 1, samplerate: 44100 }}
            video={{
              preset: 12,
              bitrate: 400000,
              profile: 1,
              fps: 15,
              videoFrontMirror: false,
            }}
            autopreview={true}
          />
        </>
      );
      // }
    }
  };

  checkPermissions = async () => {
    console.log("Checking Permissions Android");
    try {
      const granted = await PermissionsAndroid.requestMultiple([
        PermissionsAndroid.PERMISSIONS.CAMERA,
        PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        PermissionsAndroid.PERMISSIONS.FLASHLIGHT,
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      ]);
      let hasAllPermissions = true;
      Object.keys(granted).forEach(key => {
        // key: the name of the object key
        // index: the ordinal position of the key within the object
        if (granted[key] !== "granted") {
          console.log("Does not have permission for: ", granted[key]);
          hasAllPermissions = false;
        }
      });
      console.log("hasAllPermissions: ", hasAllPermissions);
      this.setState({ hasPermission: hasAllPermissions });
    } catch (err) {
      console.warn(err);
    }
  };

  onPressPublishBtn = async () => {
    const { isPublishing: publishingState, hasPermission } = this.state;

    this.setState({ isPublishing: !publishingState }, () => {
      if (Platform.OS === "android") {
        if (!hasPermission) {
          this.checkPermissions();
          return;
        }
      }

      if (publishingState) {
        this.vb.stop();
      } else {
        this.vb.start();
      }

    });
  };
  _togglePublish = () => {
    if (this.state.isPublishing) {
      this.setState({ isPublishing: false }, () => {
        this.vb.stop();
      });

      alert(
        'Stream finished!',
        'Thanks for using the app'
      );
    } else {
      alert(
        'Stream start!',
        'Thanks for using the app'
      );
      this.setState({ isPublishing: true }, () => {
        this.vb.start();
      });
    }
  }

  componentWillUnmount() {

  }

  openVideoGalleryOptions() {
    ImagePicker.launchImageLibrary(options, (response) => {
      console.log('Response = ', response);
      // alert('Response = ', JSON.stringify(response))

      if (response.didCancel) {
        //console.error('User cancelled image picker');
      }
      else if (response.error) {
        //console.error('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        //console.error('User tapped custom button: ', response.customButton);
      }
      else {
        //alert(response.uri)
        this.setState({
          imageTaken: true,
          resultVideo: response.uri
        });
      }
    });
  }

  render() {
    const { isPublishing } = this.state;
    return (
      <View style={styles.dashboardMainView}>
        <VideoHeader bgColor="#000" headerText={this.state.headerText} onLeftPress={() => this.props.navigation.goBack()} type="onAir" />
        <View style={{ flex: 1, backgroundColor: "#000" }}>
          {/* this.state.goLive &&
            <>
              <TouchableHighlight onPress={this.onPressPublishBtn} >
                <View>
                  <View style={{ width: 50, height: 50, borderRadius: 25, borderWidth: 1, borderColor: "#fd3a40", alignItems: "center", justifyContent: "center" }}>
                    <Image style={{ width: 40, height: 40, resizeMode: "stretch", borderRadius: 20 }} source={require("../assets/UserProfileIcon.png")} />
                  </View>
                  <View style={{ width: 50, backgroundColor: "#fd3a40", marginVertical: 5 }}>
                    <Text style={{ textAlign: "center", color: "#FFF", fontSize: 12, paddingVertical: 5 }}> {isPublishing ? "END LIVE" : "GO LIVE"}</Text>
                  </View>
                </View>
              </TouchableHighlight>
              <View style={{ width: "100%", height: "100%", paddingHorizontal: 20, paddingVertical: 20 }}>

              </View>
            </>
           */}
          <ImageBackground style={{ width: "100%", height: height - 100, marginTop: 10 }} resizeMode="cover" source={{ uri: "https://mk0musicgrotto1kyale.kinstacdn.com/wp-content/uploads/2018/05/singer-lounge-microphone-karaoke.jpg.640x427_q100.jpg" }}>
            <View style={{ flex: 1, backgroundColor: "rgba(0, 0, 0, 0.8)" }}>
              <Image style={{ width: width, height: 100, resizeMode: "contain" }} source={require('../assets/onAirScreen.png')} />
              <View style={{ width: "100%", paddingHorizontal: 20, paddingVertical: 20 }}>
                <TouchableHighlight onPress={this.onPressPublishBtn}>
                  <>
                    <View style={{ width: 50, height: 50, borderRadius: 25, borderWidth: 1, borderColor: "#fd3a40", alignItems: "center", justifyContent: "center" }}>
                      <Image style={{ width: 40, height: 40, resizeMode: "stretch", borderRadius: 20 }} source={require("../assets/UserProfileIcon.png")} />
                    </View>
                    <View style={{ width: 50, backgroundColor: "#fd3a40", marginVertical: 5 }}>
                      <Text style={{ textAlign: "center", color: "#FFF", fontSize: 12, paddingVertical: 5 }}> {isPublishing ? "END LIVE" : "GO LIVE"}</Text>
                    </View>
                  </>
                </TouchableHighlight>
                <View style={{ width: "100%", height: "100%", paddingHorizontal: 20, paddingVertical: 20 }}>
                  {this.renderCameraView}
                </View>
              </View>
              <View style={{ width: "100%", height: 100 }}></View>

              <ScrollView>

                <View style={{ width: "100%", paddingHorizontal: 20, paddingVertical: 20 }}>
                  <TouchableOpacity activeOpacity={1} style={{ flexDirection: "row", width: "100%", marginVertical: 10 }}>
                    <View style={{ width: 50, height: 50, borderRadius: 25, borderWidth: 1, borderColor: "#fd3a40", alignItems: "center", justifyContent: "center" }}>
                      <Image style={{ width: 40, height: 40, resizeMode: "stretch", borderRadius: 20 }} source={require("../assets/UserProfileIcon.png")} />
                    </View>
                    <View style={{ backgroundColor: "#000", padding: 10, borderRadius: 5, marginHorizontal: 10 }}>
                      <Text style={{ textAlign: "center", color: "#FFF", fontSize: 12, paddingVertical: 5 }}>Hello there</Text>
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity activeOpacity={1} style={{ flexDirection: "row", width: "100%", marginVertical: 10 }}>
                    <View style={{ width: 50, height: 50, borderRadius: 25, borderWidth: 1, borderColor: "#fd3a40", alignItems: "center", justifyContent: "center" }}>
                      <Image style={{ width: 40, height: 40, resizeMode: "stretch", borderRadius: 20 }} source={require("../assets/UserProfileIcon.png")} />
                    </View>
                    <View style={{ backgroundColor: "#000", padding: 10, borderRadius: 5, marginHorizontal: 10 }}>
                      <Text style={{ textAlign: "center", color: "#FFF", fontSize: 12, paddingVertical: 5 }}>Wow.. Lovely</Text>
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity activeOpacity={1} style={{ flexDirection: "row", width: "100%", marginVertical: 10 }}>
                    <View style={{ width: 50, height: 50, borderRadius: 25, borderWidth: 1, borderColor: "#fd3a40", alignItems: "center", justifyContent: "center" }}>
                      <Image style={{ width: 40, height: 40, resizeMode: "stretch", borderRadius: 20 }} source={require("../assets/UserProfileIcon.png")} />
                    </View>
                    <View style={{ backgroundColor: "#000", padding: 10, borderRadius: 5, marginHorizontal: 10 }}>
                      <Text style={{ textAlign: "center", color: "#FFF", fontSize: 12, paddingVertical: 5 }}>Nice song. Awesome</Text>
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity activeOpacity={1} style={{ flexDirection: "row", width: "100%", marginVertical: 10 }}>
                    <View style={{ width: 50, height: 50, borderRadius: 25, borderWidth: 1, borderColor: "#fd3a40", alignItems: "center", justifyContent: "center" }}>
                      <Image style={{ width: 40, height: 40, resizeMode: "stretch", borderRadius: 20 }} source={require("../assets/UserProfileIcon.png")} />
                    </View>
                    <View style={{ backgroundColor: "#000", padding: 10, borderRadius: 5, marginHorizontal: 10 }}>
                      <Text style={{ textAlign: "center", color: "#FFF", fontSize: 12, paddingVertical: 5 }}>Stupid</Text>
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity activeOpacity={1} style={{ flexDirection: "row", width: "100%", marginVertical: 10 }}>
                    <View style={{ width: 50, height: 50, borderRadius: 25, borderWidth: 1, borderColor: "#fd3a40", alignItems: "center", justifyContent: "center" }}>
                      <Image style={{ width: 40, height: 40, resizeMode: "stretch", borderRadius: 20 }} source={require("../assets/UserProfileIcon.png")} />
                    </View>
                    <View style={{ backgroundColor: "#000", padding: 10, borderRadius: 5, marginHorizontal: 10 }}>
                      <Text style={{ textAlign: "center", color: "#FFF", fontSize: 12, paddingVertical: 5 }}>Hate this...</Text>
                    </View>
                  </TouchableOpacity>
                </View>

              </ScrollView>

              <View style={{ flexDirection: "row", width: "100%", height: 70 }}>
                <View style={{ backgroundColor: "#000", padding: 10, borderRadius: 5, marginHorizontal: 10, flex: 1 }}>
                  <Text style={{ color: "#FFF", fontSize: 13, paddingVertical: 5, paddingHorizontal: 10 }}>Write Comment...</Text>
                </View>
                <TouchableOpacity style={{ width: 50, height: 50, alignItems: "center", justifyContent: "center" }}>
                  <Icon type="Ionicons" style={{ fontSize: 22, textAlign: "center", color: "#FFF" }} name="md-send" />
                </TouchableOpacity>
              </View>

            </View>
          </ImageBackground>

        </View>
      </View>
    );
  }
}


// import React, { Component } from "react";
// import { NodeCameraView } from "react-native-nodemediaclient";
// import {
//   StyleSheet,
//   Text,
//   View,
//   TouchableOpacity,
//   Dimensions,
//   PermissionsAndroid,
//   Platform
// } from "react-native";
// import Video from "react-native-video";
// import VideoPlayer from "react-native-video-player";

// const deviceWidth = Dimensions.get("window").width;



// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: "#000",
//     justifyContent: "center"
//   },
//   nodePlayerView: {
//     position: "absolute",
//     top: 0,
//     bottom: 0,
//     left: 0,
//     right: 0
//   },
//   nodeCameraView: {
//     position: "absolute",
//     top: 0,
//     bottom: 0,
//     left: 0,
//     right: 0
//   },
//   playBtn: {
//     justifyContent: "center",
//     alignItems: "center",
//     alignSelf: "center",
//     backgroundColor: "#333",
//     borderColor: "#333",
//     borderWidth: 3,
//     borderRadius: 2,
//     height: 50,
//     width: deviceWidth / 2,
//     paddingVertical: 10,
//     paddingHorizontal: 30,
//     elevation: 4,
//     marginVertical: 10
//   },
//   playBtnContainer: {
//     position: "absolute",
//     bottom: 100,
//     left: 0,
//     right: 0,
//     marginVertical: 20
//   },
//   goLive: {
//     justifyContent: "center",
//     alignItems: "center",
//     alignSelf: "center",
//     backgroundColor: "#d1a667",
//     borderColor: "#d1a667",
//     borderWidth: 3,
//     borderRadius: 2,
//     height: 50,
//     width: deviceWidth / 2,
//     paddingVertical: 10,
//     paddingHorizontal: 30,
//     elevation: 4,
//     marginVertical: 10
//   },
//   adminBtnContainer: {
//     position: "absolute",
//     top: 0,
//     right: 0,
//     margin: 30,
//     marginTop: 60
//   },
//   adminBtn: {
//     backgroundColor: "#006D9E",
//     padding: 10,
//     justifyContent: "center",
//     alignItems: "center",
//     borderRadius: 5,
//     elevation: 4
//   },
//   btnText: { color: "#FFF", fontSize: 18 }
// });





// import React, { Component } from "react";
// import { NodeCameraView } from "react-native-nodemediaclient";
// import {
//   StyleSheet,
//   Text,
//   View,
//   TouchableOpacity,
//   Dimensions,
//   PermissionsAndroid,
//   Platform
// } from "react-native";
// import Video from "react-native-video";
// import VideoPlayer from "react-native-video-player";
// import axios from "axios";

// const deviceWidth = Dimensions.get("window").width;

// const settings = {
//   camera: { cameraId: 1, cameraFrontMirror: true },
//   audio: { bitrate: 32000, profile: 1, samplerate: 44100 },
//   video: {
//     preset: 24,
//     bitrate: 400000,
//     profile: 2,
//     fps: 30,
//     videoFrontMirror: true
//   }
// };

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: "#000",
//     justifyContent: "center"
//   },
//   nodePlayerView: {
//     position: "absolute",
//     top: 0,
//     bottom: 0,
//     left: 0,
//     right: 0
//   },
//   nodeCameraView: {
//     position: "absolute",
//     top: 0,
//     bottom: 0,
//     left: 0,
//     right: 0
//   },
//   playBtn: {
//     justifyContent: "center",
//     alignItems: "center",
//     alignSelf: "center",
//     backgroundColor: "#333",
//     borderColor: "#333",
//     borderWidth: 3,
//     borderRadius: 2,
//     height: 50,
//     width: deviceWidth / 2,
//     paddingVertical: 10,
//     paddingHorizontal: 30,
//     elevation: 4,
//     marginVertical: 10
//   },
//   playBtnContainer: {
//     position: "absolute",
//     bottom: 100,
//     left: 0,
//     right: 0,
//     marginVertical: 20
//   },
//   goLive: {
//     justifyContent: "center",
//     alignItems: "center",
//     alignSelf: "center",
//     backgroundColor: "#d1a667",
//     borderColor: "#d1a667",
//     borderWidth: 3,
//     borderRadius: 2,
//     height: 50,
//     width: deviceWidth / 2,
//     paddingVertical: 10,
//     paddingHorizontal: 30,
//     elevation: 4,
//     marginVertical: 10
//   },
//   adminBtnContainer: {
//     position: "absolute",
//     top: 0,
//     right: 0,
//     margin: 30,
//     marginTop: 60
//   },
//   adminBtn: {
//     backgroundColor: "#006D9E",
//     padding: 10,
//     justifyContent: "center",
//     alignItems: "center",
//     borderRadius: 5,
//     elevation: 4
//   },
//   btnText: { color: "#FFF", fontSize: 18 }
// });

// export default class App extends Component {
//   state = {
//     admin: false,
//     isPublishing: false,
//     userComment: "",
//     hasPermission: false,
//     paused: true,
//     stream_key: "",
//     loading: false
//   };

//   onPressAdminBtn = async () => {
//     const { admin: adminState, hasPermission } = this.state;
//     this.setState({ admin: !adminState });
//     if (!adminState) {
//       if (Platform.OS === "android") {
//         if (!hasPermission) {
//           await this.checkPermissions();
//         }
//       }
//     }
//   };

//   onPressPlayBtn = () => {
//     const { paused: pausedState } = this.state;
//     this.setState({ paused: !pausedState });
//   };

//   renderPlayerView = () => {
//     const { paused } = this.state;
//     const source = {
//       uri: "https://stream.mux.com/2P00oCBYNBsoClKrWrxfB01rryFnugyq2hJc4XaibYNTM.m3u8"
//     };
//     return (
//       <VideoPlayer
//         video={{ uri: "https://stream.mux.com/M3W01Xw6lHxr01BPccTe2O2tiMoKcCD8o756JSDv5dFaU.m3u8" }}
//         // video={{ uri: this.props.videoUrl }}
//         videoWidth={1600}
//         videoHeight={900}
//         ref={ref => {
//           this.player = ref;
//         }} // Store reference
//         // onStart={this.onLoad}
//         // onPlayPress={this.onLoadStart}
//         // resizeMode="contain"
//         // autoplay={true}
//         resizeMode="cover"
//         pauseOnPress={paused}
//       // thumbnail={{ uri: 'https://i.picsum.photos/id/866/1600/900.jpg' }}
//       />
//     );
//   };

//   onBuffer = buffer => {
//     console.log("onBuffer: ", buffer);
//   };

//   onError = error => {
//     console.log("onError: ", error);
//   };

//   renderCameraView = () => {
//     const { hasPermission } = this.state;
//     if (Platform.OS === "android" && !hasPermission) {
//       return <View />;
//     }
//     const config = {
//       headers: {
//         'Accept': "application/json",
//         'Authorization': "Basic ZWU3MjFlN2YtYTc3MS00YTQxLThmNmEtYmEwMjlmMTBiNjg4Omc5K1F2VGwxeFhSMDNhdGV4TnhxR3FNZXJLK0lTZTVaWitmdnYvQStVeTRpcG5PWjZzbi9uTzZVa3hwSEI1UEtZZzR3VTlkcU1tbA=="
//       },
//     };
//     this.setState({ loading: true }, () => {

//       axios.post('https://api.mux.com/video/v1/live-streams', {
//         username: 'ee721e7f-a771-4a41-8f6a-ba029f10b688',
//         password: 'g9+QvTl1xXR03atexNxqGqMerK+ISe5ZZ+fvv/A+Uy4ipnOZ6sn/nO6UkxpHB5PKYg4wU9dqMml'
//       }, config)
//         .then((response) => {
//           if (response.data.stream_key) {
//             console.log("#live response.data.stream_key : ", response.data.stream_key);
//             this.setState({ loading: false, stream_key: response.data.stream_key })
//           }

//         })
//         .catch(function (error) {
//           console.log("#live error : ", error);
//         });

//     })
//     if (this.state.loading) {
//       return <Text>Loading ...</Text>
//     } else if (this.state.stream_key) {
//       return (
//         <NodeCameraView
//           style={styles.nodeCameraView}
//           /* eslint-disable */
//           ref={vb => {
//             this.vb = vb;
//           }}
//           /* eslint-enable */
//           outputUrl={`rtmp://live.mux.com/app/${this.state.stream_key}`}
//           camera={settings.camera}
//           audio={settings.audio}
//           video={settings.video}
//           autopreview
//         />
//       );
//     }
//   };

//   checkPermissions = async () => {
//     console.log("Checking Permissions Android");
//     try {
//       const granted = await PermissionsAndroid.requestMultiple([
//         PermissionsAndroid.PERMISSIONS.CAMERA,
//         PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
//         PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE
//       ]);
//       let hasAllPermissions = true;
//       Object.keys(granted).forEach(key => {
//         // key: the name of the object key
//         // index: the ordinal position of the key within the object
//         if (granted[key] !== "granted") {
//           console.log("Does not have permission for: ", granted[key]);
//           hasAllPermissions = false;
//         }
//       });
//       console.log("hasAllPermissions: ", hasAllPermissions);
//       this.setState({ hasPermission: hasAllPermissions });
//     } catch (err) {
//       console.warn(err);
//     }
//   };

//   onPressPublishBtn = async () => {
//     const { isPublishing: publishingState, hasPermission } = this.state;
//     if (Platform.OS === "android") {
//       if (!hasPermission) {
//         this.checkPermissions();
//         return;
//       }
//     }

//     if (publishingState) {
//       alert("#this.vb.stop() " + this.vb.stop)
//       this.vb.stop();
//     } else {
//       alert("#this.vb.start() " + this.vb.start)
//       this.vb.start();
//     }

//     this.setState({ isPublishing: !publishingState });
//   };

//   render() {
//     const { admin, paused, isPublishing } = this.state;
//     return (
//       <View style={styles.container}>
//         {admin ? this.renderCameraView() : this.renderPlayerView()}

//         {admin ? (
//           <TouchableOpacity onPress={() => this.onPressPublishBtn}>
//             <View style={styles.goLive}>
//               <Text style={styles.btnText}>
//                 {isPublishing ? "END LIVE" : "GO LIVE"}
//               </Text>
//             </View>
//           </TouchableOpacity>
//         ) : (
//             <TouchableOpacity
//               style={styles.playBtnContainer}
//               onPress={this.onPressPlayBtn}
//             >
//               <View style={styles.playBtn}>
//                 <Text style={styles.btnText}>{paused ? "PLAY" : "PAUSE"}</Text>
//               </View>
//             </TouchableOpacity>
//           )}

//         <TouchableOpacity
//           style={styles.adminBtnContainer}
//           onPress={this.onPressAdminBtn}
//         >
//           <View style={styles.adminBtn}>
//             <Text style={styles.btnText}>
//               {admin ? "VIEW USER" : "VIEW ADMIN"}
//             </Text>
//           </View>
//         </TouchableOpacity>
//       </View>
//     );
//   }
// }
