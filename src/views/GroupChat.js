/* @flow */
import React, { Component } from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  ImageBackground,
  ActivityIndicator,
  AsyncStorage,
  Dimensions,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  Keyboard,
  Platform,
  Modal,
  FlatList
} from 'react-native';
import { SplashBg, FacebookIcon, TwitterIcon, InstaIcon, LinkIcon, GoogleIcon, FormContainer, LoginUserIcon, LoginPassIcon, RegisterEmailIcon, SaveIconInActive, Mask, Baseurl, ButtonBg, Header } from '../components/common';
import styles, { width } from '../styles/style';
import * as Animatable from 'react-native-animatable';
import { CommonActions, StackActions } from '@react-navigation/native';
import PhoneInput from 'react-native-phone-input'
import utils from '../utils';
import { Icon, ActionSheet } from 'native-base';
import services from '../api/services';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

export default class GroupChat extends Component {
  constructor(props) {
    super(props);
    const { state } = props.navigation;

    this.state = {
      loadMask: false,
      headerText:"chat",
      chatMsgs:[
        {"msg":"abc"},
        {"msg":"abc"}
      ]
    }
  }

  static navigationOptions = {
    title: "Login",
    header: null
  };

  componentDidMount() {

  }

_renderChat = ({ item, index }) => {
    return (
      <View style={{width:screenWidth, height:screenHeight/3}}>
        <Image source={require("../assets/groupChat.png")} style={{width:screenWidth, height:screenHeight/3, resizeMode:"contain"}} />
      </View>
    );
}

_renderMask() {
  if (this.state.loadMask == true) {
    return (
      <Mask />
    )
  }
}

render() {
    return (
      <KeyboardAvoidingView behavior={Platform.OS == 'ios' ? "padding" : null} style={{ flex: 1, backgroundColor: "#bd376a" }}>
        <ImageBackground source={SplashBg} style={styles.mainBg}>
          <Header bgColor="transparent" headerText={this.state.headerText} onLeftPress={()=> this.props.navigation.goBack()} onRightPress={null} rightIcon={null} />
          <View style={{flex:1}}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={{flex: 1}}>
              <FlatList
                data={this.state.chatMsgs}
                style={{ marginBottom: 10, flex: 1 }}
                renderItem={this._renderChat} />
            </View>
          </ScrollView>
          <View style={{flexDirection:"row", width:"100%", height:70, backgroundColor:"#FFF"}}>
            <View style={{padding:10, borderRadius:5, marginHorizontal:10, flex:1}}>
                <Text style={{color:"#000", fontSize:13, paddingVertical:5, paddingHorizontal:10}}>Write Message...</Text>
            </View>
            <TouchableOpacity style={{width:50, height:50, alignItems:"center", justifyContent:"center"}}>
              <Icon type="Ionicons" style={{fontSize:22, textAlign:"center", color:"#000"}} name="md-send" />
            </TouchableOpacity>
          </View>
          </View>
        </ImageBackground>
        {this._renderMask()}
      </KeyboardAvoidingView>
    );
  }

}
