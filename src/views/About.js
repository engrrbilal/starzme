/* @flow */
import React, { Component } from 'react';
import {
  View,
  KeyboardAvoidingView,
  ScrollView,
  Image,
  TextInput,
  Text,
  ImageBackground,
  Dimensions,
  ActivityIndicator,
  AsyncStorage,
  TouchableOpacity,
  FlatList,
  StatusBar,
} from 'react-native';
import styles from '../styles/style';
import { NavigationActions } from 'react-navigation';
import { Baseurl, Mask, Header } from '../components/common';
import {Toast, Icon, ActionSheet} from 'native-base';
const {width, height} = Dimensions.get("window");
import HTML from 'react-native-render-html';

export default class About extends Component {
constructor(props) {
      super(props);
      const {state} = props.navigation;
      this.state = {
        type:state.params.screenType,
        headerText:"",
        user_Data:[],
        loadMask:true,
        aboutData:null
      }
}

componentDidMount(){
    this.getData()
}

async getData(){
    try {
      const value = await AsyncStorage.getItem('userData')

      if (value !== null){
        var data = JSON.parse(value)
        this.setState({user_Data:data})
        console.log(this.state.user_Data)

        if(this.state.type == "about"){
            this.setState({headerText:"About Us"})
            this.getAboutUs()
        }
        else {
            this.setState({headerText:"Privacy Policy"})
            this.getPrivacy()
        }

      }
      else {

      }
    }
    catch (error) {
       console.log(error)
    }
}

getAboutUs(){
      var userToken = this.state.user_Data.access_token
      fetch(Baseurl+"app-content?content_type=US", {
          method: 'GET',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+userToken,
          },
        })
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson)
          if("response" in responseJson){
              this.setState({aboutData:responseJson.response.data.content, loadMask:false})
          }
          else {
              this.setState({aboutData:[], loadMask:false})
              utils.showToast(responseJson.error.messages[0]);
          }
        })
        .catch((error) => {
              this.setState({aboutData:[], loadMask:false})
              utils.showToast("A network error occurred!");
        });
}

getPrivacy(){
      var userToken = this.state.user_Data.access_token
      fetch(Baseurl+"app-content?content_type=PP", {
          method: 'GET',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+userToken,
          },
        })
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson)
          if("response" in responseJson){
              this.setState({aboutData:responseJson.response.data.content, loadMask:false})
          }
          else {
              this.setState({aboutData:[], loadMask:false})
              utils.showToast(responseJson.error.messages[0]);
          }
        })
        .catch((error) => {
              this.setState({aboutData:[], loadMask:false})
              utils.showToast("A network error occurred!");
        });
}

_renderMask(){
    if(this.state.loadMask == true){
        return (
          <Mask />
        )
    }
}

getAboutData(){
  if(this.state.aboutData != null){
    //<Text style={styles.aboutText}>{this.state.aboutData}</Text>
    return (
      <View style={styles.cardView}>
          <HTML ignoredStyles={["font-family", "letter-spacing","font-weight"]} html={this.state.aboutData} />
      </View>
    )
  }
}

render() {
    return (
      <View style={[styles.dashboardMainView]}>
        <Header leftIcon="back" headerText={this.state.headerText} onLeftPress={()=> this.props.navigation.goBack()} onRightPress={null} />
        {this._renderMask()}
        <View style={styles.dashboardContainerView}>
          <ScrollView showsVerticalScrollIndicator={false} style={styles.marginTopMinus}>
            <View style={{flex:1, paddingHorizontal:20}}>
                {this.getAboutData()}
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}
