/* @flow */
import React, { Component } from 'react';
import {
  View,
  KeyboardAvoidingView,
  ScrollView,
  Image,
  TextInput,
  Text,
  ImageBackground,
  Dimensions,
  ActivityIndicator,
  AsyncStorage,
  TouchableOpacity,
  FlatList,
  StatusBar,
} from 'react-native';
import styles from '../styles/style';
import { NavigationActions } from 'react-navigation';
import { Baseurl, Mask, Header } from '../components/common';
import {Toast, Icon, ActionSheet} from 'native-base';
const {width, height} = Dimensions.get("window");

export default class NotificationDetail extends Component {
constructor(props) {
      super(props);
      const {state} = props.navigation;
      this.state = {
        headerText:state.params.heading,
        aboutData:state.params.data
      }
}

componentDidMount(){

}

getAboutData(){
  if(this.state.aboutData != null || this.state.aboutData != undefined){
    return (
      <View style={styles.cardView}>
          <ScrollView showsVerticalScrollIndicator={false} bounces={false}>
            <Text style={styles.aboutText}>{this.state.aboutData}</Text>
          </ScrollView>
      </View>
    )
  }
}

render() {
    return (
      <View style={[styles.dashboardMainView]}>
        <Header leftIcon="back" headerText={this.state.headerText} onLeftPress={()=> this.props.navigation.goBack()} onRightPress={null} />
        <View style={styles.dashboardContainerView}>
          <View style={[styles.marginTopMinus, {minHeight:height/2, flex:1}]}>
              <View style={{flex:1, paddingHorizontal:20}}>
                  {this.getAboutData()}
              </View>
          </View>
        </View>
      </View>
    );
  }
}
