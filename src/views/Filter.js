/* @flow */
import React, { Component } from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  ImageBackground,
  ActivityIndicator,
  AsyncStorage,
  Dimensions,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  Keyboard,
  StatusBar,
  FlatList,
  Platform
} from 'react-native';
import { Header, CameraIcon, ButtonBg, Mask, Baseurl } from '../components/common';
import styles,{width} from '../styles/style';
import { NavigationActions, StackActions } from 'react-navigation';
import {ActionSheet} from 'native-base';
import Modal from 'react-native-modalbox';
import utils from '../utils';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

const swipeThresholdHeight = 50
const swipeAreaHeight = screenHeight / 3

var BUTTONS = ["Male", "Female", "Cancel"];
var CANCEL_INDEX = 2;

var BUTTONS_ = ["0", "1", "2", "3", "4", "5", "Cancel"];
var CANCEL_INDEX_ = 6;

export default class Filter extends Component {
constructor(props) {
        super(props);
        const {state} = props.navigation;
        this.state = {
          headerText:"Filter",
          isOpen: false,
          isDisabled: false,
          swipeToClose: true,
          locations:[],
          cityText:"Select City",
          city_id:null,
          loadMask:false,
          user_Data:null,
          genderText:"Select Gender",
          serviceText:"Select Service",
          service_id:null,
          ratingText:"Select Rating",
          modalType:1,
          services:[]
        }
}

static navigationOptions = {
    title: "Filter",
    header:null
};

componentDidMount(){
  this.getData()
}

async getData(){
    try {
      const value = await AsyncStorage.getItem('userData')
      //console.log(udidToken)
      if (value !== null){
        var data = JSON.parse(value);
        this.setState({
          user_Data:data
        })
        console.log(this.state.user_Data)
      }
      else {

      }
    }
    catch (error) {
        alert(error)
    }
}

getGymsByFilter(){
  if(this.state.city_id == null){
      utils.showToast("Please select city to continue!");
  }
  else {
     this.props.navigation.navigate("FilterResults", {cityid:this.state.city_id, gender:this.state.genderText, rating:this.state.ratingText, serviceid:this.state.service_id})
  }
}

openActionsheet(type){
  if(type == "rating"){
    ActionSheet.show (
      {
        options:BUTTONS_,
        cancelButtonIndex: CANCEL_INDEX_,
        title: "Rating"
      },
      buttonIndex => {

        if(buttonIndex != 6){
            this.setState({ ratingText: BUTTONS_[buttonIndex]});
        }

      }
   )
  }
  else {
    ActionSheet.show (
      {
        options:BUTTONS,
        cancelButtonIndex: CANCEL_INDEX,
        title: "Gender"
      },
      buttonIndex => {

        if(buttonIndex != 2){
            this.setState({ genderText: BUTTONS[buttonIndex]});
        }

      }
   )
  }
}

getServices(){
      this.setState({ loadMask: true })
      this.openModal(2)
      fetch(Baseurl+"admin/service/list", {
          method: 'GET',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
        })
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson)
          if("response" in responseJson){
              this.setState({services:responseJson.response.data, loadMask:false})
          }
          else {
              this.setState({services:[], loadMask:false})
              utils.showToast(responseJson.error.messages[0]);
          }
        })
        .catch((error) => {
              this.setState({services:[], loadMask:false})
              utils.showToast("A network error occurred!");
        });
}

getCities(){
      this.setState({ loadMask: true })
      this.openModal(1)
      fetch(Baseurl+"city/list", {
          method: 'GET',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
        })
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson)
          if("response" in responseJson){
              this.setState({locations:responseJson.response.data, loadMask:false})
          }
          else {
              this.setState({locations:[], loadMask:false})
              utils.showToast(responseJson.error.messages[0]);
          }
        })
        .catch((error) => {
              this.setState({locations:[], loadMask:false})
              utils.showToast("A network error occurred!");
        });
}

openModal(type) {
  if(type == 1){
    this.refs.modal1.open()
    this.setState({ modalType: 1 })
  }
  else {
    this.refs.modal1.open()
    this.setState({ modalType: 0 })
  }
}

close_Modal() {
  this.refs.modal1.close()
  this.setState({ isOpen: false })
}

_onLocationSelect = (id, name) => {
  this.setState({
    city_id: id,
    cityText: name
  })
  this.refs.modal1.close()
}

_renderLocations = ({ item, index }) => {
  var locationTitle = item.name
  return (
    <TouchableOpacity onPress={() => this._onLocationSelect(item.id, item.name)}>
      <View style={{ flex: 1, borderWidth: 0.2, borderColor: "#8a93a5", borderRadius: 5, marginVertical: 5, marginHorizontal: 10, paddingHorizontal:10, backgroundColor:"#FFF" }}>
        <View style={{ flexDirection: "row" }}>
          <View style={{ flex: 1, paddingVertical: 5, flexDirection: "column" }}>
            <Text numberOfLines={1} style={[styles.fontBook, { color: "#000", fontSize: 16, paddingLeft: 5, paddingVertical: 10 }]}>{locationTitle}</Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
}

_onServiceSelect = (id, name) => {
  this.setState({
    service_id: id,
    serviceText: name
  })
  this.refs.modal1.close()
}

_renderServices = ({ item, index }) => {
  var serviceTitle = item.title
  return (
    <TouchableOpacity onPress={() => this._onServiceSelect(item.id, item.title)}>
      <View style={{ flex: 1, borderWidth: 0.2, borderColor: "#8a93a5", borderRadius: 5, marginVertical: 5, marginHorizontal: 10, paddingHorizontal:10, backgroundColor:"#FFF" }}>
        <View style={{ flexDirection: "row" }}>
          <View style={{ flex: 1, paddingVertical: 5, flexDirection: "column" }}>
            <Text numberOfLines={1} style={[styles.fontBook, { color: "#000", fontSize: 16, paddingLeft: 5, paddingVertical: 10 }]}>{serviceTitle}</Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
}

modalHeader() {
  if(this.state.modalType == 1){
    return (
      <Header leftIcon="back" headerText="Select City" onLeftPress={()=> this.close_Modal()} onRightPress={null} />
    )
  }
  else {
    return (
      <Header leftIcon="back" headerText="Select Service" onLeftPress={()=> this.close_Modal()} onRightPress={null} />
    )
  }

}

renderModal() {
  if(this.state.modalType == 1){
    return (
      <Modal
        style={[styles.modal, styles.modal1]}
        ref={"modal1"}
        swipeToClose={this.state.swipeToClose}
        onClosed={this.onClose}
        onOpened={this.onOpen}
        animationDuration={500}
        swipeArea={swipeAreaHeight}
        onClosingState={this.onClosingState}>

        <View style={{ flex: 1 }}>
          <StatusBar barStyle="light-content" />
          {this.modalHeader()}
          <View style={[styles.marginTopMinus, { flex: 1 }]}>
            <View style={{ flex: 1 }}>
              <FlatList
                data={this.state.locations}
                renderItem={this._renderLocations} />
            </View>
          </View>
        </View>

      </Modal>
    )
  }
  else {
    return (
      <Modal
        style={[styles.modal, styles.modal1]}
        ref={"modal1"}
        swipeToClose={this.state.swipeToClose}
        onClosed={this.onClose}
        onOpened={this.onOpen}
        animationDuration={500}
        swipeArea={swipeAreaHeight}
        onClosingState={this.onClosingState}>

        <View style={{ flex: 1 }}>
          <StatusBar barStyle="light-content" />
          {this.modalHeader()}
          <View style={[styles.marginTopMinus, { flex: 1 }]}>
            <View style={{ flex: 1 }}>
              <FlatList
                data={this.state.services}
                renderItem={this._renderServices} />
            </View>
          </View>
        </View>

      </Modal>
    )
  }

}

_renderMask(){
    if(this.state.loadMask == true){
        return (
          <Mask />
        )
    }
}

render() {
    return (
      <KeyboardAvoidingView behavior={Platform.OS == 'ios' ? "padding" : null} style={{flex:1, backgroundColor:"#FFF"}}>
        <View style={[styles.dashboardMainView]}>
         {this.renderModal()}
         {this._renderMask()}
          <Header leftIcon="back" headerText={this.state.headerText} onLeftPress={()=> this.props.navigation.goBack()} onRightPress={null} />
            <View style={{flex:1}}>
              <ScrollView showsVerticalScrollIndicator={false} style={styles.marginTopMinus}>
                <View style={{flex:1, paddingHorizontal:20}}>
                    <View style={styles.cardView}>
                      <View style={{flex:1}}>
                        <TouchableOpacity onPress={()=> this.getCities()} style={styles.labelContainerInner}>
                          <View style={{flex:1}}>
                              <Text style={styles.labelFormInner}>By City</Text>
                              <Text style={styles.formInputInnerText}>{this.state.cityText}</Text>
                          </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=> this.openActionsheet("gender")} style={styles.labelContainerInner}>
                          <View style={{flex:1}}>
                              <Text style={styles.labelFormInner}>By Gender</Text>
                              <Text style={styles.formInputInnerText}>{this.state.genderText}</Text>
                          </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=> this.getServices()} style={styles.labelContainerInner}>
                          <View style={{flex:1}}>
                              <Text style={styles.labelFormInner}>By Service</Text>
                              <Text style={styles.formInputInnerText}>{this.state.serviceText}</Text>
                          </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=> this.openActionsheet("rating")} style={styles.labelContainerInner}>
                          <View style={{flex:1}}>
                              <Text style={styles.labelFormInner}>By Rating</Text>
                              <Text style={styles.formInputInnerText}>{this.state.ratingText}</Text>
                          </View>
                        </TouchableOpacity>
                      </View>

                      <ButtonBg onPress={()=> this.getGymsByFilter()} label="SEARCH" />
                    </View>
                </View>
              </ScrollView>
            </View>
          </View>
      </KeyboardAvoidingView>
    );
  }

}
