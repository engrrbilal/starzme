/* @flow */
import React, { Component } from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  ImageBackground,
  ActivityIndicator,
  AsyncStorage,
  Dimensions,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  Keyboard,
  Platform
} from 'react-native';
import { Header, CameraIcon, ButtonBg, Mask, Baseurl } from '../components/common';
import styles,{width} from '../styles/style';
import utils from '../utils';
import { NavigationActions, StackActions } from 'react-navigation';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

export default class ChangePassword extends Component {
constructor(props) {
        super(props);
        const {state} = props.navigation;
        this.state = {
          headerText:"Change Password",
          loadMask:false,
          user_Data:null,
        }
}

static navigationOptions = {
    title: "ChangePassword",
    header:null
};

componentDidMount(){
  this.getData()
}

async getData(){
    try {
      const value = await AsyncStorage.getItem('userData')
      //console.log(udidToken)
      if (value !== null){
        var data = JSON.parse(value);
        this.setState({
          user_Data:data
        })
        console.log(this.state.user_Data)
      }
      else {

      }
    }
    catch (error) {
        alert(error)
    }
}

_onSubmitEditing = (cur, next) => {
    if (next != null) {
      this.refs[next].focus()

    } else {
      this.refs[cur].blur()
    }
}

submitChangePassword(){
  payload = {
    old_password : this.user_pass,
    new_password : this.new_pass
  }

   if(payload.old_password == undefined || payload.old_password == "") {
      utils.showToast('Please enter your current password!');
   }
   else if(payload.new_password == undefined || payload.new_password == ""){
      utils.showToast('Please enter new password!');
   }
   else if(payload.new_password.length <= 5){
      utils.showToast('Password must be atleast 6 characters!');
   }
   else if(this.confirm_pass == undefined || this.confirm_pass == "") {
      utils.showToast('Please confirm your new password!');
   }
   else if(payload.new_password != this.confirm_pass){
      utils.showToast('New password and confirm password do not match!');
   }
   else {
         console.log(payload)
         var userToken = this.state.user_Data.access_token
         this.setState({loadMask:true});
           fetch(Baseurl+'changepassword', {
               method: 'POST',
               headers: {
                 'Accept': 'application/json',
                 'Content-Type': 'application/json',
                 'Authorization': 'Bearer ' + userToken,
               },
               body: JSON.stringify(payload),
             })
             .then((response) => response.json())
             .then((responseJson) => {
               console.log(responseJson)
               this.setState({loadMask:false});
               if ("response" in responseJson) {
                 utils.showToast('Your password has been changed!');
                 const resetAction = StackActions.reset({
                   index: 0,
                   actions: [
                     NavigationActions.navigate({ routeName: 'Dashboard' })
                   ],
                 });

                 this.props.navigation.dispatch(resetAction);

               }
               else {
                 utils.showToast(responseJson.error.messages[0]);
               }

             })
             .catch((error) => {
               this.setState({loadMask:false});
               utils.showToast("A network error occurred!");
             });
    }

}

_renderMask(){
    if(this.state.loadMask == true){
        return (
          <Mask />
        )
    }
}

render() {
    return (
      <KeyboardAvoidingView behavior={Platform.OS == 'ios' ? "padding" : null} style={{flex:1, backgroundColor:"#FFF"}}>
        <View style={[styles.dashboardMainView]}>
         {this._renderMask()}
          <Header leftIcon="back" headerText={this.state.headerText} onLeftPress={()=> this.props.navigation.goBack()} onRightPress={null} />
            <View style={{flex:1}}>
              <ScrollView bounces={false} showsVerticalScrollIndicator={false} style={styles.marginTopMinus}>
                <View style={{flex:1, paddingHorizontal:20}}>
                    <View style={styles.cardView}>
                      <View style={{flex:1}}>
                        <View style={styles.labelContainerInner}>
                          <View style={{flex:1}}>
                              <Text style={styles.labelFormInner}>Current Password</Text>
                              <TextInput
                              style={styles.formInputInner}
                              placeholderTextColor={"#959595"}
                              underlineColorAndroid="transparent"
                              ref={'oldpass'}
                              secureTextEntry
                              returnKeyType="next"
                              onSubmitEditing={() => this._onSubmitEditing('oldpass', 'newpass')}
                              onChangeText={(text) => this.user_pass = text}
                              placeholder="Enter current password" />
                          </View>
                        </View>
                        <View style={styles.labelContainerInner}>
                          <View style={{flex:1}}>
                              <Text style={styles.labelFormInner}>New Password</Text>
                              <TextInput
                              style={styles.formInputInner}
                              placeholderTextColor={"#959595"}
                              underlineColorAndroid="transparent"
                              ref={'newpass'}
                              secureTextEntry
                              returnKeyType="next"
                              onSubmitEditing={() => this._onSubmitEditing('newpass', 'cpass')}
                              onChangeText={(text) => this.new_pass = text}
                              placeholder="Enter new password" />
                          </View>
                        </View>
                        <View style={styles.labelContainerInner}>
                          <View style={{flex:1}}>
                              <Text style={styles.labelFormInner}>Confirm Password</Text>
                              <TextInput
                              style={styles.formInputInner}
                              placeholderTextColor={"#959595"}
                              underlineColorAndroid="transparent"
                              ref={'cpass'}
                              secureTextEntry
                              returnKeyType="done"
                              onSubmitEditing={() => this._onSubmitEditing('cpass', null)}
                              onChangeText={(text) => this.confirm_pass = text}
                              placeholder="Confirm new password" />
                          </View>
                        </View>
                      </View>
                      <ButtonBg onPress={()=> this.submitChangePassword()} label="SUBMIT" />
                    </View>
                </View>
              </ScrollView>
            </View>
          </View>
      </KeyboardAvoidingView>
    );
  }

}
