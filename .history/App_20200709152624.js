import React from 'react';
import { AppRegistry, SafeAreaView, StatusBar } from 'react-native';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';

// import AppReducer from './reducers';
import AppNavigator from './src/AppNavigator';

// const store = createStore(AppReducer, applyMiddleware(middleware));

class App extends React.Component {
  render() {
    return (
      <AppNavigator />
    );
  }
}


export default App;
