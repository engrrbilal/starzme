import React from 'react';
import { AppRegistry,SafeAreaView,StatusBar } from 'react-native';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';

import AppReducer from './redux/rootReducer';
import { AppNavigator } from './AppNavigator';

// const store = createStore(AppReducer, null);

class App extends React.Component {
  render() {
    return (
      // <Provider store={store}>
      //     <AppNavigator />
      // </Provider>
      <AppNavigator />
    );
  }
}
//<SafeAreaView style={{flex: 1, backgroundColor: '#fcfcfc'}}>

// AppRegistry.registerComponent('App', () => App);

export default App;
