import React from 'react';
import { AppRegistry,SafeAreaView,StatusBar } from 'react-native';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';

import AppReducer from './redux/reducers';
import { AppNavigator, middleware } from './AppNavigator';

const store = createStore(AppReducer, applyMiddleware(middleware));

class ReduxExampleApp extends React.Component {
  render() {
    return (
      <Provider store={store}>
          <AppNavigator />
      </Provider>
    );
  }
}
//<SafeAreaView style={{flex: 1, backgroundColor: '#fcfcfc'}}>

AppRegistry.registerComponent('ReduxExample', () => ReduxExampleApp);

export default ReduxExampleApp;
