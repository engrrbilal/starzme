import React from 'react';
import { AppRegistry,SafeAreaView,StatusBar } from 'react-native';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';

import AppReducer from './redux/rootReducer';
import { AppNavigator } from './AppNavigator';

const store = createStore(AppReducer, null);

class ReduxExampleApp extends React.Component {
  render() {
    return (
      <Provider store={store}>
          <AppNavigator />
      </Provider>
    );
  }
}
//<SafeAreaView style={{flex: 1, backgroundColor: '#fcfcfc'}}>

AppRegistry.registerComponent('ReduxExample', () => ReduxExampleApp);

export default ReduxExampleApp;
