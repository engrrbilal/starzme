/* @flow */
import React, { Component, useState } from 'react';
import {
  View,
  KeyboardAvoidingView,
  ScrollView,
  Image,
  TextInput,
  Text,
  ImageBackground,
  Dimensions,
  ActivityIndicator,
  AsyncStorage,
  TouchableOpacity,
  FlatList,
  StatusBar,
  Platform,
  StyleSheet,
  Modal
} from 'react-native';
import styles from '../styles/style';
import { CommonActions, StackActions } from '@react-navigation/native';
import ImagePicker from 'react-native-image-picker';
import countryArray from '../utils/countryArray';
import { ColorPicker, fromHsv } from 'react-native-color-picker'

import {
  Baseurl,
  Mask,
  VideoHeader,
  UserSubHeader,
  Banner,
  PlaceHolder,
  PlaceHolderSmall,
  ButtonSmall,
  LocationIcon,
  PaymentBg,
  NotificationIcon,
  Rating,
  SplashBg,
  StateDropDown,
  StarzMenu,
  VideosMenu,
  BottomBar,
  TalentFilter,
  UploadVideoButton
} from '../components/common';
import {
  MasonryListItem
} from '../components/items';
const {width, height} = Dimensions.get("window");
import utils from '../utils';
import * as Animatable from 'react-native-animatable';
import { Container, Content, Picker } from 'native-base';
import axios from 'axios';
import Icon from 'react-native-vector-icons/FontAwesome';

var options = {
  title: 'Upload Video',
  videoQuality:'low',
  mediaType:"video",
  durationLimit:30,
  allowsEditing:true,
  storageOptions: {
    waitUntilSaved:true,
    cameraRoll:true
  }
};

export default class Profile extends Component {
  constructor(props) {
      super(props);
      const {state} = props.navigation;
      this.state = {
        userData:{},
        headerText:"",
        loadMask:false,
        imageTaken:false,
        resultVideo:null,
        profileImage:props.navigation.state.params.userData.UserImage || "https://www.irandefence.net/wp-content/themes/shadower/assets/images/default-cover.jpg",
        profileCoverImage:props.navigation.state.params.userData.UserCoverImage || "https://www.irandefence.net/wp-content/themes/shadower/assets/images/default-cover.jpg",
        country:props.navigation.state.params.userData.CountryName || "",
        phone:props.navigation.state.params.userData.PhoneNumber || "",
        zipCode:props.navigation.state.params.userData.ZipCode || "",
        font:props.navigation.state.params.userData.Fonts || "",
        color:props.navigation.state.params.userData.Color || "",
        qouteMessage: "",
        state:props.navigation.state.params.userData.StateId || "",
        talent:props.navigation.state.params.userData.Talent || "",
        talentData:[],
        stateData:[],
        showColorPicker:false,
        showPasswordConfirmModal:false,
        passwordOld:"",
        passwordNew:"",
        passwordConfirm:"",
        loading:false,
        profileImageObject:{},
        profileCoverImageObject:{}
      }
}
_changeState = (e) =>{
  let userID = this.props.navigation.state.params.userData.userID
  let stateID = this.props.navigation.state.params.userData.stateID
  fetch(`http://jemstech.net/StarzMe/WebApi/ChangeState?UserId=${userID}&StateId=${stateID}`)
  .then(res=>{
    return res.json()
  })
  .then(res=>{
    if(res.status){
      this.setState({state:e})
    }
    alert(res.message)
  })
  .catch(err=>{
    console.log(err)
  })
}

_getState = () =>{
  fetch('http://jemstech.net/StarzMe/WebApi/GetStates')
  .then(res=>{
    return res.json()
  })
  .then(res=>{
    console.log(res, ' get state')
    let data = res.data.map(res=> {
      return {
        lable:res.StateName,
        value: res.StateId
      }
    })
    console.log(data, ' data state')
    this.setState({
      stateData:data
    })
  })
  .catch(err=>{
    console.log(err)
  })
}

getTalent = () =>{
  fetch('http://jemstech.net/StarzMe/WebApi/GetTalent')
  .then(res=>{
    return res.json()
  })
  .then(res=>{
    console.log(res, ' get talent')
    let data = res.data.map(res=> {
      return {
        lable:res.TalentName,
        value: res.TalentId
      }
    })
    console.log(data, ' data sssss')
    this.setState({
      talentData:data
    })
  })
  .catch(err=>{
    console.log(err)
  })
}

_getUserData = ()=>{
  let userID = this.props.navigation.state.params.userData
  fetch('http://jemstech.net/StarzMe/WebApi/ViewProfile?UserId='+userID.UserId)
  .then(res=>{
    return res.json()
  })
  .then(res=>{
    console.log(res, ' userData')
    this.setState({
      userData:res.data,
      font:res.data.Fonts,
      country:res.data.CountryName,
      qouteMessage:res.data.QuoteMessage,
      color:res.data.Color,
      profileCoverImage:res.data.UserCoverImage,
      profileImage:res.data.UserImage,
    })
  })
  .catch(err=>{
    console.log(err)
  })
}

componentDidMount(){
  this._getUserData()
  this.getTalent()
  this._getState()
}

componentWillUnmount(){

}

openImageGalleryOptions(type){
  console.log('Rcalllll = ');
  const options = {
    title: 'Select Avatar',
    customButtons: [{ name: 'Gallery', title: 'Choose Photo from Gallery' }],
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };
  ImagePicker.launchImageLibrary(options, (response) => {
  console.log('Response = ', response);
    if(response.uri){
      if(type == "cover"){
        this.setState({
          profileCoverImage:'data:image/jpeg;base64,'+ response.data,
          profileImageObject:response
        });
      }
      else{
        this.setState({
          profileImage:'data:image/jpeg;base64,' + response.data,
          profileCoverImageObject:response
        });
      }
    }
  });
}

_submiteProfile = () =>{
  this.setState({loading:true})
  var {
    country,
    phone,
    zipCode,
    font,
    color,
    qouteMessage,
    state,
    talent,
    profileCoverImageObject,
    userData,
    profileImageObject,
    profileImage,
    profileCoverImage
  } = this.state;

  var userID = userData.UserId;
  console.log(userID, ' userID')
  var firstName = userData.FirstName || "";
  var lastName = userData.LastName || "";
  var formdata = new FormData()
  formdata.append("UserId",userID)
  formdata.append("FirstName",firstName)
  formdata.append("LastName",lastName)
  formdata.append("PhoneNumber",phone)
  formdata.append("CountryName",country)
  formdata.append("ZipCode",zipCode)
  formdata.append("Talent",talent)
  formdata.append("Color",color)
  formdata.append("Fonts",font)
  formdata.append("QuoteMessage",qouteMessage)
  if(profileImageObject.uri && profileCoverImageObject.uri){
    formdata.append("UserImage", {
      type: 'image/jpeg',
      name: profileImageObject.fileName,
      uri:profileImageObject.uri
    });
    formdata.append("UserCoverImage", {
      type: 'image/jpeg',
      name: profileCoverImageObject.fileName,
      uri:profileCoverImageObject.uri
    });
  }
  else{
    formdata.append("UserImage",profileImage)
    formdata.append("UserCoverImage",profileCoverImage)
  }
  fetch('http://jemstech.net/StarzMe/WebApi/EditProfile',{
      method:"POST",
      headers:{
        "Content-Type":"multipart/form-data",
      },
      body:formdata
    })
    .then(res=> {
      return res.json()
    })
    .then(res=> {
      console.log(res, 'confirm')
      // alert(res.message)
      // this.setState({loading:false})

    })
    .catch(err=> {
      console.log(err)
    })
}
_changePassword = () =>{
  this.setState({loading:true})
  var {
    passwordOld,
    passwordNew,
    passwordConfirm,
    userData
  } = this.state;
  var userID = userData.UserId;
  var formData = new FormData()
  if(passwordNew == passwordConfirm){
    formData.append("UserId",userID)
    formData.append("Password", passwordNew)
    formData.append("PreviousPassword", passwordOld)

    fetch('http://jemstech.net/StarzMe/WebApi/UpdatePassword',{
      method:"POST",
      headers:{
        "Content-Type":"application/json",
      },
      body:JSON.stringify({
        UserId:userID,
        Password:passwordNew,
        PreviousPassword:passwordOld
      })
    })
    .then(res=> {
      return res.json()
    })
    .then(res=> {
      console.log(res, 'change pass')
      this._getUserData()
      this.OldtextInput.clear()
      this.NewtextInput.clear()
      this.ConfirmtextInput.clear()

      this.setState({
        loading:false,
        passwordOld:"",
        passwordNew:"",
        passwordConfirm:""
      })

      alert(res.message)
    })
    .catch(err=> {
      console.log(err, 'change pass err')

    })
  }
  else{
    alert("Password did not matched!")
  }
}
render() {
    let firstName = this.state.userData.FirstName || "";
    let lastName = this.state.userData.LastName || "";
    const {width, height} = Dimensions.get("window");
    return (
      <View style={styles.dashboardMainView}>
        <ChangePasswordModal
          isVisible={this.state.showPasswordConfirmModal}
          onClose={()=>this.setState({showPasswordConfirmModal:false})}
          onChangeOld={(e)=>this.setState({passwordOld:e})}
          OldValue={this.state.passwordOld}
          Oldref={input => { this.OldtextInput = input }}
          onChangeNew={(e)=>this.setState({passwordNew:e})}
          NewValue={this.state.passwordNew}
          Newref={input => { this.NewtextInput = input }}
          onChangeConfirm={(e)=>this.setState({passwordConfirm:e})}
          ConfirmValue={this.state.passwordConfirm}
          Confirmref={input => { this.ConfirmtextInput = input }}
          onSubmit={()=>this._changePassword()}
          loading={this.state.loading}
        />
        <RenderColorPicker
          defaultColor={this.state.color}
          isVisible={this.state.showColorPicker}
          onClose={()=>this.setState({showColorPicker:false})}
          onChange={(e)=>{
            console.log(e)
            this.setState({color:e})
          }}/>
          <ImageBackground 
              style={{width:"100%", minHeight:height * 0.2, justifyContent:"space-between"}} 
              resizeMode="cover" 
              source={{uri:this.state.profileCoverImage}}>
              <VideoHeader 
                bgColor="transparent"
                headerText={this.state.headerText}
                onLeftPress={()=> this.props.navigation.goBack()} 
                type="onAir" />
              <Text style={[styles.headerHeading, {fontFamily:this.state.font, color:this.state.color}]}>
                  {this.state.qouteMessage}
              </Text>
              <View style={styles.profileImageParentContainer}>
                <View style={{flexDirection:'row', alignItems:'center'}}>
                  <TouchableOpacity onPress={()=>this.openImageGalleryOptions("profile")} style={styles.profileImageContainer}>
                    <Image style={styles.profileImage} 
                      resizeMode="cover" 
                      source={{uri:this.state.profileImage}}/>
                  </TouchableOpacity>
                  <Text numberOfLines={1} style={styles.userName}>{firstName + "  " + lastName}</Text>
                </View>
                <TouchableOpacity onPress={()=>this.openImageGalleryOptions("cover")} style={{width:30, height:30, borderRadius:50, backgroundColor:'gray', justifyContent:'center', alignItems:'center'}}>
                  <Icon name={"camera"} size={20} color="#fff" />
                </TouchableOpacity>
              </View>
            </ImageBackground>
            <ScrollView showsVerticalScrollIndicator={false} style={styles.cardParent}>
              <View style={styles.cardContainer}>
                <View style={styles.cardHeadingContainer}>
                    <Text style={styles.accountText}>Account</Text>
                </View>
                <View>
                  <InputWithIconAndDropwdown
                    value={this.state.country}
                    data={countryArray}
                    IconName="caret-down"
                    onChangeText={(e)=>{
                      console.log(e, ' eee')
                      this.setState({country:e})
                    }}
                    inputType="dropdown" 
                    placeholder={"New York"}
                    heading={"Country"}/>
                  <InputWithIconAndDropwdown
                    // disabled={true}
                    // value={this.state.password}
                    selectedColor={"*******"}
                    onPress={()=>this.setState({showPasswordConfirmModal:true})}
                    isModal={true}
                    IconName="edit"
                    secureTextEntry={true}
                    onChangeText={(e)=>{console.log(e)}}
                    inputType="default"
                    placeholder={"********"} 
                    heading={"Password"}/>
                  <InputWithIconAndDropwdown
                    value={this.state.phone}
                    IconName="edit"
                    onChangeText={(e)=>{this.setState({phone:e})}}
                    inputType="number-pad" 
                    placeholder={"03152498159"} 
                    heading={"Phone"}/>
                  <InputWithIconAndDropwdown
                    value={this.state.zipCode}
                    IconName="edit"
                    onChangeText={(e)=>{this.setState({zipCode:e})}}
                    inputType="number-pad" 
                    placeholder={"1235"} 
                    heading={"Zip Code"}/>
                  <InputWithIconAndDropwdown
                    value={this.state.font}
                    data={[
                      {lable:"Montserrat Black", value:"Montserrat-Black"},
                      {lable:"Montserrat BoldItalic", value:"Montserrat-BoldItalic"},
                      {lable:"MONTSERRAT ULTRALIGHT", value:"MONTSERRAT-ULTRALIGHT"},
                    ]}
                    IconName="caret-down"
                    onChangeText={(e)=>{this.setState({font:e})}}
                    inputType="dropdown" 
                    placeholder={"Robto"} 
                    heading={"Quote Font"}/>
                  <InputWithIconAndDropwdown
                    selectedColor={this.state.color}
                    onPress={()=>this.setState({showColorPicker:true})}
                    isModal={true}
                    value={this.state.color}
                    IconName="tint"
                    onChangeText={(e)=>{this.setState({color:e})}}
                    inputType="default" 
                    placeholder={"Red"} 
                    heading={"Font Color"}/>
                  <InputWithIconAndDropwdown
                    value={this.state.qouteMessage}
                    IconName="edit"
                    onChangeText={(e)=>{
                      console.log(e)
                      this.setState({qouteMessage:e})
                    }}
                    inputType="default" 
                    placeholder={"message"} 
                    heading={"Quote Message"}/>
                  <InputWithIconAndDropwdown
                    value={this.state.state}
                    data={this.state.stateData}
                    IconName="caret-down"
                    onChangeText={(e)=>{
                      console.log("sdsadasdas dsadasda")
                      this._changeState(e)
                    }}
                    inputType="dropdown" 
                    placeholder={"Albio"} 
                    heading={"State"}/>
                  <InputWithIconAndDropwdown
                    value={this.state.talent}
                    data={this.state.talentData}
                    IconName="caret-down"
                    onChangeText={(e)=>{this.setState({talent:e})}}
                    inputType="dropdown" 
                    placeholder={"Dance"} 
                    heading={"Talent"}/>
                </View>
              </View>
              <TouchableOpacity onPress={()=>{
                this._submiteProfile()
              }} style={styles.confirmButtom}>
                {!this.state.loading ? <Text style={{color:'red'}}>Submit</Text> :  <ActivityIndicator size="small" color="red" />}
              </TouchableOpacity>
          </ScrollView>
      </View>
    );
  }
}

const RenderColorPicker = (props) =>{
  return(
    <Modal
        animationType="slide"
        transparent={true}
        visible={props.isVisible}>
        <View style={modalStyles.centeredView}>
            <TouchableOpacity 
              onPress={props.onClose} 
              style={{padding:20}}>
                <Icon name={"times"} size={20} color="gray" />
            </TouchableOpacity>
            <ColorPicker
              // oldColor={props.defaultColor}
              hideSliders={true}
              onColorChange={color => props.onChange(fromHsv(color))}
              style={{width:'100%', height:'100%'}}
            />
        </View>
      </Modal>
  )
}

const ChangePasswordModal = (props) =>{
  return(
    <Modal
        animationType="slide"
        transparent={true}
        visible={props.isVisible}>
        <View style={modalStyles.centeredView}>
          <TouchableOpacity 
              onPress={props.onClose} 
              style={{padding:20}}>
                <Icon name={"times"} size={20} color="gray" />
          </TouchableOpacity>
          <View style={{width:'100%', height:'100%', paddingHorizontal:20}}>
            <Text style={[styles.cardInputHeadings, {marginBottom:10}]}>Old Password</Text>
            <TextInput
              ref={props.Oldref}
              value={props.OldValue}
              secureTextEntry={true}
              onChangeText={props.onChangeOld}
              keyboardType={"default"} 
              placeholder={"******"} 
              style={{width:'100%', height:50,marginBottom:20}}/>  

            <Text style={[styles.cardInputHeadings, {marginBottom:10}]}>New Password</Text>
            <TextInput
              ref={props.Newref}
              value={props.NewValue}
              secureTextEntry={true}
              onChangeText={props.onChangeNew}
              keyboardType={"default"} 
              placeholder={"******"} 
              style={{width:'100%', height:50,marginBottom:20}}/>  

            <Text style={[styles.cardInputHeadings, {marginBottom:10}]}>Confirm Password</Text>
            <TextInput
              ref={props.Confirmref}
              value={props.ConfirmValue}
              secureTextEntry={true}
              onChangeText={props.onChangeConfirm}
              keyboardType={"default"} 
              placeholder={"******"} 
              style={{width:'100%', height:50,marginBottom:20}}/>
              
              <TouchableOpacity onPress={props.onSubmit} style={styles.confirmButtom}>
                {!props.loading ? <Text style={{color:'red'}}>Submit</Text> :  <ActivityIndicator size="small" color="red" />}
              </TouchableOpacity>
          </View>
        </View>
      </Modal>
  )
}


const modalStyles = StyleSheet.create({
  centeredView: {
    position:'absolute',
    zIndex:20000000,
    backgroundColor:'#fff',
    height:'100%',
    width:'100%',

  }
});

const RenderPicker = (props) =>{
  return(
      <Container style= {{width:width * 0.7, height:50}}>
          <Picker
              activeLabelStyle={{color: 'gray'}}
              placeholderStyle={{color: 'gray'}}
              style= {{backgroundColor:'#fff'}}
              enabled={true}
              iosHeader={props.iosHeaderText}
              mode="dropdown"
              selectedValue={props.value}
              itemStyle={{padding:0}}
              onValueChange={(v)=>{
                if(v !== props.value){
                  props.onChangeText(v)
                }
              }}>
              {props.data.map((res,i)=>{
                return(
                  <Picker.Item key={i} label={res.lable} value={res.value}/>
                )
              })}
          </Picker>
      </Container>
  )
}



const InputWithIconAndDropwdown = (props) =>{
  return(
    <View style={styles.cardInputContainer}>
      <View>
        <Text style={styles.cardInputHeadings}>{props.heading}</Text>
        {!props.isModal ? 
          (props.inputType !== "dropdown" ? <TextInput
            disabled={props.disabled}
            value={props.value}
            secureTextEntry={props.secureTextEntry || false}
            onChangeText={(e)=>{props.onChangeText(e)}}
            keyboardType={props.inputType} 
            placeholder={props.placeholder} 
            style={styles.cardInput}/> : <RenderPicker
            value={props.value}
            onChangeText={props.onChangeText}
            iosHeaderText={props.heading}
            onChange={props.onChange}
            data={props.data}/>)
          :
           <TouchableOpacity onPress={props.onPress} style={styles.cardInput}>
            <Text>{props.selectedColor}</Text>
          </TouchableOpacity>
          }
      </View>
      <View style={styles.iconContainer}>
        <Icon name={props.IconName} size={20} color="red" />
      </View>
    </View>
  )
}

const pickerStyles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 40,
    alignItems: "center",
  }
});