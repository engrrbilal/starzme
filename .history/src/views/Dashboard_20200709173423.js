/* @flow */
import React, { Component } from 'react';
import {
  View,
  KeyboardAvoidingView,
  ScrollView,
  Image,
  TextInput,
  Text,
  ImageBackground,
  Dimensions,
  ActivityIndicator,
  TouchableOpacity,
  FlatList,
  StatusBar,
  Platform
} from 'react-native';
import styles from '../styles/style';
import { CommonActions, StackActions } from '@react-navigation/native';

import {
  Baseurl,
  Mask,
  Header,
  UserSubHeader,
  Banner,
  PlaceHolder,
  PlaceHolderSmall,
  ButtonSmall,
  LocationIcon,
  PaymentBg,
  NotificationIcon,
  Rating,
  SplashBg,
  SearchDropDown,
  StarzMenu,
  VideosMenu,
  BottomBar,
  TalentFilter,
  UserVideoModal,
  CustomPopup,
  RatingPopup,
  StarzBoard
} from '../components/common';
import {
  MasonryListItem,
  HorizontalVideoList
} from '../components/items';
import {Fab, Toast, Icon, ActionSheet} from 'native-base';
const {width, height} = Dimensions.get("window");
import utils from '../utils';
import * as Animatable from 'react-native-animatable';
import AsyncStorage from '@react-native-community/async-storage';


const VideoCategories = [
  {"category_name":""},
  {"category_name":"Flash Videos", type:"FlashBarVideos"},
  {"category_name":"Recent Videos", type:"RecentVideos"},
  {"category_name":"Top Rated Videos", type:"TopRatedVideos"},
  {"category_name":"", type:"FlashBarVideos"}
]

const WinnerCategories = [
  {"category_name":""},
  {"category_name":"Monthly Winners"},
  {"category_name":"Semi Finalists"},
  {"category_name":"Finalists"},
  {"category_name":"Winner"},
  {"category_name":""}
]

var BUTTONS_ = ["5 miles", "10 miles", "20 miles", "Cancel"];
var CANCEL_INDEX_ = 3;

var BUTTONS = ["Dance Party", "Opening", "Exhibition", "Cancel"];
var CANCEL_INDEX = 3;

export default class Dashboard extends Component {
constructor(props) {
      super(props);
      const {state} = props.navigation;
      this.state = {
        headerText:"Dashboard",
        loadMask:false,
        starzMenuSelected:2,
        isTalentFilter:false,
        filterAnimation:"",
        third:"https://i.guim.co.uk/img/media/dfd56d28e5b8546c9f2cac7362d03256378cd81a/253_47_1880_2348/master/1880.jpg?width=700&quality=85&auto=format&fit=max&s=ad00c490c7ed5c63435657d811e2bc18",
        second:"https://cdn.britannica.com/s:500x350/53/9953-004-02360894/Catherine-Wheel-Twyla-Tharp-1981.jpg",
        first:"https://assets.rebelmouse.io/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpbWFnZSI6Imh0dHBzOi8vYXNzZXRzLnJibC5tcy8yMTcxMTY4OC8yMDAweC5qcGciLCJleHBpcmVzX2F0IjoxNjExNjU4MTI3fQ.uamRGI_B_EpQTgVsnWiLdNSqeNThp1HNGYIJzavyTlU/img.jpg?width=523&height=305",
        videos:[
          {image:"https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Two_dancers.jpg/220px-Two_dancers.jpg"},
          {image:"https://www.fulcrumgallery.com/product-images/P937983-10/ballerina-in-red-detail.jpg"},
          {image:"https://upload.wikimedia.org/wikipedia/commons/thumb/6/64/Polonezkoy_08859_nevit.jpg/364px-Polonezkoy_08859_nevit.jpg"},
          {image:"https://upload.wikimedia.org/wikipedia/commons/thumb/2/2d/Festival_des_Vieilles_Charrues_2018_-_Ang%C3%A8le_-_004-2.jpg/250px-Festival_des_Vieilles_Charrues_2018_-_Ang%C3%A8le_-_004-2.jpg"},
          {image:"https://www.smashinglists.com/wp-content/uploads/2012/09/Shoulders-Back.jpg"},
          {image:"https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRkiAIDU-aOr0CIJrvfWYNnfcocGLTL3Vq6JAUdvhwwpr28emSo"},
          {image:"https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Two_dancers.jpg/220px-Two_dancers.jpg"},
          {image:"https://www.fulcrumgallery.com/product-images/P937983-10/ballerina-in-red-detail.jpg"},
          {image:"https://upload.wikimedia.org/wikipedia/commons/thumb/6/64/Polonezkoy_08859_nevit.jpg/364px-Polonezkoy_08859_nevit.jpg"},
          {image:"https://www.smashinglists.com/wp-content/uploads/2012/09/Shoulders-Back.jpg"}
        ],
        isVideoModal:false,
        videoModalAnimation:"",
        isCustomPopup:false,
        customPopupTitle:"",
        customPopupRightPress:"",
        customPopupLeftPress:"",
        isRatingPopup:false,
        ratingPopupTitle:"",
        ratingPopupRightPress:"",
        ratingPopupLeftPress:"",
        imageSource:"",
        isWinnerScreenSelected:0,
        tab1:true,
        tab2:false,
        tab3:false,
        tab4:false,
        distanceText:"Select Distance",
        eventTypeText:"Select Event Type",
        data:[],
        selected:{},
        videoCategory:1,
        VideoType:"FlashBarVideos",
        userData:{}
      }
}

getVideos = ()=>{
  fetch(`http://jemstech.net/StarzMe/WebApi/${this.state.VideoType}?StateId=42083485485`)
  .then(res=>{
    return res.json()
  })
  .then(res=>{
    console.log(res, ' res')
    this.setState({
      data:res.data !== null ? res.data : []
    })
  })
}

componentDidMount(){
  AsyncStorage.getItem('UserData')
  .then(res=>{
    if(res){
      this.setState({userData:JSON.parse(res).data})
    }
  })
  .catch(err=>console.log(err))
  this.getVideos()
}

componentWillUnmount(){

}

_renderFlashVideos = ({ item, index }) => {
  var animationDelay = index*50

  return (
    <TouchableOpacity>
        <Animatable.View
        animation="slideInUp"
        iterationCount={1}
        style={styles.videoContainer}
        direction="alternate"
        duration={500}
        delay={animationDelay}>
              <Image source={{uri:item.image}} style={{width:width/2, height:width/3, resizeMode:"cover"}} />
        </Animatable.View>
    </TouchableOpacity>
  );
}

showView(){
  const {tab1,tab2,tab3,tab4,tab5} = this.state

  if(tab1 == true){

      if(this.state.starzMenuSelected == 1 || this.state.starzMenuSelected == "1"){
        if(this.state.isWinnerScreenSelected == 1){
          return (
            <ScrollView showsVerticalScrollIndicator={false}>
              <View style={{flex: 1}}>
                <HorizontalVideoList stateName="California" />
                <HorizontalVideoList stateName="Texas" />
                <HorizontalVideoList stateName="Alabama" />
              </View>
            </ScrollView>
          )
        }
        else {
          return (
            <StarzBoard
            leftPress={()=> this.moveToWinnerScreen()}
            rightPress={()=> this.moveToWinnerScreen()}
            bottomLeftPress={()=> this.moveToWinnerScreen()}
            bottomRightPress={()=> this.moveToWinnerScreen()} />
          )
        }
      }
      else if(this.state.starzMenuSelected == 2 || this.state.starzMenuSelected == "2"){
        const Gallery = this.state.videos
        var galleryImages = []

        for(let i = 0; i < Gallery.length; i++){
          b={uri:Gallery[i].image};
          galleryImages.push(b);
        }


        return (
            <ScrollView showsVerticalScrollIndicator={false}>
              <View style={{
                  flex: 1,
                  flexDirection:'row',
                  flexWrap:'wrap',
                  justifyContent:'space-between',
                  paddingHorizontal:5,
                }}>
                {this.state.data.map((res, i)=>{
                    return(
                      <Animatable.View
                        key={i}
                        animation="fadeInRightBig"
                        iterationCount={1}
                        style={{width:width*0.32, height:width*0.32}}
                        direction="alternate"
                        duration={800}
                        delay={200}>
                        <TouchableOpacity onPress={()=>this._onVideoSelect(res)}>
                          <Image source={{uri:res.CoverImage}} style={{width:"100%", height:"100%", resizeMode:"cover", borderWidth:2, borderColor:"#512c79"}} />
                        </TouchableOpacity>
                      </Animatable.View>

                    )
                })}
              </View>
            </ScrollView>
        )
      }
      else {
        return (
          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={{flex: 1, marginTop:10, backgroundColor:"#ededed"}}>
              <Animatable.View
              animation="zoomIn"
              iterationCount={1}
              style={{width:width, height:height/2}}
              direction="alternate"
              duration={800}
              delay={200}>
                <TouchableOpacity>
                  <Image source={require("../assets/newsImage.png")} style={{width:width, height:height/2, resizeMode:"cover"}} />
                </TouchableOpacity>
             </Animatable.View>
             <Animatable.View
             animation="zoomIn"
             iterationCount={1}
             style={{width:width, height:height/2}}
             direction="alternate"
             duration={800}
             delay={200}>
               <TouchableOpacity>
                 <Image source={require("../assets/newsImage.png")} style={{width:width, height:height/2, resizeMode:"cover"}} />
               </TouchableOpacity>
            </Animatable.View>
            <Animatable.View
            animation="zoomIn"
            iterationCount={1}
            style={{width:width, height:height/2}}
            direction="alternate"
            duration={800}
            delay={200}>
              <TouchableOpacity>
                <Image source={require("../assets/newsImage.png")} style={{width:width, height:height/2, resizeMode:"cover"}} />
              </TouchableOpacity>
           </Animatable.View>
          </View>
          </ScrollView>
        )
      }

  }
  else if(tab4 == true){

    return (
      <View style={{flex:1}}>
          <View style={{width:"100%", flexDirection:"row", paddingVertical:10}}>
            <TouchableOpacity>
              <View style={{ width:width/3}}>
                <Text numberOfLines={2} style={{color: "#FFF", fontSize: 13, paddingVertical: 12, textAlign:"center"}}>Monthly</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity>
              <View style={{ width:width/3}}>
                <Text numberOfLines={2} style={{color: "#FFF", fontSize: 15, paddingTop: 10, textAlign:"center", fontWeight:"600"}}>Today</Text>
                <Image style={{width:width/3, height:15, resizeMode:"contain"}} source={require("../assets/selectedBorder.png")} />
              </View>
            </TouchableOpacity>
            <TouchableOpacity>
              <View style={{ width:width/3}}>
                <Text numberOfLines={2} style={{color: "#FFF", fontSize: 13, paddingVertical: 12, textAlign:"center"}}>This week</Text>
              </View>
            </TouchableOpacity>
          </View>
          <TouchableOpacity onPress={() => this.openActionsheet("dis")} style={{width:"100%", flexDirection:"row", paddingVertical:5, paddingHorizontal:"15%"}}>
            <View style={{flex:1}}>
              <Text numberOfLines={2} style={{color: "#FFF", fontSize: 14, paddingVertical: 5, fontWeight:"bold"}}>{this.state.distanceText}</Text>
            </View>
            <View style={{width:30, height:30, alignItems:"center", justifyContent:"center"}}>
              <Icon type="Ionicons" style={{fontSize:22, textAlign:"right", color:"#FFF"}} name="md-arrow-dropdown" />
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.openActionsheet("eventType")} style={{width:"100%", flexDirection:"row", paddingVertical:5, paddingHorizontal:"15%"}}>
            <View style={{flex:1}}>
              <Text numberOfLines={2} style={{color: "#FFF", fontSize: 14, paddingVertical: 5, fontWeight:"bold"}}>{this.state.eventTypeText}</Text>
            </View>
            <View style={{width:30, height:30, alignItems:"center", justifyContent:"center"}}>
              <Icon type="Ionicons" style={{fontSize:22, textAlign:"right", color:"#FFF"}} name="md-arrow-dropdown" />
            </View>
          </TouchableOpacity>

          <View style={{width:width, height:height/1.8, paddingVertical:10}}>
            <Image source={require("../assets/eventMap.png")} style={{width:width, height:height/1.8, resizeMode:"cover"}} />
          </View>
      </View>
    )

  }

}

onSelectStarzMenu(visible) {
  this.setState({ starzMenuSelected: visible, isWinnerScreenSelected:0, videoCategory:1 });
}

_onVideoSelect = (res) => {
    this.setUserVideoVisible(true, res)
}

_renderMask(){
    if(this.state.loadMask == true){
        return (
          <Mask />
        )
    }
}

_renderTalentFilter(){
    if(this.state.isTalentFilter == true){
        return (
          <TalentFilter onClose={()=> this.setTalentFilterVisible(false)} filterAnimation={this.state.filterAnimation} />
        )
    }
}

setTalentFilterVisible(visible){
  if(visible == true){
    this.setState({ isTalentFilter: visible, filterAnimation:"fadeInDownBig" });
  }
  else {
    this.setState({ filterAnimation:"fadeOutUpBig" });
    setTimeout(()=>{
      this.setState({ isTalentFilter: visible });
    },400)
  }
}

moveToWinnerScreen(){
  this.setState({ isWinnerScreenSelected: 1 });
}

likeVideo(){
  var rPress = ()=> this.closeCustomPopup()
  var lPress = ()=> this.closeCustomPopup()
  this.openCustomPopup(rPress,lPress,"Do you like this video?","Confirm","Cancel")
}

shareVideo(){
  var rPress = ()=> this.closeCustomPopup()
  var lPress = ()=> this.closeCustomPopup()
  this.openCustomPopup(rPress,lPress,"Do you want to share this video?","Confirm","Cancel")
}

rateVideo(){
  var rPress = ()=> this.closeRatingPopup()
  var lPress = ()=> this.closeRatingPopup()
  var img = "rate"
  this.openRatingPopup(rPress,lPress,"Rate this video",img)
}

flashVideo(){
  var rPress = ()=> this.closeRatingPopup()
  var lPress = ()=> this.closeRatingPopup()
  var img = "flash"
  this.openRatingPopup(rPress,lPress,"Do you want to flash this video?",img)
}

_renderVideoModal(){
    if(this.state.isVideoModal){
        return (
          <UserVideoModal
            onClose={()=> this.setUserVideoVisible(false)}
            onLikePress={()=> this.likeVideo()}
            onRatePress={()=> this.rateVideo()}
            onFlashPress={()=> this.flashVideo()}
            onSharePress={()=> this.shareVideo()}
            videoModalAnimation={this.state.videoModalAnimation}
            data={this.state.selected}
          />
        )
    }
}

setUserVideoVisible(visible, res) {
  if(visible == true){
    this.setState({ isVideoModal: visible, videoModalAnimation:"fadeInUpBig", selected: res ? res : {}});
  }
  else {
    this.setState({ videoModalAnimation:"fadeOutDownBig" });
    setTimeout(()=>{
      this.setState({ isVideoModal: visible });
    },400)
  }
}

openCustomPopup(rPress,lPress,title,rText,lText){
  this.setState({
    isCustomPopup:true,
    customPopupTitle:title,
    customPopupRightPress:rPress,
    customPopupLeftPress:lPress,
    customPopupRightText:rText,
    customPopupLeftText:lText
  })
}

closeCustomPopup(){
  this.setState({isCustomPopup:false})
}

_renderCustomPopup(){
    if(this.state.isCustomPopup == true){
        return (
          <CustomPopup title={this.state.customPopupTitle} rightPress={this.state.customPopupRightPress} rightText={this.state.customPopupRightText} leftPress={this.state.customPopupLeftPress} leftText={this.state.customPopupLeftText} />
        )
    }
}

openRatingPopup(rPress,lPress,title,imgsrc){
  this.setState({
    isRatingPopup:true,
    ratingPopupTitle:title,
    ratingPopupRightPress:rPress,
    ratingPopupLeftPress:lPress,
    imageSource:imgsrc
  })
}

closeRatingPopup(){
  this.setState({isRatingPopup:false})
}

_renderRatingPopup(){
    if(this.state.isRatingPopup == true){
        return (
          <RatingPopup imageSrc={this.state.imageSource} title={this.state.ratingPopupTitle} rightPress={this.state.ratingPopupRightPress} rightText="Confirm" leftPress={this.state.ratingPopupLeftPress} leftText="Cancel" />
        )
    }
}

showVideosMenu(){
  const {tab1,tab2,tab3,tab4,tab5} = this.state

  if(tab1 == true){
    if(this.state.starzMenuSelected == 2 || this.state.starzMenuSelected == "2"){
      return (
          <VideosMenu 
            selected={this.state.videoCategory}
            onPress={async (x, t)=>{
              await this.setState({videoCategory:x, VideoType:t})
              this.getVideos()
            }}
            catData={VideoCategories} />
      )
    }
    else if(this.state.starzMenuSelected == 1 || this.state.starzMenuSelected == "1"){
      if(this.state.isWinnerScreenSelected == 1){
        return (
          <VideosMenu 
            selected={this.state.videoCategory}
            onPress={async (x, t)=>{
              await this.setState({videoCategory:x, VideoType:t})
              this.getVideos()
            }}
            catData={WinnerCategories} />
        )
      }
    }
  }
  else if(tab4 == true){

  }

}

showSearch(){
  const {tab1,tab2,tab3,tab4,tab5} = this.state

  if(tab1 == true){
    if(this.state.starzMenuSelected == 2 || this.state.starzMenuSelected == "2"){
      return (
        <SearchDropDown text="Search" onRightPress={()=> this.setTalentFilterVisible(true)} />
      )
    }
    else if(this.state.starzMenuSelected == 1 || this.state.starzMenuSelected == "1"){
      return (
        <TouchableOpacity>

          <Animatable.View
          animation="fadeIn"
          iterationCount={1}
          style={styles.videoContainer}
          direction="alternate"
          duration={700}
          delay={300}>
                <Image source={require("../assets/ad.png")} style={{width:width, height:50, resizeMode:"cover", marginVertical:15}} />
          </Animatable.View>

        </TouchableOpacity>
      )
    }
    else if(this.state.starzMenuSelected == 3 || this.state.starzMenuSelected == "3"){
      return (
        <TouchableOpacity>
            <Animatable.View
            animation="fadeIn"
            iterationCount={1}
            style={styles.videoContainer}
            direction="alternate"
            duration={700}
            delay={300}>
                  <Image source={require("../assets/ad.png")} style={{width:width, height:50, resizeMode:"cover", marginVertical:15}} />
            </Animatable.View>
        </TouchableOpacity>
      )
    }
  }
  else if(tab4 == true){

  }

}

showStarzMenu(){
  const {tab1,tab2,tab3,tab4,tab5,videoCategory} = this.state

  if(tab1 == true){
    return (
      <StarzMenu 
        boardPress={()=> this.onSelectStarzMenu("1")}
        contestPress={()=> this.onSelectStarzMenu("2")}
        newsPress={()=> this.onSelectStarzMenu("3")}
        selected={this.state.starzMenuSelected} />
    )
  }
}

openActionsheet(type) {
  if(type == "dis"){
    ActionSheet.show(
      {
        options: BUTTONS_,
        cancelButtonIndex: CANCEL_INDEX_,
        title: "Select Distance"
      },
      buttonIndex => {

        if (buttonIndex != 3) {
          this.setState({ distanceText: BUTTONS_[buttonIndex] });
        }

      }
    )
  }
  else {
    ActionSheet.show(
      {
        options: BUTTONS,
        cancelButtonIndex: CANCEL_INDEX,
        title: "Select Event Type"
      },
      buttonIndex => {

        if (buttonIndex != 3) {
          this.setState({ eventTypeText: BUTTONS[buttonIndex] });
        }

      }
    )
  }

}

switchTabs(tab){
    if(tab == 1){
      this.setState({ tab1: true, tab2:false, tab3:false, tab4:false, headerText:"dashboard" });
    }
    else if(tab == 2){
      this.setState({ tab1: false, tab2:true, tab3:false, tab4:false, headerText:"dashboard" });
    }
    else if(tab == 3){
      this.setState({ tab1: false, tab2:false, tab3:true, tab4:false, headerText:"dashboard" });
    }
    else if(tab == 4){
      this.setState({ tab1: false, tab2:false, tab3:false, tab4:true, headerText:"starz events" });
    }
}

_renderFabButton(){
  if(this.state.tab4 == true){
    return (
      <Fab
        active={false}
        direction="up"
        containerStyle={{bottom:100}}
        style={{ backgroundColor: '#ea0759' }}
        position="bottomRight"
        onPress={()=> this.props.navigation.navigate("AddEvents")}>
        <Icon type="FontAwesome" name="calendar" />
      </Fab>
    )
  }
}

render() {
    return (
      <View style={styles.dashboardMainView}>
        <ImageBackground source={SplashBg} style={styles.mainBg}>
          <Header bgColor="transparent" headerText={this.state.headerText} onLeftPress={null} onRightPress={null} rightIcon={null} />
          <UserSubHeader
            profileImage={this.state.userData.UserImage}
            profileOnPress={()=> this.props.navigation.navigate("Profile", {userData:this.state.userData})} 
            chatOnPress={()=> this.props.navigation.navigate("GroupChat")} 
            notificationOnPress={()=> this.props.navigation.navigate("Notifications")} 
            walletOnPress={()=> this.props.navigation.navigate("Purchase")} />
          {this.showSearch()}
          {this.showStarzMenu()}
          {this.showVideosMenu()}
          <View style={styles.dashboardContainerView}>
            {this.showView()}
          </View>
          <BottomBar onHomePress={()=> this.switchTabs(1)} onAirPress={()=> this.props.navigation.navigate("OnAir")} onUploadPress={()=> this.props.navigation.navigate("UploadVideo")} onEventPress={()=> this.switchTabs(4)} />
          </ImageBackground>
          {this._renderMask()}
          {this._renderTalentFilter()}
          {this._renderVideoModal()}
          {this._renderCustomPopup()}
          {this._renderRatingPopup()}
          {this._renderFabButton()}
      </View>
    );
  }
}
