/* @flow */
import React, { Component } from 'react';
import {
  View,
  KeyboardAvoidingView,
  ScrollView,
  Image,
  TextInput,
  Text,
  ImageBackground,
  Dimensions,
  ActivityIndicator,
  AsyncStorage,
  TouchableOpacity,
  FlatList,
  StatusBar,
  Platform,
  Modal
} from 'react-native';
import styles from '../styles/style';
import { CommonActions, StackActions } from '@react-navigation/native';
import {
  Baseurl,
  Mask,
  Header,
  UserSubHeader,
  Banner,
  PlaceHolder,
  PlaceHolderSmall,
  ButtonSmall,
  LocationIcon,
  PaymentBg,
  NotificationIcon,
  Rating,
  SplashBg,
  StateDropDown,
  StarzMenu,
  VideosMenu,
  BottomBar,
  TalentFilter
} from '../components/common';
import {
  MasonryListItem
} from '../components/items';
import {Toast, Icon, ActionSheet} from 'native-base';
const {width, height} = Dimensions.get("window");
import utils from '../utils';
import * as Animatable from 'react-native-animatable';

export default class Guest extends Component {
constructor(props) {
      super(props);
      const {state} = props.navigation;
      this.state = {
        headerText:"guest",
        loadMask:false,
        stateText:"Select State",
        locations:null,
        state_id: 0,
        modalVisible: false,
        starzMenuSelected:2,
        isTalentFilter:false,
        filterAnimation:"",
        third:"https://i.guim.co.uk/img/media/dfd56d28e5b8546c9f2cac7362d03256378cd81a/253_47_1880_2348/master/1880.jpg?width=700&quality=85&auto=format&fit=max&s=ad00c490c7ed5c63435657d811e2bc18",
        second:"https://cdn.britannica.com/s:500x350/53/9953-004-02360894/Catherine-Wheel-Twyla-Tharp-1981.jpg",
        first:"https://assets.rebelmouse.io/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpbWFnZSI6Imh0dHBzOi8vYXNzZXRzLnJibC5tcy8yMTcxMTY4OC8yMDAweC5qcGciLCJleHBpcmVzX2F0IjoxNjExNjU4MTI3fQ.uamRGI_B_EpQTgVsnWiLdNSqeNThp1HNGYIJzavyTlU/img.jpg?width=523&height=305",
        videos:[
          {image:"https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Two_dancers.jpg/220px-Two_dancers.jpg"},
          {image:"https://www.fulcrumgallery.com/product-images/P937983-10/ballerina-in-red-detail.jpg"},
          {image:"https://upload.wikimedia.org/wikipedia/commons/thumb/6/64/Polonezkoy_08859_nevit.jpg/364px-Polonezkoy_08859_nevit.jpg"},
          {image:"https://upload.wikimedia.org/wikipedia/commons/thumb/2/2d/Festival_des_Vieilles_Charrues_2018_-_Ang%C3%A8le_-_004-2.jpg/250px-Festival_des_Vieilles_Charrues_2018_-_Ang%C3%A8le_-_004-2.jpg"},
          {image:"https://www.smashinglists.com/wp-content/uploads/2012/09/Shoulders-Back.jpg"},
          {image:"https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRkiAIDU-aOr0CIJrvfWYNnfcocGLTL3Vq6JAUdvhwwpr28emSo"},
          {image:"https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Two_dancers.jpg/220px-Two_dancers.jpg"},
          {image:"https://www.fulcrumgallery.com/product-images/P937983-10/ballerina-in-red-detail.jpg"},
          {image:"https://upload.wikimedia.org/wikipedia/commons/thumb/6/64/Polonezkoy_08859_nevit.jpg/364px-Polonezkoy_08859_nevit.jpg"},
          {image:"https://www.smashinglists.com/wp-content/uploads/2012/09/Shoulders-Back.jpg"}
        ]
      }
}

componentDidMount(){

}

componentWillUnmount(){

}

getStates(){
  this.setState({ modalVisible: true, isLoading: true, modalHeaderText: "Select State" })
  fetch(Baseurl + "GetStates", {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
  })
    .then((response) => response.json())
    .then((responseJson) => {
      console.log(responseJson)
      if (responseJson.status == "true" || responseJson.status == true) {
        this.setState({ locations: responseJson.data, isLoading: false })
      }
      else {
        this.setState({ locations: [], isLoading: false })
        utils.showToast("A network error occurred!");
      }
    })
    .catch((error) => {
      console.log(error)
      this.setState({ locations: [], isLoading: false })
      utils.showToast("A network error occurred!");
    });
}

_renderFlashVideos = ({ item, index }) => {
  var animationDelay = index*50

  return (
    <TouchableOpacity>
        <Animatable.View
        animation="slideInUp"
        iterationCount={1}
        style={styles.videoContainer}
        direction="alternate"
        duration={500}
        delay={animationDelay}>
              <Image source={{uri:item.image}} style={{width:width/2, height:width/3, resizeMode:"cover"}} />
        </Animatable.View>
    </TouchableOpacity>
  );
}

showView(){
  const Gallery = this.state.videos
  var galleryImages = []

  for(let i = 0; i < Gallery.length; i++){
    b={uri:Gallery[i].image};
    galleryImages.push(b);
  }

  return (
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={{flex: 1}}>
          <View style={{width:"100%", flexDirection:"row", paddingHorizontal:1}}>
            <TouchableOpacity>
              <Image source={{uri:this.state.first}} style={{width:width/1.5, height:width/1.5, resizeMode:"cover", borderWidth:2, borderColor:"#512c79"}} />
            </TouchableOpacity>
            <View style={{width:width/3}}>
              <TouchableOpacity>
                <Image source={{uri:this.state.second}} style={{width:width/3, height:width/3, resizeMode:"cover", borderWidth:2, borderColor:"#512c79"}} />
              </TouchableOpacity>
              <TouchableOpacity>
                <Image source={{uri:this.state.third}} style={{width:width/3, height:width/3, resizeMode:"cover", borderWidth:2, borderColor:"#512c79"}} />
              </TouchableOpacity>
            </View>
          </View>
          <MasonryListItem items={galleryImages} onPress={this._onVideoSelect} />
        </View>
      </ScrollView>
  )
}

setModalVisible(visible) {
  this.setState({ modalVisible: visible });
}

onSelectStarzMenu(visible) {
  this.setState({ starzMenuSelected: visible });
}

_onLocationSelect = (id, name) => {
  this.setState({
    state_id: id,
    stateText: name
  })
  this.setState({ modalVisible: false })
}

_onVideoSelect = () => {

}

renderModal() {
  return (
    <Modal
      animationType="slide"
      transparent={false}
      visible={this.state.modalVisible}
      onRequestClose={() => {

      }}>
      <View style={{ flex: 1 }}>
        <Header bgColor="#bd376a" headerText={this.state.modalHeaderText} onLeftPress={() => this.setModalVisible(false)} onRightPress={null} rightIcon={null} />
        {this.renderContent()}
      </View>
    </Modal>
  )
}

_renderLocations = ({ item, index }) => {
  var locationTitle = item.StateName
  return (
    <TouchableOpacity onPress={() => this._onLocationSelect(item.StateId, item.StateName)}>
      <View style={{ flex: 1, borderWidth: 0.2, borderColor: "#8a93a5", borderRadius: 5, marginVertical: 5, marginHorizontal: 10, paddingHorizontal: 10, backgroundColor: "#FFF" }}>
        <View style={{ flexDirection: "row" }}>
          <View style={{ flex: 1, paddingVertical: 5, flexDirection: "column" }}>
            <Text numberOfLines={1} style={[styles.fontBook, { color: "#000", fontSize: 16, paddingLeft: 5, paddingVertical: 10 }]}>{locationTitle}</Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
}

renderContent() {
  if (this.state.isLoading == true) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator size="large" color="#ff0000" />
      </View>
    )
  }
  else if (this.state.isLoading != true && this.state.locations != "") {
    return (
      <View style={{ flex: 1 }}>
        <FlatList
          data={this.state.locations}
          style={{ marginBottom: 10, flex: 1 }}
          renderItem={this._renderLocations} />
      </View>
    )
  }
  else {

  }
}

_renderMask(){
    if(this.state.loadMask == true){
        return (
          <Mask />
        )
    }
}

_renderTalentFilter(){
    if(this.state.isTalentFilter == true){
        return (
          <TalentFilter onClose={()=> this.setTalentFilterVisible(false)} filterAnimation={this.state.filterAnimation} />
        )
    }
}

setTalentFilterVisible(visible) {
  if(visible == true){
    this.setState({ isTalentFilter: visible, filterAnimation:"fadeInDownBig" });
  }
  else {
    this.setState({ filterAnimation:"fadeOutUpBig" });
    setTimeout(()=>{
      this.setState({ isTalentFilter: visible });
    },400)
  }
}

render() {
    return (
      <View style={styles.dashboardMainView}>
        <ImageBackground source={SplashBg} style={styles.mainBg}>
          <Header bgColor="transparent" headerText={this.state.headerText} onLeftPress={()=> this.props.navigation.goBack()} onRightPress={()=> this.setTalentFilterVisible(true)} rightIcon="ios-options" />
          <UserSubHeader profileOnPress={null} chatOnPress={null} notificationOnPress={null} walletOnPress={null} />
          <StateDropDown text={this.state.stateText} onPress={()=> this.getStates()} />
          <StarzMenu boardPress={()=> this.onSelectStarzMenu("1")} contestPress={()=> this.onSelectStarzMenu("2")} newsPress={()=> this.onSelectStarzMenu("3")} selected={this.state.starzMenuSelected} />
          <VideosMenu />
          <View style={styles.dashboardContainerView}>
              {this.showView()}
          </View>
          <BottomBar onHomePress={null} onAirPress={null} onUploadPress={null} onNewsPress={null} onBoardPress={null} />
          </ImageBackground>
          {this.renderModal()}
          {this._renderMask()}
          {this._renderTalentFilter()}
      </View>
    );
  }
}
