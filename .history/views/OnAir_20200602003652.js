/* @flow */
import React, { Component } from 'react';
import {
  View,
  KeyboardAvoidingView,
  ScrollView,
  Image,
  TextInput,
  Text,
  ImageBackground,
  Dimensions,
  ActivityIndicator,
  AsyncStorage,
  TouchableOpacity,
  FlatList,
  StatusBar,
  Platform,
  Modal
} from 'react-native';
import styles from '../styles/style';
// import { NavigationActions } from 'react-navigation';
import ImagePicker from 'react-native-image-picker';
import {
  Baseurl,
  Mask,
  VideoHeader,
  UserSubHeader,
  Banner,
  PlaceHolder,
  PlaceHolderSmall,
  ButtonSmall,
  LocationIcon,
  PaymentBg,
  NotificationIcon,
  Rating,
  SplashBg,
  StateDropDown,
  StarzMenu,
  VideosMenu,
  BottomBar,
  TalentFilter,
  UploadVideoButton
} from '../components/common';
import {
  MasonryListItem
} from '../components/items';
import {Toast, Icon, ActionSheet} from 'native-base';
const {width, height} = Dimensions.get("window");
import utils from '../utils';
import * as Animatable from 'react-native-animatable';

var options = {
  title: 'Upload Video',
  videoQuality:'low',
  mediaType:"video",
  durationLimit:30,
  allowsEditing:true,
  storageOptions: {
    waitUntilSaved:true,
    cameraRoll:true
  }
};

export default class OnAir extends Component {
constructor(props) {
      super(props);
      const {state} = props.navigation;
      this.state = {
        headerText:"",
        loadMask:false,
        imageTaken:false,
        resultVideo:null
      }
}

componentDidMount(){
  //this.openVideoGalleryOptions()
}

componentWillUnmount(){

}

openVideoGalleryOptions(){
  ImagePicker.launchImageLibrary(options, (response) => {
  console.log('Response = ', response);
  // alert('Response = ', JSON.stringify(response))

  if (response.didCancel) {
    //console.error('User cancelled image picker');
  }
  else if (response.error) {
    //console.error('ImagePicker Error: ', response.error);
  }
  else if (response.customButton) {
    //console.error('User tapped custom button: ', response.customButton);
  }
  else {
     //alert(response.uri)
     this.setState({
       imageTaken:true,
       resultVideo: response.uri
     });
    }
  });
}

render() {
    return (
      <View style={styles.dashboardMainView}>
          <VideoHeader bgColor="#000" headerText={this.state.headerText} onLeftPress={()=> this.props.navigation.goBack()} type="onAir" />
          <View style={{flex:1, backgroundColor:"#000"}}>

          <ImageBackground style={{width:"100%", height:height-100, marginTop:10}} resizeMode="cover" source={{uri:"https://mk0musicgrotto1kyale.kinstacdn.com/wp-content/uploads/2018/05/singer-lounge-microphone-karaoke.jpg.640x427_q100.jpg"}}>
              <View style={{flex:1, backgroundColor: "rgba(0, 0, 0, 0.8)"}}>
                  <Image style={{width:width, height:100, resizeMode:"contain"}} source={require('../assets/onAirScreen.png')} />
                  <View style={{width:"100%", paddingHorizontal:20, paddingVertical:20}}>
                    <TouchableOpacity activeOpacity={1}>
                      <View style={{width:50, height:50, borderRadius:25, borderWidth:1, borderColor:"#fd3a40", alignItems: "center", justifyContent:"center"}}>
                          <Image style={{width:40, height:40, resizeMode:"stretch", borderRadius:20}} source={require("../assets/UserProfileIcon.png")} />
                      </View>
                      <View style={{width:50, backgroundColor:"#fd3a40", marginVertical:5}}>
                          <Text style={{textAlign:"center", color:"#FFF", fontSize:12, paddingVertical:5}}>LIVE</Text>
                      </View>
                    </TouchableOpacity>
                  </View>

                  <View style={{width:"100%", height:100}}></View>

                  <ScrollView>

                  <View style={{width:"100%", paddingHorizontal:20, paddingVertical:20}}>
                    <TouchableOpacity activeOpacity={1} style={{flexDirection:"row", width:"100%", marginVertical:10}}>
                      <View style={{width:50, height:50, borderRadius:25, borderWidth:1, borderColor:"#fd3a40", alignItems: "center", justifyContent:"center"}}>
                          <Image style={{width:40, height:40, resizeMode:"stretch", borderRadius:20}} source={require("../assets/UserProfileIcon.png")} />
                      </View>
                      <View style={{backgroundColor:"#000", padding:10, borderRadius:5, marginHorizontal:10}}>
                          <Text style={{textAlign:"center", color:"#FFF", fontSize:12, paddingVertical:5}}>Hello there</Text>
                      </View>
                    </TouchableOpacity>

                    <TouchableOpacity activeOpacity={1} style={{flexDirection:"row", width:"100%", marginVertical:10}}>
                      <View style={{width:50, height:50, borderRadius:25, borderWidth:1, borderColor:"#fd3a40", alignItems: "center", justifyContent:"center"}}>
                          <Image style={{width:40, height:40, resizeMode:"stretch", borderRadius:20}} source={require("../assets/UserProfileIcon.png")} />
                      </View>
                      <View style={{backgroundColor:"#000", padding:10, borderRadius:5, marginHorizontal:10}}>
                          <Text style={{textAlign:"center", color:"#FFF", fontSize:12, paddingVertical:5}}>Wow.. Lovely</Text>
                      </View>
                    </TouchableOpacity>

                    <TouchableOpacity activeOpacity={1} style={{flexDirection:"row", width:"100%", marginVertical:10}}>
                      <View style={{width:50, height:50, borderRadius:25, borderWidth:1, borderColor:"#fd3a40", alignItems: "center", justifyContent:"center"}}>
                          <Image style={{width:40, height:40, resizeMode:"stretch", borderRadius:20}} source={require("../assets/UserProfileIcon.png")} />
                      </View>
                      <View style={{backgroundColor:"#000", padding:10, borderRadius:5, marginHorizontal:10}}>
                          <Text style={{textAlign:"center", color:"#FFF", fontSize:12, paddingVertical:5}}>Nice song. Awesome</Text>
                      </View>
                    </TouchableOpacity>

                    <TouchableOpacity activeOpacity={1} style={{flexDirection:"row", width:"100%", marginVertical:10}}>
                      <View style={{width:50, height:50, borderRadius:25, borderWidth:1, borderColor:"#fd3a40", alignItems: "center", justifyContent:"center"}}>
                          <Image style={{width:40, height:40, resizeMode:"stretch", borderRadius:20}} source={require("../assets/UserProfileIcon.png")} />
                      </View>
                      <View style={{backgroundColor:"#000", padding:10, borderRadius:5, marginHorizontal:10}}>
                          <Text style={{textAlign:"center", color:"#FFF", fontSize:12, paddingVertical:5}}>Stupid</Text>
                      </View>
                    </TouchableOpacity>

                    <TouchableOpacity activeOpacity={1} style={{flexDirection:"row", width:"100%", marginVertical:10}}>
                      <View style={{width:50, height:50, borderRadius:25, borderWidth:1, borderColor:"#fd3a40", alignItems: "center", justifyContent:"center"}}>
                          <Image style={{width:40, height:40, resizeMode:"stretch", borderRadius:20}} source={require("../assets/UserProfileIcon.png")} />
                      </View>
                      <View style={{backgroundColor:"#000", padding:10, borderRadius:5, marginHorizontal:10}}>
                          <Text style={{textAlign:"center", color:"#FFF", fontSize:12, paddingVertical:5}}>Hate this...</Text>
                      </View>
                    </TouchableOpacity>
                  </View>

                  </ScrollView>

                  <View style={{flexDirection:"row", width:"100%", height:70}}>
                    <View style={{backgroundColor:"#000", padding:10, borderRadius:5, marginHorizontal:10, flex:1}}>
                        <Text style={{color:"#FFF", fontSize:13, paddingVertical:5, paddingHorizontal:10}}>Write Comment...</Text>
                    </View>
                    <TouchableOpacity style={{width:50, height:50, alignItems:"center", justifyContent:"center"}}>
                      <Icon type="Ionicons" style={{fontSize:22, textAlign:"center", color:"#FFF"}} name="md-send" />
                    </TouchableOpacity>
                  </View>


              </View>
          </ImageBackground>

          </View>
      </View>
    );
  }
}
