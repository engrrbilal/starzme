/* @flow */
import React, { Component } from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  ImageBackground,
  ActivityIndicator,
  AsyncStorage,
  Dimensions,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  Keyboard,
  Platform,
  Modal,
  FlatList
} from 'react-native';
import { SplashBg, FacebookIcon, TwitterIcon, InstaIcon, LinkIcon, GoogleIcon, FormContainer, LoginUserIcon, LoginPassIcon, RegisterEmailIcon, SaveIconInActive, Mask, Baseurl, ButtonBg, Header, Leading } from '../components/common';
import styles, { width } from '../styles/style';
import * as Animatable from 'react-native-animatable';
// import { NavigationActions, StackActions } from 'react-navigation';
import PhoneInput from 'react-native-phone-input'
import utils from '../utils';
import { Icon, ActionSheet } from 'native-base';
import services from '../api/services';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

export default class PasswordVerfication extends Component {
    constructor(props) {
        super(props);
        const { state } = props.navigation;
        this.state = {
            loadMask: false,
            user_id: state.params ? state.params.userid : "",
            user_otp:"",
            phone: "",
            input1: "",
            input2: "",
            input3: "",
            input4: "",
            headerText:"confirm your account"
        }
    }

    componentDidMount() {
        console.log(this.state.data+" "+this.state.phone)
        //alert(this.state.data.OTP)
        //{"UserId":"138591792715","OTP":6956} +923122681827
        //var _data = JSON.st
    }

    checkCode() {
      var final_Code = this.state.input1+this.state.input2+this.state.input3+this.state.input4
      //var userData = JSON.parse(this.state.data)
      var _otp = this.state.user_otp
      //alert(_otp)
      payload = {
        code: final_Code,
      }
      if (payload.code.length < 4) {
        utils.showToast('Please enter 4 digit code sent to your phone number!');
      }
      else if (payload.code != _otp) {
        utils.showToast('Please enter correct code sent to your phone number!');
      }
      else {

          _payload = {
            UserId : this.state.user_id
          }
                console.log(_payload)
                this.setState({loadMask:true});
                  fetch(Baseurl+'ConfirmRegistration', {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                      },
                      body: JSON.stringify(_payload),
                    })
                    .then((response) => response.json())
                    .then((responseJson) => {
                      console.log(responseJson)

                      if(responseJson.status == "true" || responseJson.status == true){
                        this.setState({loadMask:false})
                        utils.showToast('Your Registration is confirmed. Please login to continue!');
                        this.props.navigation.navigate("Login")
                      }
                      else {
                        this.setState({loadMask:false})
                        utils.showToast(responseJson.message);
                      }

                    })
                    .catch((error) => {
                      this.setState({loadMask:false});
                      utils.showToast("A network error occurred!");
                    });


      }

    }

    handleChange = (name, next, value) => {

        this.setState({
            [name]: value,
        })
        if (value !== '') {
            this._onSubmitEditing(name, next)
            if (name == 'input4' && next == 'input4') {
                Keyboard.dismiss()
            }
        }

    }

    _onSubmitEditing = (cur, next) => {
        if (next != null) {
            this.refs[next].focus()
        } else {
            this.refs[cur].blur()
        }
    }

    _renderMask() {
        if (this.state.loadMask == true) {
            return (
                <Mask />
            )
        }
    }

    render() {
        const { input1 = '', input2 = '', input3 = '', input4 = '', } = this.state;
        return (
          <KeyboardAvoidingView behavior={Platform.OS == 'ios' ? "padding" : null} style={{ flex: 1, backgroundColor: "#bd376a" }}>
            <ImageBackground source={SplashBg} style={styles.mainBg}>
              <Header bgColor="transparent" headerText={this.state.headerText} onLeftPress={()=> this.props.navigation.goBack()} onRightPress={null} rightIcon={null} />
              <ScrollView keyboardShouldPersistTaps={"always"} style={{ paddingHorizontal: 10 }} showsVerticalScrollIndicator={false}>
                <View style={{width:"100%", marginTop:50}}>
                  <Text style={[styles.loginText, {lineHeight:18, paddingHorizontal:5, textAlign:"center", color:"#FFF"}]}>4 Digit OTP Code sent on your mobile number :</Text>
                  <Text style={[styles.loginText, { paddingVertical: 10, fontWeight: 'bold', fontSize:18, textAlign:"center", color:"#FFF" }]}>{this.state.phone}</Text>
                    <View style={[styles.labelContainer, { paddingHorizontal: 0, flexDirection: 'column', justifyContent: 'center', backgroundColor: 'transparent', borderBottomWidth:0 }]}>
                      <View style={{ width:"100%", flexDirection: 'row' }}>
                        <TextInput
                          style={styles.pinInput}
                          placeholderTextColor={"#000"}
                          ref={'input1'}
                          value={input1}
                          returnKeyType="done"
                          /* onSubmitEditing={} */
                          underlineColorAndroid="transparent"
                          keyboardType="number-pad"
                          maxLength={1}
                          secureTextEntry={false}
                          onChangeText={this.handleChange.bind(this, "input1", 'input2')}
                          placeholder="*" />
                        <TextInput
                          style={styles.pinInput}
                          placeholderTextColor={"#000"}
                          ref={'input2'}
                          returnKeyType="done"
                          maxLength={1}
                          value={input2}
                          secureTextEntry={false}
                          /* onSubmitEditing={Keyboard.dismiss()} */
                          underlineColorAndroid="transparent"
                          keyboardType="number-pad"
                          onChangeText={this.handleChange.bind(this, "input2", "input3")}
                          placeholder="*" />
                        <TextInput
                          style={styles.pinInput}
                          placeholderTextColor={"#000"}
                          ref={'input3'}
                          returnKeyType="done"
                          maxLength={1}
                          value={input3}
                          secureTextEntry={false}
                          /* onSubmitEditing={Keyboard.dismiss()} */
                          underlineColorAndroid="transparent"
                          keyboardType="number-pad"
                          onChangeText={this.handleChange.bind(this, "input3", "input4")}
                          placeholder="*" />
                        <TextInput
                          style={styles.pinInput}
                          placeholderTextColor={"#000"}
                          ref={'input4'}
                          returnKeyType="done"
                          maxLength={1}
                          value={input4}
                          secureTextEntry={false}
                          underlineColorAndroid="transparent"
                          keyboardType="number-pad"
                          onChangeText={this.handleChange.bind(this, "input4", "input4")}
                          placeholder="*" />
                      </View>
                    </View>
                    <View style={{ width: "100%", paddingHorizontal: 50, marginVertical: 30, alignItems: "center", justifyContent: "center" }}>
                      <ButtonBg onPress={() => this.checkCode()} label="Confirm" color="#fd3a40" />
                    </View>
                    <Image source={Leading} resizeMode="cover" style={{width:"100%", height:500}} />
                </View>
              </ScrollView>
              {this._renderMask()}
            </ImageBackground>
          </KeyboardAvoidingView>
        );
    }

}
