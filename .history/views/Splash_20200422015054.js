/* @flow */
import React, { Component } from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  ImageBackground,
  ActivityIndicator,
  AsyncStorage,
  Platform
} from 'react-native';
import { ButtonBg, SplashBg, SplashLogo, SplashLogoText } from '../components/common';
import styles,{width} from '../styles/style';
import * as Animatable from 'react-native-animatable';
import { NavigationActions, StackActions } from 'react-navigation';
import SplashScreen from 'react-native-splash-screen'
import ConfettiCannon from 'react-native-confetti-cannon';

export default class Splash extends Component {
  static navigationOptions = {
    title: "Splash",
    header:null
  };
  state = {
    animationNameState:"fadeInDown",
    animationText:"fadeInUp"
  }

componentDidMount(){
  SplashScreen.hide();
  setTimeout(()=>{
    const resetAction = StackActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({ routeName: 'Dashboard' })
          ],
    });

    this.props.navigation.dispatch(resetAction);
  },3000)
}

render() {
    return (
      <ImageBackground source={SplashBg} style={styles.splashBg}>
       <ConfettiCannon count={70} origin={{x: -10, y: 0}} />
        <View style={{flex:1, alignItems:"center", justifyContent:"center"}}>
        <View style={{flex:0.2}} />
              <View style={styles.splashinner}>
                    <Animatable.View
                    animation={this.state.animationNameState}
                    iterationCount={1}
                    direction="alternate"
                    delay={800}>
                        <Image source={SplashLogo} style={styles.splashLogo} />
                    </Animatable.View>
              </View>
              <View style={{flex:0.2}} />
        </View>
      </ImageBackground>
    );
  }

}
