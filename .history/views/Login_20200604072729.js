/* @flow */
import React, { Component } from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  ImageBackground,
  ActivityIndicator,
  AsyncStorage,
  Dimensions,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  Keyboard,
  Platform
} from 'react-native';
import { SplashBg, FacebookIcon, TwitterIcon, InstaIcon, LinkIcon, GoogleIcon, FormContainer, LoginUserIcon, LoginPassIcon, RegisterEmailIcon, SaveIconInActive, Mask, Baseurl, ButtonBg} from '../components/common';
import styles,{width} from '../styles/style';
import * as Animatable from 'react-native-animatable';
// import { NavigationActions, StackActions } from 'react-navigation';
import utils from '../utils';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

export default class Login extends Component {
constructor(props) {
        super(props);
        const {state} = props.navigation;
        this.state = {
          loadMask:false
        }
}

static navigationOptions = {
    title: "Login",
    header:null
};

componentDidMount(){

}

_onSubmitEditing = (cur, next) => {
    if (next != null) {
      this.refs[next].focus()

    } else {
        this.login()
    }
}

socialLogin(){
  // this.setState({loadMask:true});
  // setTimeout(()=>{
  //   this.setState({loadMask:false});
  //   const resetAction = StackActions.reset({
  //         index: 0,
  //         actions: [
  //           NavigationActions.navigate({ routeName: 'Dashboard' })
  //         ],
  //   });

  //   this.props.navigation.dispatch(resetAction);
  // },3000)
}

login(){
  var isSocial = false
  payload = {
    EmailAddress : this.user_email,
    Password : this.user_pass,
    SocialAccountId : isSocial,
  }

  if(payload.EmailAddress == undefined || payload.EmailAddress == "") {
      utils.showToast('Please enter your email address!');
   }
   else if(!utils.validateEmail(payload.EmailAddress)){
      utils.showToast('Please enter a valid email address!');
   }
   else if(payload.Password == undefined || payload.Password == ""){
      utils.showToast('Please enter your password!');
   }
   else {
         console.log(payload)
         this.setState({loadMask:true});
           fetch(Baseurl+'Login', {
               method: 'POST',
               headers: {
                 'Accept': 'application/json',
                 'Content-Type': 'application/json',
               },
               body: JSON.stringify(payload),
             })
             .then((response) => response.json())
             .then((responseJson) => {
               console.log(responseJson)
               if(responseJson.status == "true" || responseJson.status == true){
                 this.setState({loadMask:false})
                this.props.navigation.navigate('Dashboard')
               }
               else {
                 this.setState({loadMask:false})
                 utils.showToast(responseJson.message);
               }

             })
             .catch((error) => {
               this.setState({loadMask:false});
               utils.showToast("A network error occurred!");
             });
    }

}

async saveData(userData) {
    try {
      var data = JSON.stringify(userData);
      await AsyncStorage.setItem('userData', data)
      this.setState({ loadMask: false });
      // const resetAction = StackActions.reset({
      //   index: 0,
      //   actions: [
      //     NavigationActions.navigate({ routeName: 'Dashboard' })
      //   ],
      // });

      // this.props.navigation.dispatch(resetAction);
    }
    catch (error) {
      console.log(error)
    }
}

_renderMask(){
    if(this.state.loadMask == true){
        return (
          <Mask />
        )
    }
}

render() {
    return (
      <KeyboardAvoidingView behavior={Platform.OS == 'ios' ? "padding" : null} style={{flex:1, backgroundColor:"#bd376a"}}>
        <ImageBackground source={SplashBg} style={styles.mainBg}>
        {this._renderMask()}
          <View style={{flex:1, marginTop:screenHeight/8}}>
            <ScrollView bounces={false}>
              <View style={{width:"100%", marginVertical:10}}>
                <Text style={styles.loginTitleText}>Login</Text>
              </View>
              <View style={{width:"100%", flexDirection:"row"}}>
                <TouchableOpacity onPress={()=> this.socialLogin()}>
                  <View style={styles.socialIconsCont}>
                    <Image source={FacebookIcon} style={styles.socialIcons} />
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=> this.socialLogin()}>
                  <View style={styles.socialIconsCont}>
                    <Image source={TwitterIcon} style={styles.socialIcons} />
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=> this.socialLogin()}>
                  <View style={styles.socialIconsCont}>
                    <Image source={InstaIcon} style={styles.socialIcons} />
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=> this.socialLogin()}>
                  <View style={styles.socialIconsCont}>
                    <Image source={LinkIcon} style={styles.socialIcons} />
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=> this.socialLogin()}>
                  <View style={styles.socialIconsCont}>
                    <Image source={GoogleIcon} style={styles.socialIcons} />
                  </View>
                </TouchableOpacity>
              </View>
              <View style={{width:"100%", marginTop:50, paddingHorizontal:20}}>
                  <View style={{width:"100%"}}>
                    <Text style={styles.loginInnerText}>Or Login with your Email</Text>
                  </View>
                  <FormContainer onPress={()=> this.login()}>
                    <View style={styles.labelContainer}>
                      <View style={styles.formIconsCont}>
                          <Image source={RegisterEmailIcon} style={styles.formIcons} />
                      </View>
                      <View style={{flex:1}}>
                          <TextInput
                          style={styles.formInput}
                          placeholderTextColor={"#999999"}
                          keyboardType="email-address"
                          ref={'email'}
                          returnKeyType="next"
                          onSubmitEditing={() => this._onSubmitEditing('email', 'pass')}
                          underlineColorAndroid="transparent"
                          onChangeText={(text) => this.user_email = text}
                          placeholder="Enter E-mail" />
                      </View>
                    </View>
                    <View style={[styles.labelContainer, {borderBottomWidth:0}]}>
                      <View style={styles.formIconsCont}>
                          <Image source={LoginPassIcon} style={styles.formIcons} />
                      </View>
                      <View style={{flex:1}}>
                          <TextInput
                          style={styles.formInput}
                          ref={'pass'}
                          onSubmitEditing={() => this._onSubmitEditing('pass', null)}
                          returnKeyType="done"
                          placeholderTextColor={"#999999"}
                          underlineColorAndroid="transparent"
                          secureTextEntry
                          onChangeText={(text) => this.user_pass = text}
                          placeholder="Enter Password" />
                      </View>
                    </View>
                  </FormContainer>
                  <View style={{width:"100%", flexDirection:"row", paddingHorizontal:10, marginTop:20}}>
                    <View style={{flex:1, alignItems:"center", justifyContent:"center"}}>
                      <ButtonBg label="Sign up" onPress={()=> this.props.navigation.navigate("Register")} color="#fd3a40" />
                    </View>
                    <View style={{flex:1, alignItems:"center", justifyContent:"center"}}>
                      <ButtonBg label="Log in" onPress={()=> this.login()} color="#7755cd" />
                    </View>
                  </View>
                  <View style={{flex:1, paddingHorizontal:30, marginTop:25, alignItems:"center", justifyContent:"center"}}>
                      <ButtonBg onPress={()=> this.props.navigation.navigate("Guest")} label="LOGIN AS A GUEST" color="#dd5001" />
                  </View>
              </View>
            </ScrollView>
          </View>
        </ImageBackground>
      </KeyboardAvoidingView>
    );
  }

}
