/* @flow */
import React, { Component } from 'react';
import {
  View,
  KeyboardAvoidingView,
  ScrollView,
  Image,
  TextInput,
  Text,
  ImageBackground,
  Dimensions,
  ActivityIndicator,
  AsyncStorage,
  TouchableOpacity,
  FlatList,
  StatusBar,
  Platform,
  Modal
} from 'react-native';
import styles from '../styles/style';
import { NavigationActions } from 'react-navigation';
import ImagePicker from 'react-native-image-picker';
import {
  Baseurl,
  Mask,
  VideoHeader,
  UserSubHeader,
  Banner,
  PlaceHolder,
  PlaceHolderSmall,
  ButtonSmall,
  LocationIcon,
  PaymentBg,
  NotificationIcon,
  Rating,
  SplashBg,
  StateDropDown,
  StarzMenu,
  VideosMenu,
  BottomBar,
  TalentFilter,
  UploadVideoButton
} from '../components/common';
import {
  MasonryListItem
} from '../components/items';
import {Toast, Icon, ActionSheet} from 'native-base';
const {width, height} = Dimensions.get("window");
import utils from '../utils';
import * as Animatable from 'react-native-animatable';

var options = {
  title: 'Upload Video',
  videoQuality:'low',
  mediaType:"video",
  durationLimit:30,
  allowsEditing:true,
  storageOptions: {
    waitUntilSaved:true,
    cameraRoll:true
  }
};

export default class Profile extends Component {
constructor(props) {
      super(props);
      const {state} = props.navigation;
      this.state = {
        headerText:"",
        loadMask:false,
        imageTaken:false,
        resultVideo:null
      }
}

componentDidMount(){
  //this.openVideoGalleryOptions()
}

componentWillUnmount(){

}

openVideoGalleryOptions(){
  ImagePicker.launchImageLibrary(options, (response) => {
  console.log('Response = ', response);
  // alert('Response = ', JSON.stringify(response))

  if (response.didCancel) {
    //console.error('User cancelled image picker');
  }
  else if (response.error) {
    //console.error('ImagePicker Error: ', response.error);
  }
  else if (response.customButton) {
    //console.error('User tapped custom button: ', response.customButton);
  }
  else {
     //alert(response.uri)
     this.setState({
       imageTaken:true,
       resultVideo: response.uri
     });
    }
  });
}

render() {
    return (
      <View style={styles.dashboardMainView}>
          <View style={{flex:1}}>
            <ImageBackground style={{width:"100%", height:250}} resizeMode="cover" source={{uri:"https://mk0musicgrotto1kyale.kinstacdn.com/wp-content/uploads/2018/05/singer-lounge-microphone-karaoke.jpg.640x427_q100.jpg"}}>
              <VideoHeader bgColor="transparent" headerText={this.state.headerText} onLeftPress={()=> this.props.navigation.goBack()} type="onAir" />
              <View style={{width:"100%", flexDirection:"row", backgroundColor:"red"}}>
                <Text style={{width:"100%", fontSize:13, color:"#FFF", fontSize:16}}>Your work is going to fill a large part of your life...</Text>
                <View></View>
              </View>
            </ImageBackground>
          </View>
      </View>
    );
  }
}
