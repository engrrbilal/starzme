import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  StatusBar,
  Dimensions,
  Platform,
  Image,
  TextInput
} from 'react-native';
import {Root} from 'native-base';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';

import styles from './styles/style';
import Splash from './views/Splash';
import Login from './views/Login';
import Register from './views/Register';
import PasswordVerfication from './views/passwordVerification';
import Dashboard from './views/Dashboard';
import Guest from './views/Guest'
import UploadVideo from './views/UploadVideo'
import OnAir from './views/OnAir'
import AddEvents from './views/AddEvents'
import Purchase from './views/Purchase'
import GroupChat from './views/GroupChat'
import Notifications from './views/Notifications'
import Profile from './views/Profile'

// const AppStack = createStackNavigator({
//     Splash: {
//       screen: Splash,
//       navigationOptions: {
//       gesturesEnabled: false,
//     }
//     },
//     Login: {
//       screen: Login,
//       navigationOptions: {
//       gesturesEnabled: false,
//     }
//     },
//     Register: {
//       screen: Register,
//       navigationOptions: {
//       gesturesEnabled: true,
//     }
//     },
//     PasswordVerfication: {
//       screen: PasswordVerfication,
//       navigationOptions: {
//         gesturesEnabled: false,
//       }
//     },
//     Guest: {
//       screen: Guest,
//       navigationOptions: {
//         gesturesEnabled: false,
//       }
//     },
//     UploadVideo: {
//       screen: UploadVideo,
//       navigationOptions: {
//         gesturesEnabled: false,
//       }
//     },
//     OnAir: {
//       screen: OnAir,
//       navigationOptions: {
//         gesturesEnabled: false,
//       }
//     },
//     AddEvents: {
//       screen: AddEvents,
//       navigationOptions: {
//         gesturesEnabled: false,
//       }
//     },
//     Profile: {
//       screen: Profile,
//       navigationOptions: {
//         gesturesEnabled: false,
//       }
//     },
//     Purchase: {
//       screen: Purchase,
//       navigationOptions: {
//         gesturesEnabled: false,
//       }
//     },
//     GroupChat: {
//       screen: GroupChat,
//       navigationOptions: {
//         gesturesEnabled: false,
//       }
//     },
//     Notifications: {
//       screen: Notifications,
//       navigationOptions: {
//         gesturesEnabled: false,
//       }
//     },
//     Dashboard: {
//       screen: Dashboard,
//       navigationOptions: {
//         gesturesEnabled: false,
//       }
//     }
//   },{
//       initialRouteName: 'Splash',
//       headerMode:'none',
//       header:null,
// });

const Routes = [
  {
    name:"Splash",
    component:Splash
  },
  {
    name:"Login",
    component:Login
  },
  {
    name:"Register",
    component:Register
  },
  {
    name:"PasswordVerfication",
    component:PasswordVerfication
  },
  {
    name:"Guest",
    component:Guest
  },
  {
    name:"UploadVideo",
    component:UploadVideo
  },
  {
    name:"OnAir",
    component:OnAir
  },
  {
    name:"AddEvents",
    component:AddEvents
  },
  {
    name:"Profile",
    component:Profile
  },
  {
    name:"Purchase",
    component:Purchase
  },
  {
    name:"GroupChat",
    component:GroupChat
  },
  {
    name:"Notifications",
    component:Notifications
  },
  {
    name:"Dashboard",
    component:Dashboard
  },
]
export default class AppNavigator extends Component<{}> {
  AppStack(){
    const Stack = createStackNavigator();

    <NavigationContainer>
      <Stack.Navigator>
        {Routes.map((route, index)=>{
      return(
        <Stack.Screen options={{ headerShown: false }} name={route.name} component={route.component} />
      )
    })}
      </Stack.Navigator>
    </NavigationContainer>
  }

_renderStatusBar(){
    if(Platform.OS === "ios"){
      return (
        <StatusBar translucent
          animated
          StatusBarAnimation="fade"
          backgroundColor="rgba(0,0,0,0.3)"
          barStyle="light-content"/>
      )
    }
    else {
      return (
        <StatusBar
          animated
          StatusBarAnimation="fade"
          backgroundColor="black"
          barStyle="light-content"/>
      )
    }
}

constructor() {
  super();
  Text.defaultProps = Text.defaultProps || {}
  Text.defaultProps.allowFontScaling=false;
  TextInput.defaultProps = TextInput.defaultProps || {}
  TextInput.defaultProps.allowFontScaling=false;
}

render() {
    return (
      <Root>
        <View style={{flex:1, backgroundColor:"#bd376a"}}>
          {this._renderStatusBar()}
          {this.AppStack()}
        </View>
      </Root>
    );
  }

}
